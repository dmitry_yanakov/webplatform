import { EventEmitter, Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

export interface User {
  avatar?: string;
  id?: number;
  birth_date?: number;
  branch_id?: number;
  company_id?: number;
  date?: string;
  email?: string;
  first_name?: string;
  is_approved?: number;
  is_email?: number;
  is_mobile_notification?: number;
  language?: string;
  last_name?: string;
  phone_code?: number;
  phone_country?: string;
  phone_number?: number;
  rights?: number;
}

export interface Company {
  allow_date?: string;
  branch_id?: number;
  branch_title?: null;
  balance?: number;
  currency_code?: string;
  currency_course?: number;
  currency_id?: number;
  currency_symbol?: string;
  currency_title?: string;
  date?: string;
  description?: string;
  email?: string;
  employee_id?: number;
  employees_count?: number;
  fb_url?: string;
  id?: number;
  ig_url?: string;
  industry_id?: number;
  is_credit?: number;
  is_ready?: number;
  online_accept?: number;
  online_ban?: number;
  online_booking?: number;
  online_reject?: number;
  role_id?: number;
  role_title?: string;
  services_count?: number;
  session?: number;
  site_url?: string;
  sms?: number;
  sms_balance?: number;
  sms_client_booking?: number;
  sms_client_invite?: number;
  sms_client_status?: number;
  sms_subscription?: number;
  title?: string;
  type_id?: number;
}

export interface Branch {
  breaks?: BranchBreaks[];
  city?: string;
  company_id?: number;
  country?: string;
  date?: string;
  description?: string;
  employees_count?: number;
  id?: number;
  latitude?: number;
  longitude?: number;
  phones?: string;
  services_count?: number;
  state?: string;
  street?: string;
  times?: BranchTime[];
  title?: string;
  zip?: number;
}

export interface BranchTime {
  day?: number;
  time?: string;
  is_active?: number;
}

export interface BranchBreaks {
  id?: number;
  day?: number;
  from?: number;
  to?: number;
}

export interface UserLocation {
  code?: string;
  country?: string;
  ip?: string;
  phone_code?: number;
}

export enum Title {
  dashboard = 'title-dashboard',
  profile = 'title-profile',
  clients = 'title-clients',
}

@Injectable({
  providedIn: 'root',
})
export class CommonDataService {
  public user: User = {};
  public companies: Company[] = [];
  public branches: Branch[] = [];
  public currentCompany: Company = {};
  public currentBranch: Branch = {};
  public userLocation: UserLocation = {};

  public companyUpdate = new EventEmitter<boolean>();
  public isCompanyReadySubject$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  private missionAnnouncedSource = new BehaviorSubject<string>('');
  titleChanged$ = this.missionAnnouncedSource.asObservable();
  constructor() {}

  changeTitle(title: string = Title.dashboard) {
    this.missionAnnouncedSource.next(title);
  }
}
