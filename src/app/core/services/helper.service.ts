import { Injectable } from '@angular/core';
import { isArray, isObject, reduce, forEach } from 'lodash';

@Injectable({
  providedIn: 'root',
})
export class HelperService {
  constructor() {}

  copyToClipboard(text) {
    if (document.queryCommandSupported && document.queryCommandSupported('copy')) {
      const textarea = document.createElement('textarea');
      textarea.textContent = text;
      textarea.style.position = 'fixed';
      document.body.appendChild(textarea);
      textarea.select();
      try {
        return document.execCommand('copy');
      } catch (ex) {
        return false;
      } finally {
        document.body.removeChild(textarea);
      }
    }
  }

  getUnicObjectsValue(compareObject, initObject, exceptions?) {
    return reduce(
      initObject,
      (result, value, key) => {
        if (exceptions && exceptions.some((i) => i === key)) {
          return result;
        } else if (!isArray(value) && !isObject(value) && compareObject[key] !== value) {
          result[key] = compareObject[key];

          return result;
        } else if (!isArray(value) && isObject(value)) {
          forEach(value, (sub_value, index) => {
            if (compareObject[key][index] !== sub_value) {
              result[index] = compareObject[key][index];
            }
          });

          return result;
        } else if (isArray(value)) {
          result[key] = compareObject[key];

          return result;
        }
        return result;
      },
      {}
    );
  }

  parseHours(value: number, suffix: string): string {
    return Math.floor(value / 60) !== 0 ? `${Math.floor(value / 60)} ${suffix}` : '';
  }

  parseMinutes(value, suffix: string): string {
    return value % 60 !== 0 ? `${value % 60} ${suffix}` : '';
  }

  plural(number, titles) {
    const cases = [2, 0, 1, 1, 1, 2];
    return titles[number % 100 > 4 && number % 100 < 20 ? 2 : cases[number % 10 < 5 ? number % 10 : 5]];
  }

  randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
  }
}
