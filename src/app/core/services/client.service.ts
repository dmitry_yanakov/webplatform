import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ClientService {
  loading: Subject<boolean> = new Subject<boolean>();
  search: Subject<string> = new Subject<string>();
  clients: Subject<{ data: []; offset: number; count: number; total: number }> = new Subject();
  subscriptions: Subject<{}> = new Subject();
  transactions$: BehaviorSubject<[]> = new BehaviorSubject([]);
  courses$: BehaviorSubject<[]> = new BehaviorSubject([]);
  booking$: BehaviorSubject<[]> = new BehaviorSubject([]);
  public statistics$: BehaviorSubject<{}> = new BehaviorSubject({});
  public clientId$: BehaviorSubject<number> = new BehaviorSubject(null);
  constructor(private apiService: ApiService) {}

  getClients({ offset = 0, count = 7, search = '' }) {
    return this.apiService.getClients({ offset, count, search }).pipe(
      map((res) => {
        if (res['status'] === 'success') {
          const clients = res['data']['clients'].map((client) => {
            const avatar = client.avatar.split('avatars/')[1].length > 0 ? client.avatar : null;

            return { ...client, full_name: client.first_name + ' ' + client.last_name, avatar };
          });

          this.clients.next({ data: clients, offset, count, total: res['data']['count'] });

          return res['data'];
        } else {
          this.clients.next({ data: [], offset: 0, count: 7, total: 0 });

          return;
        }
      })
    );
  }

  createClient(clientData) {
    return this.apiService.createClient(clientData).pipe(
      map((res) => {
        if (res['status'] === 'success') {
          return res['data']['client_id'];
        } else {
          return 0;
        }
      })
    );
  }

  updateClient(clientData) {
    return this.apiService.updateClient(clientData).pipe(
      map((res) => {
        if (res['status'] === 'success') {
          return res;
        } else {
          return 0;
        }
      })
    );
  }

  banClient(client_id, is_ban) {
    return this.apiService.banClient(client_id, is_ban).pipe(
      map((res) => {
        if (res['status'] === 'success') {
          return res;
        } else {
          return 0;
        }
      })
    );
  }

  searchClients({ text = '' }) {
    this.loading.next(true);

    return this.getClients({
      search: text,
    });
  }

  getClient(client_id) {
    return this.apiService.getClient(client_id).pipe(
      map((res: any) => {
        if (res['status'] === 'success') {
          const data = {
            avatar: res['data'].avatar.split('avatars/')[1].length > 0 ? res['data'].avatar : null,
            source_id: res['data'].source_id,
            source_title: res['data'].source_title,
            first_name: res['data'].first_name,
            last_name: res['data'].last_name,
            middle_name: res['data'].middle_name,
            email: res['data'].email,
            phone_country: res['data'].phone_country,
            phone_code: res['data'].phone_code,
            phone_number: res['data'].phone_number,
            sex: res['data'].sex,
            status: res['data'].status,
            is_catalog: res['data'].is_catalog,
            is_ban: res['data'].is_ban,
            date: res['data'].date,
            birth: res['data'].birth,
            last_transaction: res['data'].last_transaction,
            id: res['data'].id,
            comment: res['data'].comment,
            branch_id: res['data'].branch_id,
          };
          this.clientId$.next(res['data'].id);
          this.subscriptions.next(res['data'].subscriptions);
          this.courses$.next(res['data'].courses);
          this.transactions$.next(res['data'].transactions);
          this.booking$.next(res['data'].booking);
          this.statistics$.next({
            bookings_count: res['data']['bookings_count'],
            transactions_middle: res['data']['transactions_middle'],
            transactions_profit: res['data']['transactions_profit'],
            last_transaction: res['data']['last_transaction'],
            bookings_deny_count: res['data']['bookings_deny_count'],
            bookings_accept_count: res['data']['bookings_accept_count'],
          });
          return data;
        }

        return {};
      })
    );
  }

  inviteClient(client_id, email, phone) {
    return this.apiService.inviteClient(client_id, email, phone).pipe(
      map((res) => {
        if (res['status'] === 'success') {
          return res;
        } else {
          return 0;
        }
      })
    );
  }

  getClientNote(client_id) {
    return this.apiService.clientNoteGet(client_id).pipe(
      map((res: any) => {
        if (res['status'] === 'success') {
          return res['data'];
        }

        return [];
      })
    );
  }

  createClientNote(client_id, note) {
    return this.apiService.clientNoteCreate({ client_id, note }).pipe(
      map((res) => {
        if (res['status'] === 'success') {
          return res;
        } else {
          return 0;
        }
      })
    );
  }

  updateClientNote(note_id, new_note) {
    return this.apiService.clientNoteUpdate(note_id, new_note).pipe(
      map((res) => {
        if (res['status'] === 'success') {
          return res;
        } else {
          return 0;
        }
      })
    );
  }

  removeNote(note_id) {
    return this.apiService.clientNoteRemove(note_id).pipe(
      map((res) => {
        if (res['status'] === 'success') {
          return res;
        } else {
          return 0;
        }
      })
    );
  }
}
