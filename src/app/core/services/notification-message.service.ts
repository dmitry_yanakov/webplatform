import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class NotificationMessageService {
  constructor(private messageService: MessageService) {}

  pushNotification({ life = 5000, detail = '', data = null }) {
    this.messageService.add({ key: 'notification', life, detail, data });
  }

  pushAlert({ life = 5000, detail = '', data = null }) {
    this.messageService.add({ key: 'alert', closable: false, detail, life, data });
  }
}
