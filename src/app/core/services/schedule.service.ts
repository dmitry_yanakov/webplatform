import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { map, tap, switchMap } from 'rxjs/operators';
import { normalize, schema } from 'normalizr';
import * as moment from 'moment';
import { AvailableEmployeesAndTime } from '@core/interfaces/schedule';
import { HelperService } from '@core/services/helper.service';
import { CommonDataService } from '@core/services/common-data.service';
import { MessageService } from 'primeng/api';
import { NotificationsService } from './notifications.service';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ScheduleService {
  colors = ['#FFB84D', '#83D875', '#FC695F', '#5FC4FC', '#728DEF', '#FF88AC', '#8FE4D5', '#C388FF'];

  constructor(
    private apiService: ApiService,
    private helperService: HelperService,
    private commonDataService: CommonDataService,
    private notificationsService: NotificationsService
  ) {}

  createBooking({ product_id, employee_id, from_date, comment = '', repeat_from = '', repeat_to = '', is_formed = '' }) {
    const random_color = this.colors[this.helperService.randomNumber(0, 7)];

    return this.apiService.createBooking({
      product_id,
      employee_id,
      from_date,
      comment,
      repeat_from,
      repeat_to,
      is_formed,
      color: random_color,
    });
  }

  addBookingClient({ booking_id, clients }) {
    return this.apiService.addBookingClient({ booking_id, clients });
  }

  getEmployeesAvailableTime({ product_id, employee_id, date }) {
    return this.apiService
      .getEmployeesAvailableTime({
        product_id,
        employee_id,
        current_date: moment().format('YYYY-MM-DD HH:mm'),
        date: moment(date).format('YYYY-MM-DD'),
      })
      .pipe(
        map(
          (res): AvailableEmployeesAndTime => {
            if (res['status'] === 'success') {
              const employee = new schema.Entity('employees');
              const time = new schema.Entity(
                'times',
                {
                  employees: [employee],
                },
                { idAttribute: (value) => moment(value.from_time).format('HH:mm') }
              );

              return normalize(res['data'], [time]);
            } else {
              return { entities: { employees: {}, times: {} }, result: [] };
            }
          }
        )
      );
  }

  acceptClient({ booking_id, client_id, notification_id = 0 }) {
    return this.apiService.acceptClient({ booking_id, client_id }).pipe(
      switchMap((res) => {
        if (res['status'] === 'success') {
          return this.notificationsService.clearNotification(notification_id);
        }

        return of(res);
      })
    );
  }

  rejectClient({ booking_id, client_id, notification_id = 0 }) {
    const business_type = this.commonDataService.currentCompany.session;

    if (business_type === 0) {
      return this.apiService.cancelClient({ booking_id, client_id }).pipe(
        switchMap((res) => {
          if (res['status'] === 'success') {
            return this.apiService.changeBookingStatus({ booking_id, status: 4 });
          }
        }),
        switchMap((res) => {
          if (res['status'] === 'success') {
            return this.apiService.removeBooking(booking_id);
          }
        }),
        switchMap((res) => {
          if (res['status'] === 'success') {
            return this.notificationsService.clearNotification(notification_id);
          }

          return of(res);
        })
      );
    } else if (business_type === 1) {
      return this.apiService.removeClient({ booking_id, client_id }).pipe(
        switchMap((res) => {
          if (res['status'] === 'success') {
            return this.notificationsService.clearNotification(notification_id);
          }

          return of(res);
        })
      );
    }
  }
}
