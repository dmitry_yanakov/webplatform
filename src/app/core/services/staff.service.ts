import { EventEmitter, Injectable, Output } from '@angular/core';
import { ApiService } from '@core/services/api.service';
import { map } from 'rxjs/operators';
import { forkJoin, Subject } from 'rxjs';
import { CommonDataService } from '@core/services/common-data.service';

@Injectable({
  providedIn: 'root',
})
export class StaffService {
  is_fire: number;
  loading: Subject<boolean> = new Subject<boolean>();
  search: Subject<string> = new Subject<string>();
  employees: Subject<{ data: []; offset: number; count: number; total: number }> = new Subject();

  @Output() employeeUpdated: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private apiService: ApiService, private commonDataService: CommonDataService) {}

  setIsFiredStatus(is_fire) {
    this.is_fire = is_fire;
  }

  searchEmployees(text = '') {
    this.loading.next(true);
    this.search.next(text);

    return this.getEmployees({ search: text });
  }

  getEmployee(employee_id) {
    return this.apiService.getEmployee(employee_id).pipe(
      map((res) => {
        if (res['status'] === 'success') {
          const avatar = res['data'].avatar.split('avatars/')[1].length > 0 ? res['data'].avatar : null;

          return { ...res['data'], avatar };
        }

        return {};
      })
    );
  }

  getEmployees({ offset = 0, count = 7, search = '', is_fire = Number(this.is_fire) }) {
    return this.apiService.getEmployees({ offset, count, search, is_fire }).pipe(
      map((res) => {
        if (res['status'] === 'success') {
          const employees = res['data']['employees'].map((employee) => {
            const avatar = employee.avatar.split('avatars/')[1].length > 0 ? employee.avatar : null;
            return { ...employee, avatar };
          });

          this.employees.next({ data: employees, offset, count, total: res['data']['count'] });

          return employees;
        } else {
          this.employees.next({ data: [], offset: 0, count: 7, total: 0 });

          return [];
        }
      })
    );
  }

  getCategory(branch_id) {
    const business_type = this.commonDataService.currentCompany.session;
    const product_type = business_type === 1 ? 3 : 1;

    return this.apiService.getCategory({ branch_id, product_type }).pipe(
      map((res) => {
        if (res['status'] === 'success') {
          return res['data'];
        }

        return [];
      })
    );
  }

  getProducts(branch_id) {
    const business_type = this.commonDataService.currentCompany.session;
    const product_type = business_type === 1 ? 3 : 1;

    return this.apiService.getProducts({ branch_id, count: 999, product_type, product_id: null }).pipe(
      map((res) => {
        if (res['status'] === 'success') {
          return res['data']['products'];
        }
        return [];
      })
    );
  }

  deactivateEmployee(employee_id) {
    return this.apiService.deactivateEmployee(employee_id);
  }
}
