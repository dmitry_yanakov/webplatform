import { Injectable, EventEmitter } from '@angular/core';
import { ApiService } from './api.service';
import { AuthService, FacebookLoginProvider, SocialUser, GoogleLoginProvider } from 'angularx-social-login';
import { Router } from '@angular/router';
import { CommonDataService, Branch, Company } from './common-data.service';

@Injectable()
export class AppAuthService {
  token;

  constructor(
    private apiService: ApiService,
    private router: Router,
    private authService: AuthService,
    private commonData: CommonDataService
  ) {}

  loginWithGoogle() {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    this.afterLoginSocial();
  }

  loginWithFB() {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
    this.afterLoginSocial();
  }

  afterLoginSocial() {
    this.authService.authState.subscribe((user) => {
      if (user && user != null) {
        if (user['provider'] === 'GOOGLE') {
          this.apiService.loginWithGoogle(user['id'], user['firstName'], user['lastName'], user['email']).subscribe((data) => {
            if (data['status'] === 'success') {
              this.afterLogin(data['data']['token']);
            }
          });
        } else if (user['provider'] === 'FACEBOOK') {
          this.apiService.loginWithFB(user['id'], user['firstName'], user['lastName'], user['email'], '', '').subscribe((data) => {
            if (data['status'] === 'success') {
              this.afterLogin(data['data']['token']);
            }
          });
        }
      }
    });
  }

  afterLogin(token) {
    this.apiService.token = token;
    localStorage.setItem('token', token);
    this.apiService.getSelfUser(token).subscribe((selfUserResponse) => {
      if (selfUserResponse['status'] === 'success') {
        this.apiService.user = selfUserResponse['data'];
        this.commonData.user = selfUserResponse['data'];
        if (!this.apiService.user['phone_number']) {
          this.router.navigate(['/more']);
        } else {
          this.getCompanies();
        }
      }
    });
  }

  getCompanies() {
    this.apiService.getSelfCompany().subscribe((selfCompanyResponse) => {
      if (selfCompanyResponse['status'] === 'success') {
        if (selfCompanyResponse['data'].length) {
          this.commonData.companies = selfCompanyResponse['data'];
          const companies: Company[] = this.commonData.companies;
          const company_id: number = this.commonData.user.company_id;

          if (company_id) {
            this.commonData.currentCompany = companies.find((company) => company_id === company.id);
            this.getBranches(company_id);
          } else {
            const currentCompany: Company = companies[0];

            this.getBranches(currentCompany['id']);
            this.apiService.setUser({ company_id: currentCompany['id'] }).subscribe((res) => {
              if (res['status'] === 'success') {
                this.commonData.currentCompany = currentCompany;
              }
            });
          }
          this.router.navigate(['/desk']);
        }
      } else if (selfCompanyResponse['status'] === 'error') {
        if (selfCompanyResponse['errors'][0].code === 3) {
          this.router.navigate(['/create-company']);
        }
      }
    });
  }

  getBranches(company_id) {
    this.apiService.getCompanyBranch(company_id).subscribe((res) => {
      if (res['status'] === 'success') {
        if (res['data']['branches'].length) {
          this.commonData.branches = res['data']['branches'];
          this.setCurrentBranch();
        } else {
          this.router.navigate(['/create-company']);
        }
      }
    });
  }

  setCurrentBranch() {
    const branches: Branch[] = this.commonData.branches;
    const branch_id: number = this.commonData.user.branch_id;

    if (branch_id) {
      this.commonData.currentBranch = branches.find((branch) => branch_id === branch.id);
    } else if (branches.length) {
      const currentBranch: Branch = branches[0];

      this.apiService.setUser({ branch_id: currentBranch['id'] }).subscribe((res) => {
        if (res['status'] === 'success') {
          this.commonData.currentBranch = currentBranch;
        }
      });
    } else {
      return;
    }
    this.commonData.isCompanyReadySubject$.next(!!this.commonData.currentCompany.is_ready);
  }

  signOut(): void {
    this.authService.signOut();
  }
}
