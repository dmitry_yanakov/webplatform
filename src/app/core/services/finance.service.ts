import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ApiService } from './api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class FinanceService {
  type: Subject<0 | 1> = new Subject<0 | 1>();
  loading: Subject<boolean> = new Subject<boolean>();
  search: Subject<string> = new Subject<string>();
  data: Subject<{ data: []; offset: number; count: number; total: number }> = new Subject();

  constructor(private apiService: ApiService) {}

  setFinanceType(type) {
    this.type.next(type);
  }

  searchFinance({ text = '', type }) {
    this.loading.next(true);

    return this.getFinance({
      search: text,
      type,
    });
  }

  getFinance({ type, offset = 0, count = 7, search = '' }) {
    return this.apiService.getTransaction({ type, offset, count, search }).pipe(
      map((res) => {
        if (res['status'] === 'success') {
          this.data.next({ data: res['data']['transactions'], offset, count, total: res['data']['count'] });
          return true;
        } else {
          this.data.next({ data: [], offset: 0, count: 7, total: 0 });

          return false;
        }
      })
    );
  }
}
