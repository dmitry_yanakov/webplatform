import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { ApiService } from './api.service';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  product_type: Subject<number> = new Subject<number>();
  loading: Subject<boolean> = new Subject<boolean>();
  search: Subject<string> = new Subject<string>();
  products: Subject<{ data: []; offset: number; count: number; total: number }> = new Subject();

  constructor(private apiService: ApiService) {}

  setProductType(product_type) {
    this.product_type.next(product_type);
  }

  searchProducts({ text = '', product_type = 0 }) {
    this.loading.next(true);

    return this.getProducts({
      search: text,
      product_type,
      is_archive: product_type === -1 ? 1 : 0,
    });
  }

  getProducts({ product_type = 0, offset = 0, count = 7, search = '', is_archive = 0, product_id = null }) {
    return this.apiService.getProducts({ product_type, offset, count, search, is_archive, product_id }).pipe(
      map((res) => {
        if (res['status'] === 'success') {
          this.products.next({ data: res['data']['products'], offset, count, total: res['data']['count'] });

          return res['data'];
        } else {
          this.products.next({ data: [], offset: 0, count: 7, total: 0 });

          return;
        }
      })
    );
  }

  getProductCategories({ product_type }: any) {
    return this.apiService.getCategory({ product_type }).pipe(
      map((res) => {
        if (res['status'] === 'success') {
          return res['data'];
        } else {
          return [];
        }
      })
    );
  }

  toArchive(product_id: number) {
    return this.apiService.toArchive(product_id).pipe(
      tap((res) => {
        // TODO: добавить метод для вывода уведомления
      })
    );
  }

  fromArchive(product_id: number) {
    return this.apiService.fromArchive(product_id).pipe(
      tap((res) => {
        // TODO: добавить метод для вывода уведомления
      })
    );
  }

  createProduct(product) {
    return this.apiService.createProduct({ ...product }).pipe(
      tap((res) => {
        // TODO: добавить метод для вывода уведомления
      })
    );
  }

  editProduct(product) {
    return this.apiService.editProduct(product).pipe(
      tap((res) => {
        // TODO: добавить метод для вывода уведомления
      })
    );
  }

  createCategory({ title, product_type }) {
    return this.apiService.createCategory({ title, product_type }).pipe(
      tap((res) => {
        // TODO: добавить метод для вывода уведомления

        return res;
      })
    );
  }

  editCategory({ title, category_id }) {
    return this.apiService.editCategory({ title, category_id }).pipe(
      tap((res) => {
        // TODO: добавить метод для вывода уведомления
      })
    );
  }
}
