import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ApiService } from './api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PaymentService {
  loading: Subject<boolean> = new Subject<boolean>();
  search: Subject<string> = new Subject<string>();
  data: Subject<{ data: []; offset: number; count: number; total: number }> = new Subject();

  constructor(private apiService: ApiService) {}

  getPayments({ offset = 0, count = 7 }) {
    return this.apiService.getPayments({ offset, count }).pipe(
      map((res) => {
        if (res['status'] === 'success') {
          this.data.next({ data: res['data']['payments'], offset, count, total: res['data']['count'] });
          return true;
        } else {
          this.data.next({ data: [], offset: 0, count: 7, total: 0 });

          return false;
        }
      })
    );
  }
}
