import { Injectable } from '@angular/core';
import {ApiService} from '@core/services/api.service';
import {TranslateService} from '@ngx-translate/core';

interface Industry {
  id: number;
  category: string;
  title: string;
  type: number;
}

interface IndustryCategoriesList {
  label: string;
  items: Array<{
      label: string;
      value: number;
    }>;
}

interface Currency {
  code: string;
  course: number;
  id: number;
  symbol: string;
  title: string;
}

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private api: ApiService,
              private translate: TranslateService) { }


  // getIndustry(): Observable<IndustryCategoriesList[]> {
  //   const business_type = this.store.value.currentCompany.type_id;
  //   const industries_type = business_type === 1 ? 0 : 1;
  //
  //   return this.api.getIndustries().pipe(
  //     map((res): any => res['status'] === 'success' ? res['data'] : [] ),
  //     reduce(((acc: IndustryCategoriesList[], value: Industry): IndustryCategoriesList[] => {
  //
  //       if (!value || value.type !== industries_type) {
  //         return acc;
  //       }
  //       const categoryIndex = acc.findIndex(i => i.label === this.translate.instant(value.category));
  //
  //       if (categoryIndex === -1) {
  //         const category = { label: this.translate.instant(value.category), items: [] };
  //         category.items.push({ label: this.translate.instant(value.title), value: value.id });
  //         acc.push(category);
  //       } else {
  //         acc[categoryIndex].items.push({ label: this.translate.instant(value.title), value: value.id });
  //       }
  //
  //       return acc;
  //     }), []),
  //
  //   );
  // }
  //
  // getCurrencies(): Observable<any> {
  //   return this.api.getCurrencies().pipe(
  //     tap(res => res['status'] === 'success' ? res['data'] : []),
  //     map((currency: Currency) => {
  //       return { label: this.translate.instant(currency.title), value: currency.id };
  //     })
  //   )
  // }
}
