import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { tap } from 'rxjs/operators';
import { CommonDataService } from '@core/services/common-data.service';
import { BehaviorSubject } from 'rxjs';
import { MessageService } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { NotificationMessageService } from './notification-message.service';

interface Notifications {
  id: number;
  type: string;
  text: string;
  date: string;
  is_show: number;
}

@Injectable({
  providedIn: 'root',
})
export class NotificationsService {
  notifications: BehaviorSubject<Notifications[]> = new BehaviorSubject<Notifications[]>([]);
  new_notifications_count = new BehaviorSubject<number>(0);
  new_requests_count = new BehaviorSubject<number>(0);

  constructor(
    private apiService: ApiService,
    private commonDataService: CommonDataService,
    private notificationMessageService: NotificationMessageService,
    private translate: TranslateService
  ) {}

  getNotifications() {
    const notifications = this.notifications.getValue();
    const last_notification_id = notifications.length > 0 ? notifications[notifications.length - 1].id : 0;

    return this.apiService.getNotifications(last_notification_id).pipe(
      tap((res) => {
        if (res['status'] === 'success') {
          const is_executor = this.commonDataService.currentCompany.role_id === 5;
          const is_owner = this.commonDataService.currentCompany.role_id === 4;
          const is_manager = this.commonDataService.currentCompany.role_id === 3;
          const is_administrator = this.commonDataService.currentCompany.role_id === 1;

          const new_notifications = res['data']
            .reduce((result, notification) => {
              const is_notification_exist = notifications.some((i) => i.id === notification.id);

              if (is_notification_exist) {
                return result;
              }

              const is_request = notification.type === 'booking_request';
              const is_json_data = JSON.parse(notification.text);
              const notification_data = is_json_data ? JSON.parse(notification.text) : notification.text;
              const _notification = {
                ...notification,
                text: { ...notification_data, ...this.generateNotificationDetailsByType({ ...notification, text: notification_data }) },
              };

              // if user role is OWNER, MANAGER or ADMINISTRATOR, receive all notifications
              // if user role is EXECUTOR, receive notification that apply to him
              if (is_owner || is_manager || is_administrator) {
                if (notification.is_show !== 1) {
                  if (is_request) {
                    this.new_requests_count.next(this.new_requests_count.getValue() + 1);
                  }
                  this.new_notifications_count.next(this.new_notifications_count.getValue() + 1);
                }
                this.showNotificationMessage(_notification);

                result.push(_notification);
              } else if (is_executor && notification_data.employee_id === this.commonDataService.currentCompany.employee_id) {
                if (notification.is_show !== 1) {
                  if (is_request) {
                    this.new_requests_count.next(this.new_requests_count.getValue() + 1);
                  } else {
                    this.new_notifications_count.next(this.new_notifications_count.getValue() + 1);
                  }
                }
                this.showNotificationMessage(_notification);

                result.push(_notification);
              }

              return result;
            }, [])
            .sort((a, b) => a.id - b.id);

          this.notifications.next([...notifications, ...new_notifications]);
        }
      })
    );
  }

  checkNotification(notification) {
    return this.apiService.showNotification(notification.id).pipe(
      tap((res) => {
        if (res['status'] === 'success') {
          if (notification.type === 'booking_request') {
            this.new_requests_count.next(this.new_requests_count.getValue() - 1);
            this.new_notifications_count.next(this.new_notifications_count.getValue() - 1);
          } else {
            this.new_notifications_count.next(this.new_notifications_count.getValue() - 1);
          }
        }
      })
    );
  }

  clearNotification(notification_id = 0) {
    return this.apiService.clearNotification(notification_id).pipe(
      tap((res) => {
        if (res['status'] === 'success') {
          if (notification_id === 0) {
            this.notifications.next([]);
          } else {
            const notification_index = this.notifications.getValue().findIndex((i) => i.id === notification_id);

            this.notifications.next([
              ...this.notifications.getValue().slice(0, notification_index),
              ...this.notifications.getValue().slice(notification_index + 1),
            ]);
          }
        }
      })
    );
  }

  showNotificationMessage(notification) {
    if (moment(notification.date).diff(moment(), 'seconds') > -10) {
      this.notificationMessageService.pushNotification({
        life: 999999,
        data: this.generateNotificationDetailsByType(notification),
      });
    }
  }

  generateNotificationDetailsByType(notification): { link_pathname?: string; link_title?: string; message: string } {
    const { text, type } = notification;
    let booking_from_date;

    switch (type) {
      case 'allow_date':
        return {
          message: this.translate.instant('notification-allow-date-text'),
        };

      case 'allow_date_soon':
        return {
          message: this.translate.instant('notification-allow-date-soon-text'),
        };

      case 'welcome':
        return {
          message: this.translate.instant('notification-welcome-text'),
        };

      case 'booking_start':
        booking_from_date = moment(text.booking_from_date).format('HH:mm');

        return {
          link_pathname: `/booking/${text.booking_id}`,
          link_title: text.product_title,
          message: `, ${booking_from_date} ${this.translate.instant('notification-booking-start-text')}`,
        };

      case 'booking_end':
        booking_from_date = moment(text.booking_from_date).format('HH:mm');

        return {
          link_pathname: `/booking/${text.booking_id}`,
          link_title: text.product_title,
          message: `, ${booking_from_date} ${this.translate.instant('notification-booking-end-text')}`,
        };

      case 'booking_cancel_time':
        booking_from_date = moment(text.booking_from).format('MMMM DD, YYYY HH:mm');
        return {
          message: `${this.translate.instant('badge-request')} ${text.product_title} ${booking_from_date} ${this.translate.instant(
            'from-client'
          )} ${text.client_first_name} ${text.client_last_name} ${this.translate.instant('notification-booking-cancel-time-text')}`,
        };

      case 'booking_cancel':
        booking_from_date = moment(text.booking_from).format('MMMM DD, YYYY HH:mm');
        return {
          message: `${this.translate.instant('badge-request')} ${text.product_title} ${booking_from_date} ${this.translate.instant(
            'notification-booking-cancel-text'
          )} ${text.client_first_name} ${text.client_last_name}`,
        };

      case 'booking_request':
        booking_from_date = moment(text.booking_from_date).format('MMMM DD, YYYY HH:mm');

        return {
          message: `${text.product_title}, ${booking_from_date} ${this.translate.instant('from-client')} ${text.client_first_name} ${
            text.client_last_name
          }`,
        };

      case 'balance':
        return {
          message: 'notification-lower-balance',
        };

      default:
        return {
          message: '',
        };
    }
  }
}
