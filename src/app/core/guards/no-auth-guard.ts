import { ApiService } from './../services/api.service';
import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';


@Injectable({ providedIn: 'root' })
export class NoAuthGuard implements CanActivate {
    constructor(private router: Router) { }

    canActivate() {
      if (localStorage.getItem('token')) {
        this.router.navigate(['desk']);
        return false;
      }
      return true;
    }
}
