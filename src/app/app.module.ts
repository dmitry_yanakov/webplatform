import { DeskModule } from './pages/desk/desk.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CoreModule } from '@core/core.module';
import { AuthModule } from './pages/auth/auth.module';
import { SocialLoginModule, AuthServiceConfig, FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';
import { UserProfileModule } from './pages/user-profile/user-profile.module';
import { CreateCompanyModule } from './pages/create-company/create-company.module';
import { CompanyProfileModule } from './pages/company-profile/company-profile.module';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { AgmCoreModule } from '@agm/core';
import { EmployeesModule } from './pages/employees/employees.module';
import { HeaderModule, SharedModule } from './shared';
import { ProductsModule } from './pages/products/products.module';
import { PaymentsModule } from './pages/payments/payments.module';
import { ImportsModule } from './pages/imports/imports.module';
import { SmsModule } from './pages/sms/sms.module';
import { FinanceModule } from './pages/finance/finance.module';
import { ClientsModule } from './pages/clients/clients.module';
import { ScheduleModule } from './pages/schedule/schedule.module';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('412543217226-c8cvrml1q3he9ena7m8m2jcf23autnb8.apps.googleusercontent.com'),
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('509165506334626'),
  },
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [AppComponent, NotFoundComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    CoreModule,
    AuthModule,
    SocialLoginModule,
    DeskModule,
    UserProfileModule,
    CreateCompanyModule,
    CompanyProfileModule,
    EmployeesModule,
    ProductsModule,
    PaymentsModule,
    ScheduleModule,
    AppRoutingModule,
    SharedModule,
    HeaderModule,
    ImportsModule,
    SmsModule,
    FinanceModule,
    ClientsModule,
    ToastModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCokv39DsotQK9pui7dzIpbuQXjGHItq-k',
      libraries: ['places'],
    }),
  ],
  exports: [TranslateModule],
  providers: [
    TranslateService,
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig,
    },
    MessageService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
