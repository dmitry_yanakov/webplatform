import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from '@core/services/api.service';
import { TranslateService } from '@ngx-translate/core';
import { SelectItem } from 'primeng/api';

import { CommonDataService, Title } from '@core/services/common-data.service';
import { IPhoneCodes } from '../../../../shared/components/phone-field/phone-field.component';
import { NotificationMessageService } from '@core/services/notification-message.service';

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss'],
})
export class UserSettingsComponent implements OnInit {
  @ViewChild('birth_dateInput', { static: true })
  birth_dateInput: ElementRef;

  userProfile: FormGroup;
  phoneCodes: Array<IPhoneCodes> = [];
  isLanguageOpen = false;
  user_name = '';
  languages: SelectItem[] = [
    { label: 'Русский', value: 'ru' },
    { label: 'English', value: 'en' },
    { label: 'French', value: 'fr' },
  ];
  user;

  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private translate: TranslateService,
    private commonData: CommonDataService,
    private notificationMessageService: NotificationMessageService
  ) {}

  ngOnInit() {
    this.user = this.commonData.user.first_name;
    this.initRegForm();
    this.commonData.changeTitle(Title.profile);
  }

  initRegForm() {
    this.userProfile = this.fb.group({
      avatar: '',
      first_name: this.commonData.user.first_name,
      last_name: this.commonData.user.last_name,
      birth_date: this.commonData.user.birth_date,
      phone: [
        {
          code: this.commonData.user.phone_country,
          phone_code: this.commonData.user.phone_code,
        },
      ],
      phone_number: this.commonData.user.phone_number,
      language: this.commonData.user.language,
    });

    this.apiService.getPhoneCodes().subscribe((codesResp) => {
      this.phoneCodes = codesResp['data'].map(
        (phone): IPhoneCodes => {
          return {
            label: phone.name,
            value: {
              name: this.translate.instant(phone.name),
              code: phone.code.toLowerCase(),
              phone_code: phone.phone_code,
            },
          };
        }
      );

      this.user_name = `${this.commonData.user.first_name} ${this.commonData.user.last_name}`;

      this.apiService.getUserAvatar(this.commonData.user.id).subscribe((response) => this.userProfile.patchValue({ avatar: response }));
    });
  }

  onSubmit() {
    const data = {
      first_name: this.userProfile.value.first_name,
      last_name: this.userProfile.value.last_name,
      birth_date: this.userProfile.value.birth_date,
      phone_code: this.userProfile.value.phone.phone_code,
      phone_country: this.userProfile.value.phone.code,
      phone_number: this.userProfile.value.phone_number,
      language: this.userProfile.value.language,
      avatar: this.userProfile.value.avatar,
    };

    this.apiService.setUser(data).subscribe((res) => {
      if (res['status'] === 'success') {
        this.commonData.user = { ...this.commonData.user, ...data };
        localStorage.setItem('locale', this.userProfile.value.language);
        this.translate.use(this.userProfile.value.language);
        this.notificationMessageService.pushAlert({ detail: 'notification-update-user' });
      }
    });
  }

  rotateIcon() {
    this.isLanguageOpen = !this.isLanguageOpen;
  }
}
