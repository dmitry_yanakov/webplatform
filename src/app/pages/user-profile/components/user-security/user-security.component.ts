import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { ApiService } from '@core/services/api.service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { CommonDataService } from '@core/services/common-data.service';

@Component({
  selector: 'app-user-security',
  templateUrl: './user-security.component.html',
  styleUrls: ['./user-security.component.scss'],
})
export class UserSecurityComponent implements OnInit {
  securityForm: FormGroup;
  showPass = [false, false];

  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private commonData: CommonDataService,
    private translate: TranslateService,
    private router: Router
  ) {}

  ngOnInit() {
    this.initRegForm();
  }

  get f() {
    return this.securityForm.controls;
  }

  initRegForm() {
    this.securityForm = this.fb.group({
      email: [{ value: this.commonData.user.email, disabled: true }],
      oldPass: ['', [Validators.required, Validators.pattern(/^(?=\D*\d)(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z]).{8,30}$/)]],
      newPass: ['', [Validators.required, Validators.pattern(/^(?=\D*\d)(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z]).{8,30}$/)]],
    });
  }

  onSubmit() {
    this.apiService.changePassword(this.f.oldPass.value, this.f.newPass.value).subscribe((response) => {
      this.router.navigate(['/login']);
    });
  }
}
