import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { CalendarModule } from 'primeng/calendar';
import {DropdownModule} from 'primeng/dropdown';
import { OverlayPanelModule } from 'primeng/overlaypanel';

import { UserProfileComponent } from './user-profile.component';
import { UserSettingsComponent } from './components/user-settings/user-settings.component';
import { UserSecurityComponent } from './components/user-security/user-security.component';

import {SharedModule, ScrollPanelModule, CropperModule, HeaderModule} from '../../shared';




@NgModule({
  declarations: [UserProfileComponent, UserSettingsComponent, UserSecurityComponent],
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    CalendarModule,
    DropdownModule,
    OverlayPanelModule,
    CropperModule,
    ScrollPanelModule,
    HeaderModule,
  ]
})
export class UserProfileModule { }
