import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ProductService } from '@core/services/product.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import * as moment from 'moment';
import { NotificationMessageService } from '@core/services/notification-message.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss'],
})
export class EditProductComponent implements OnInit, OnDestroy {
  isReady = false;
  productForm: FormGroup;
  product_type_name: string;
  product_type: number;
  product_id: number;
  product;
  product_create_token = ['edit-good', 'edit-service', 'edit-pass', 'edit-class', 'edit-service-package'];
  product_edit_notifications = [
    'notification-update-good',
    'notification-update-service',
    'notification-update-pass',
    'notification-update-class',
    'notification-update-service-package',
  ];

  unsubscribe: Subject<any> = new Subject<any>();

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private productService: ProductService,
    private notificationMessageService: NotificationMessageService
  ) {}

  ngOnInit() {
    this.productForm = this.fb.group({});

    this.activatedRoute.params.subscribe((params) => {
      this.product_type_name = params['product-type'];
      this.product_id = params['product-id'];
    });

    this.productService.product_type.pipe(takeUntil(this.unsubscribe)).subscribe((product_type) => {
      this.product_type = product_type;
    });
  }

  goBack() {
    this.location.back();
  }

  // After a form is initialized, we link it to our main form
  formInitialized(form: FormGroup) {
    this.productForm = form;

    this.isReady = true;
  }

  onSubmit() {
    let data = {
      ...this.productForm.value,
      type: this.product_type,
      product_id: this.product_id,
    };

    if (this.product_type === 2 || this.product_type === 4) {
      const time = this.productForm.value.time;
      const lifetime_type = this.productForm.value.lifetime_type;

      data = {
        ...data,
        time: lifetime_type === 1 ? `${time}D` : lifetime_type === 2 ? `${time}M` : moment(time).format('YYYY-MM-DD'),
      };
    }

    this.productService.editProduct(data).subscribe((res) => {
      if (res['status'] === 'success') {
        this.location.back();
        this.notificationMessageService.pushAlert({ detail: this.product_edit_notifications[this.product_type] });
      }
    });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
