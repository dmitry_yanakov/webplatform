import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { ProductService } from '@core/services/product.service';
import { takeUntil } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { NotificationMessageService } from '@core/services/notification-message.service';

@Component({
  selector: 'app-category-edit',
  templateUrl: './category-edit.component.html',
  styleUrls: ['./category-edit.component.scss'],
})
export class CategoryEditComponent implements OnInit, OnDestroy {
  categoryForm: FormGroup;

  unsubscribe: Subject<any> = new Subject<any>();

  @Output() categoryEdited = new EventEmitter<{ id: number; title: string | number }>();
  @Output() close = new EventEmitter<any>();
  @Input() editingCategory;

  constructor(
    private fb: FormBuilder,
    private productService: ProductService,
    private notificationMessageService: NotificationMessageService
  ) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    console.log(this.editingCategory);
    this.categoryForm = this.fb.group({
      title: [this.editingCategory.title, Validators.required],
    });
  }

  onSubmit() {
    this.productService
      .editCategory({ title: this.categoryForm.value.title, category_id: this.editingCategory.id })
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((res) => {
        if (res['status'] === 'success') {
          this.categoryEdited.emit({ id: this.editingCategory.id, title: this.categoryForm.value.title });
          this.notificationMessageService.pushAlert({ detail: 'notification-update-category' });
        }
      });
  }

  goBack() {
    this.close.emit();
  }

  ngOnDestroy(): void {
    this.categoryForm.patchValue({
      title: '',
    });

    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
