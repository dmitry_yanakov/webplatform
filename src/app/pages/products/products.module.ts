import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { TooltipModule } from 'primeng/tooltip';
import { HeaderModule } from '@modules';
import { RouterModule } from '@angular/router';
import { DataViewModule } from 'primeng/dataview';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { ProductsComponent } from './products.component';
import { TableGoodsComponent } from './products-lists/table-goods/table-goods.component';
import { TableServicePackagesComponent } from './products-lists/table-service-packages/table-service-packages.component';
import { TableServicesComponent } from './products-lists/table-services/table-services.component';
import { TablePassesComponent } from './products-lists/table-passes/table-passes.component';
import { TableClassesComponent } from './products-lists/table-classes/table-classes.component';
import { TableArchiveComponent } from './products-lists/table-archive/table-archive.component';
import { ScrollPanelModule, SharedModule } from '../../shared';
import { FormGoodComponent } from './components/forms/form-good/form-good.component';
import { ProductsListsComponent } from './products-lists/products-lists.component';
import { CreateProductComponent } from './create/create-product/create-product.component';
import { DropdownModule } from 'primeng/dropdown';
import { FormServiceComponent } from './components/forms/form-service/form-service.component';
import { FormServicePackageComponent } from './components/forms/form-service-package/form-service-package.component';
import { FormPassComponent } from './components/forms/form-pass/form-pass.component';
import { FormClassComponent } from './components/forms/form-class/form-class.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CategoryCreateComponent } from './create/category-create/category-create.component';
import { CategoryEditComponent } from './edit/category-edit/category-edit.component';
import { EditProductComponent } from './edit/edit-product/edit-product.component';
import { FormPassGeneralComponent } from './components/forms/form-pass/components/form-pass-general/form-pass-general.component';
import { FormPassRelatedClassesComponent } from './components/forms/form-pass/components/form-pass-related-classes/form-pass-related-classes.component';
import { FormClassGeneralComponent } from './components/forms/form-class/components/form-class-general/form-class-general.component';
import { FormClassEmployeesComponent } from './components/forms/form-class/components/form-class-employees/form-class-employees.component';
import { FormClassRelatedPassesComponent } from './components/forms/form-class/components/form-class-related-passes/form-class-related-passes.component';
import { FormServiceGeneralComponent } from './components/forms/form-service/components/form-service-general/form-service-general.component';
import { FormServiceEmployeesComponent } from './components/forms/form-service/components/form-service-employees/form-service-employees.component';

@NgModule({
  declarations: [
    ProductsComponent,
    TableGoodsComponent,
    TableServicePackagesComponent,
    TableServicesComponent,
    TablePassesComponent,
    TableClassesComponent,
    TableArchiveComponent,
    ProductsListsComponent,
    CreateProductComponent,
    FormGoodComponent,
    FormServiceComponent,
    FormServicePackageComponent,
    FormPassComponent,
    FormClassComponent,
    CategoryCreateComponent,
    CategoryEditComponent,
    EditProductComponent,
    FormPassGeneralComponent,
    FormPassRelatedClassesComponent,
    FormClassGeneralComponent,
    FormClassEmployeesComponent,
    FormClassRelatedPassesComponent,
    FormServiceGeneralComponent,
    FormServiceEmployeesComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    HeaderModule,
    RouterModule,
    ScrollPanelModule,
    DataViewModule,
    OverlayPanelModule,
    DropdownModule,
    NgSelectModule,
    ReactiveFormsModule,
    SharedModule,
    FormsModule,
    TooltipModule,
  ],
})
export class ProductsModule {}
