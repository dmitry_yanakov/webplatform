import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ProductService } from '@core/services/product.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import * as moment from 'moment';
import { NotificationMessageService } from '@core/services/notification-message.service';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.scss'],
})
export class CreateProductComponent implements OnInit, OnDestroy {
  isReady = false;
  productForm: FormGroup;
  product_type_name: string;
  product_type: number;
  product_create_token = ['new-good', 'new-service', 'new-pass', 'new-class', 'new-service-package'];
  product_create_notifications = [
    'notification-create-good',
    'notification-create-service',
    'notification-create-pass',
    'notification-create-class',
    'notification-create-service-package',
  ];

  unsubscribe: Subject<any> = new Subject<any>();

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private productService: ProductService,
    private notificationMessageService: NotificationMessageService
  ) {}

  ngOnInit() {
    this.productForm = this.fb.group({});
    this.activatedRoute.params.subscribe((params) => {
      this.product_type_name = params['product-type'];
    });
    this.productService.product_type.pipe(takeUntil(this.unsubscribe)).subscribe((product_type) => {
      this.product_type = product_type;
    });
    this.isReady = true;
  }

  goBack() {
    this.location.back();
  }

  // After a form is initialized, we link it to our main form
  formInitialized(form: FormGroup) {
    this.productForm = form;
  }

  onSubmit() {
    const data = {
      ...this.productForm.value,
      type: this.product_type,
    };

    if (this.product_type === 2 || this.product_type === 4) {
      const time = this.productForm.value.time;
      const lifetime_type = this.productForm.value.lifetime_type;

      data.time = lifetime_type === 1 ? `${time}D` : lifetime_type === 2 ? `${time}M` : moment(time).format('YYYY-MM-DD');
    }

    this.productService
      .createProduct(data)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((res) => {
        if (res['status'] === 'success') {
          this.location.back();
          this.notificationMessageService.pushAlert({ detail: this.product_create_notifications[this.product_type] });
        }
      });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
