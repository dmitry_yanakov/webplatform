import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { ProductService } from '@core/services/product.service';
import { takeUntil } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { NotificationMessageService } from '@core/services/notification-message.service';

@Component({
  selector: 'app-category-create',
  templateUrl: './category-create.component.html',
  styleUrls: ['./category-create.component.scss'],
})
export class CategoryCreateComponent implements OnInit, OnDestroy {
  categoryForm: FormGroup;

  unsubscribe: Subject<any> = new Subject<any>();

  @Output() categoryCreated = new EventEmitter<{ id: number; title: string | number }>();
  @Output() close = new EventEmitter<any>();
  @Input() product_type: number;

  constructor(
    private fb: FormBuilder,
    private productService: ProductService,
    private notificationMessageService: NotificationMessageService
  ) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.categoryForm = this.fb.group({
      title: ['', Validators.required],
    });
  }

  onSubmit() {
    this.productService
      .createCategory({ title: this.categoryForm.value.title, product_type: this.product_type })
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((res) => {
        if (res['status'] === 'success') {
          this.categoryCreated.emit({ id: res['data']['category_id'], title: this.categoryForm.value.title });
          this.notificationMessageService.pushAlert({ detail: 'notification-create-category' });
        }
      });
  }

  goBack() {
    this.close.emit();
  }

  ngOnDestroy(): void {
    this.categoryForm.patchValue({
      title: '',
    });

    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
