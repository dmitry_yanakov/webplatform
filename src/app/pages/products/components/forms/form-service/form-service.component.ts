import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { ProductService } from '@core/services/product.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { StaffService } from '@core/services/staff.service';
import { ActivatedRoute } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject, forkJoin, of } from 'rxjs';

@Component({
  selector: 'app-form-service',
  templateUrl: './form-service.component.html',
  styleUrls: ['./form-service.component.scss'],
})
export class FormServiceComponent implements OnInit, OnDestroy {
  serviceForm: FormGroup;
  selected_tab = 0;
  product_id: number;
  product_type = 1;

  @Output() formReady = new EventEmitter<FormGroup>();

  unsubscribe: Subject<any> = new Subject<any>();

  constructor(
    private fb: FormBuilder,
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      this.selected_tab = Number(params['selected-tab']) || 0;
      this.product_id = params['product-id'];
    });

    this.productService.setProductType(1);
    this.initForm();
  }

  initForm() {
    this.serviceForm = this.fb.group({
      title: ['', Validators.required],
      category_id: ['', Validators.required],
      duration: ['', Validators.required],
      price: ['', Validators.required],
      price_max: [''],
      flexible_price: false,
      description: [''],
      employees: [[]],
      relations: [[]],
    });


    // При редактировании передается product_id товара
    // запрашиваем данные по редактироваемому товару и подставляем в форму
    if (this.product_id) {
      this.productService
        .getProducts({
          product_type: this.product_type,
          product_id: this.product_id,
        })
        .pipe(takeUntil(this.unsubscribe))
        .subscribe((data) => {
          const product = data.products[0];

          const hours = Math.floor(product.duration / 60);
          const minutes = Math.floor(product.duration % 60);
          const flexible_price = Number(product.price_max) > 0;

          this.serviceForm.patchValue({
            ...product,
            duration: `${hours < 10 ? '0' + hours : hours}:${minutes}`,
            employees: product.employees ? product.employees.map((employee) => employee.employee_id) : [],
            flexible_price,
          });
        });
    }

    this.formReady.emit(this.serviceForm);
  }

  switchTab(selected_tab) {
    this.selected_tab = selected_tab;
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
