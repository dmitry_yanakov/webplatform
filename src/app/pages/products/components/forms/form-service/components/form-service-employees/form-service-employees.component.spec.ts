import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormServiceEmployeesComponent } from './form-service-employees.component';

describe('FormServiceEmployeesComponent', () => {
  let component: FormServiceEmployeesComponent;
  let fixture: ComponentFixture<FormServiceEmployeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormServiceEmployeesComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormServiceEmployeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
