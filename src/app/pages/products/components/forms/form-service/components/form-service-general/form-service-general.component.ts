import { Component, Input, OnInit } from '@angular/core';
import { ProductService } from '@core/services/product.service';
import { take } from 'rxjs/operators';
import { FormGroup } from '@angular/forms';
import { CommonDataService } from '@core/services/common-data.service';

@Component({
  selector: 'app-form-service-general',
  templateUrl: './form-service-general.component.html',
  styleUrls: ['./form-service-general.component.scss'],
})
export class FormServiceGeneralComponent implements OnInit {
  isReady = false;
  _generalForm: FormGroup;

  categories = [];
  selectedCategory;
  currency_symbol = '';

  @Input() generalForm;

  constructor(private productService: ProductService, private commonDataService: CommonDataService) {}

  ngOnInit() {
    this.currency_symbol = this.commonDataService.currentCompany.currency_symbol;
    this.initForm();
    this.getProductCategories();
  }

  initForm() {
    this._generalForm = this.generalForm;
  }

  getProductCategories() {
    this.productService
      .getProductCategories({ product_type: 1 })
      .pipe(take(1))
      .subscribe((categories) => {
        this.categories = categories;

        if (categories.length === 1) {
          this.selectedCategory = categories[0].id;
          this._generalForm.get('category_id').setValue(categories[0].id);
        }

        this.isReady = true;
      });
  }

  pushCategory(newCategory) {
    this.categories = [...this.categories, newCategory];

    this._generalForm.patchValue({
      category_id: newCategory.id,
    });
  }

  replaceCategory(editedCategory) {
    const _editedCategoryIndex = this.categories.findIndex((category) => category.id === editedCategory.id);

    this.categories = [
      ...this.categories.slice(0, _editedCategoryIndex),
      ...this.categories.slice(_editedCategoryIndex + 1),
      editedCategory,
    ].sort((a, b) => a.id - b.id);

    this._generalForm.patchValue({
      category_id: editedCategory.id,
    });
  }

  flexPriceChange(status) {
    if (status === false) {
      this._generalForm.patchValue({
        price_max: '',
      });
    }
    this._generalForm.patchValue({
      flexible_price: status,
    });
  }

  descriptionChange(value) {
    if (this.description.length < 128) {
      this._generalForm.patchValue({
        description: value,
      });
    }
  }

  setPrice(value) {
    this._generalForm.patchValue({
      price: value ? (value * 100).toFixed(0) : '',
    });
  }

  setMaxPrice(value) {
    this._generalForm.patchValue({
      price_max: value ? (value * 100).toFixed(0) : '',
    });
  }

  get price() {
    return this._generalForm.get('price').value;
  }

  get price_max() {
    return this._generalForm.get('price_max').value;
  }

  get flexible_price() {
    return this._generalForm.get('flexible_price').value;
  }

  get description() {
    return this._generalForm.get('description').value;
  }
}
