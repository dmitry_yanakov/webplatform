import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormServiceGeneralComponent } from './form-service-general.component';

describe('FormServiceGeneralComponent', () => {
  let component: FormServiceGeneralComponent;
  let fixture: ComponentFixture<FormServiceGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormServiceGeneralComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormServiceGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
