import {Component, EventEmitter, OnInit, Output, OnDestroy} from '@angular/core';
import { CommonDataService } from '@core/services/common-data.service';
import { ProductService } from '@core/services/product.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {take, takeUntil} from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/internal/Subject';

@Component({
  selector: 'app-form-good',
  templateUrl: './form-good.component.html',
  styleUrls: ['./form-good.component.scss'],
})
export class FormGoodComponent implements OnInit, OnDestroy {
  productForm: FormGroup;
  product_type = 0;
  product_id: number;
  currency_symbol: string;
  extra_charge_types: { label: string; value: number }[];
  categories = [];
  selectedCategory;

  @Output() formReady = new EventEmitter<FormGroup>();

  unsubscribe: Subject<any> = new Subject<any>();

  constructor(
    private fb: FormBuilder,
    private commonDataService: CommonDataService,
    private productService: ProductService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.activatedRoute.params.pipe().subscribe((params) => {
      this.product_id = params['product-id'];
    });

    this.productService.setProductType(0);
    this.initForm();
    this.getProductCategories();

    this.currency_symbol = this.commonDataService.currentCompany.currency_symbol;
    this.extra_charge_types = [
      { label: '%', value: 1 },
      { label: this.currency_symbol, value: 2 },
    ];
  }

  initForm() {
    this.productForm = this.fb.group({
      title: ['', Validators.required],
      category_id: [null, [Validators.required]],
      cost: [null],
      extra_charge: [null],
      price: [null, [Validators.required]],
      count: [1],
      vendor: '',
      description: '',
      selected_charge_type: 1,
    });

    this.productForm.valueChanges.subscribe(value => {
      console.log(value);
    });
    // При редактировании передается product_id товара
    // запрашиваем данные по редактироваемому товару и подставляем в форму
    if (this.product_id) {
      this.productService
        .getProducts({
          product_type: this.product_type,
          product_id: this.product_id,
        })
        .pipe(takeUntil(this.unsubscribe))
        .subscribe((data) => {
          const product = data.products[0];
          const extra_charge = product.cost > 1 ? (product.price - product.cost) : 0;

            this.productForm.patchValue({
              ...product,
              selected_charge_type: 2,
              cost: product.cost > 1 ? product.cost : product.price,
              extra_charge,
            });
        });
    }

    this.formReady.emit(this.productForm);
  }

  getProductCategories() {
    this.productService
      .getProductCategories({ product_type: 0 })
      .pipe(take(1))
      .subscribe((categories) => {
        this.categories = categories;
      });
  }

  pushCategory(newCategory) {
    this.categories = [...this.categories, newCategory];

    this.productForm.patchValue({
      category_id: newCategory.id,
    });
  }

  replaceCategory(editedCategory) {
    const _editedCategoryIndex = this.categories.findIndex((category) => category.id === editedCategory.id);

    this.categories = [
      ...this.categories.slice(0, _editedCategoryIndex),
      ...this.categories.slice(_editedCategoryIndex + 1),
      editedCategory,
    ].sort((a, b) => a.id - b.id);

    this.productForm.patchValue({
      category_id: editedCategory.id,
    });
  }

  supplyPriceChange(value) {
    this.productForm.patchValue({
      cost: (value * 100).toFixed(0),
    });

    if (this.extra_charge.value) {
      const cost = Number(value);
      const extra_charge = Number(this.extra_charge.value);

      if (this.selected_charge_type.value === 1) {
        this.productForm.patchValue({
          price: (cost * 100 + cost * extra_charge).toFixed(0),
        });
      } else if (this.selected_charge_type.value === 2) {
        this.productForm.patchValue({
          price: (cost * 100 + extra_charge).toFixed(0),
        });
      }
    }
  }

  extraChargeChange(value) {
    if (!value) {
      this.productForm.patchValue({
        extra_charge: null,
        price: null,
      });

      return;
    }

    this.productForm.patchValue({
      extra_charge: this.selected_charge_type.value === 1 ? Number(value) : (Number(value) * 100).toFixed(0),
    });

    if (this.cost.value) {
      const cost = Number(this.cost.value);
      const extra_charge = Number(value);

      if (this.selected_charge_type.value === 1) {
        this.productForm.patchValue({
          price: (cost + (cost / 100) * extra_charge).toFixed(0),
        });
      } else if (this.selected_charge_type.value === 2) {
        this.productForm.patchValue({
          price: (extra_charge * 100 + cost).toFixed(0),
        });
      }
    }
  }

  extraChargeTypeChange(value) {
    this.productForm.patchValue({
      extra_charge: value === 1 ? Number(this.extra_charge.value) / 100 : Number(this.extra_charge.value) * 100,
    });

    if (this.cost.value) {
      const cost = Number(this.cost.value);
      const extra_charge = Number(this.extra_charge.value);

      if (value === 1) {
        this.productForm.patchValue({
          price: (cost + (cost / 100) * extra_charge).toFixed(0),
        });
      } else if (value === 2) {
        this.productForm.patchValue({
          price: (extra_charge + cost).toFixed(0),
        });
      }
    }
  }

  priceChange(value) {
    this.productForm.patchValue({
      price: (value * 100).toFixed(0),
    });

    if (this.cost.value) {
      const cost = Number(this.cost.value);
      const price = Number(value);

      if (this.selected_charge_type.value === 1) {
        this.productForm.patchValue({
          extra_charge: ((value * 100 * 100) / cost - 100).toFixed(0),
        });
      } else if (this.selected_charge_type.value === 2) {
        this.productForm.patchValue({
          extra_charge: (price * 100 - cost).toFixed(0),
        });
      }
    }
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  get cost() {
    return this.productForm.get('cost');
  }

  get extra_charge() {
    return this.productForm.get('extra_charge');
  }

  get price() {
    return this.productForm.get('price');
  }

  get selected_charge_type() {
    return this.productForm.get('selected_charge_type');
  }

  get description() {
    return this.productForm.get('description').value;
  }
}
