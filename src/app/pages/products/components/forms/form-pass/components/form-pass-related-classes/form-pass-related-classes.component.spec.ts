import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormPassRelatedClassesComponent } from './form-pass-related-classes.component';

describe('FormPassRelatedClassesComponent', () => {
  let component: FormPassRelatedClassesComponent;
  let fixture: ComponentFixture<FormPassRelatedClassesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormPassRelatedClassesComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormPassRelatedClassesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
