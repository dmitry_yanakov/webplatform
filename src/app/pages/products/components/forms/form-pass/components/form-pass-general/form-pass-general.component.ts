import { Component, OnInit, ViewChild, Input, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonDataService } from '@core/services/common-data.service';
import { OverlayPanel } from 'primeng/overlaypanel';

@Component({
  selector: 'app-form-pass-general',
  templateUrl: './form-pass-general.component.html',
  styleUrls: ['./form-pass-general.component.scss'],
})
export class FormPassGeneralComponent implements OnInit {
  passGeneralForm: FormGroup;
  is_collapsed = true;

  pass_start_types = [
    { label: 'purchase', value: 1 },
    { label: 'first_use', value: 2 },
    { label: 'date', value: 3 },
  ];

  pass_lifetime_types = [
    { label: 'day', value: 1 },
    { label: 'month', value: 2 },
    { label: 'date', value: 3 },
  ];

  currency_symbol: string;

  @Input() generalForm;

  @ViewChild('lifetimeCalendar', { static: false }) lifetimeCalendar: OverlayPanel;
  @ViewChild('targetField', { static: false }) targetField: ElementRef;
  constructor(private fb: FormBuilder, private commonDataService: CommonDataService) {}

  ngOnInit() {
    this.initForm();

    this.currency_symbol = this.commonDataService.currentCompany.currency_symbol;
  }

  // Инициализирует форму вида:
  // generalForm: this.fb.group({
  //   title: ['', Validators.required],
  //   price: ['', Validators.required],
  //   limit: [null],
  //   time: [null],
  //   activate: [null],
  //   lifetime_type: [1],
  //   start_type: [1],
  // }),
  initForm() {
    this.passGeneralForm = this.generalForm;
  }

  limitStatusChange(value) {
    if (value) {
      this.passGeneralForm.patchValue({
        limit: '',
        limit_status: true,
      });

      return;
    }

    this.passGeneralForm.patchValue({
      limit: -1,
      limit_status: false,
    });
  }

  priceChange(value) {
    this.passGeneralForm.patchValue({
      cost: value * 100,
      price: value * 100,
    });
  }

  startTypeChange(value) {
    if (this.start_type === 1 || this.start_type === 2) {
      this.passGeneralForm.patchValue({
        activate: value,
        start_type: value,
      });

      return;
    }

    if (this.start_type === 3) {
      this.passGeneralForm.patchValue({
        activate: '',
      });
    }
  }

  lifetimeTypeChange(event) {
    this.passGeneralForm.patchValue({
      time: '',
      lifetime_type: event.value,
    });

    if (event.value === 3) {
      this.lifetimeCalendar.show(event, this.targetField.nativeElement);
    }
  }

  get lifetime_type() {
    return this.passGeneralForm.get('lifetime_type').value;
  }

  get start_type() {
    return this.passGeneralForm.get('start_type').value;
  }

  get price() {
    return this.passGeneralForm.get('price').value;
  }

  get limit() {
    return this.passGeneralForm.get('limit').value;
  }

  get limit_status() {
    return this.passGeneralForm.get('limit_status').value;
  }

  get activate() {
    return this.passGeneralForm.get('activate').value;
  }

  get description() {
    return this.passGeneralForm.get('description').value;
  }
}
