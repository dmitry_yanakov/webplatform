import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormPassGeneralComponent } from './form-pass-general.component';

describe('FormPassGeneralComponent', () => {
  let component: FormPassGeneralComponent;
  let fixture: ComponentFixture<FormPassGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormPassGeneralComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormPassGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
