import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { Subject, of, forkJoin } from 'rxjs';
import { catchError, takeUntil } from 'rxjs/operators';
import { ProductService } from '@core/services/product.service';

@Component({
  selector: 'app-form-pass-related-classes',
  templateUrl: './form-pass-related-classes.component.html',
  styleUrls: ['./form-pass-related-classes.component.scss'],
})
export class FormPassRelatedClassesComponent implements OnInit, OnDestroy {
  relatedServicesForm;
  isLoading = true;
  classes = [];
  categories = [];
  filteredProductsByCategory = [];
  searchValue$: Subject<string> = new Subject<string>();

  @Input() productsForm: AbstractControl;

  private unsubscribe: Subject<any> = new Subject<any>();

  constructor(private productService: ProductService) {}

  ngOnInit() {
    this.initForm();
    this.initProducts();

    this.searchValue$.pipe(takeUntil(this.unsubscribe)).subscribe((searchValue) => {
      this.getProductsFilteredBySearchValue(searchValue);
    });
  }

  initForm() {
    this.relatedServicesForm = this.productsForm;
  }

  initProducts() {
    forkJoin([
      this.productService.getProducts({ product_type: 3 }),
      this.productService.getProductCategories({ product_type: 3 }),
    ])
      .pipe(
        catchError(() => {
          return of([[], []]);
        }),
        takeUntil(this.unsubscribe)
      )
      .subscribe(([data, categories]) => {
        this.categories = categories;
        this.classes = data && data.products ? data.products : [];

        this.getProductsFilteredByCategory();
        this.isLoading = false;
      });
  }

  getProductsFilteredByCategory() {
    this.filteredProductsByCategory = this.categories.reduce((acc, category) => {
      if (this.classes.some((i) => i.category_id === category.id)) {
        const categoryProducts = this.classes
          .filter((product) => product.category_id === category.id)
          .map((product) => {
            const isSelected = this.relatedServicesForm.value.some((product_id) => product_id === product.id);
            return { ...product, selected: isSelected, visible: true };
          });

        acc.push({ category_title: category.title, is_category_visible: true, products: categoryProducts });
      }

      return acc;
    }, []);
  }

  // Перебирает продукты при поиске
  // Назначает статус visible = true только тем, которые соответствуют поисковому запросу
  // Если ни один не соответсует поисковому запросу, скрывает катекорию со всеми продуктами
  getProductsFilteredBySearchValue(searchValue: string = '') {
    this.filteredProductsByCategory.forEach((category) => {
      category.products.forEach((categoryProduct) => {
        categoryProduct.visible = categoryProduct.title.toLowerCase().indexOf(searchValue) !== -1;
      });

      category.is_category_visible = !category.products.every((categoryProduct) => !categoryProduct.visible);
    });
  }

  toggleProduct(product) {
    const arrayId = this.relatedServicesForm.value.findIndex((i) => i === product.id);

    if (arrayId === -1) {
      this.relatedServicesForm.setValue([...this.relatedServicesForm.value, product.id]);
    } else {
      this.relatedServicesForm.setValue([
        ...this.relatedServicesForm.value.slice(0, arrayId),
        ...this.relatedServicesForm.value.slice(arrayId + 1),
      ]);
    }

    product.selected = arrayId === -1;
  }

  toggleAllProducts() {
    if (this.classes.length === this.relatedServicesForm.value.length) {
      this.relatedServicesForm.setValue([]);
      this.setAllProductSelectStatus(false);
    } else {
      const allClasses = this.classes.map((product) => product.id);

      this.relatedServicesForm.setValue(allClasses);
      this.setAllProductSelectStatus(true);
    }
  }

  setAllProductSelectStatus(status) {
    this.filteredProductsByCategory.forEach((category) => {
      category.products.forEach((product) => {
        product.selected = status;
      });
    });
  }

  searchProducts(text) {
    this.searchValue$.next(text.toLowerCase());
  }

  clearSearchField() {
    return this.searchValue$.next('');
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  get isAllSelected() {
    return this.classes.length === this.relatedServicesForm.value.length;
  }

  get isNoResults() {
    return this.filteredProductsByCategory.every((category) => !category.is_category_visible);
  }
}
