import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { ProductService } from '@core/services/product.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import * as moment from 'moment';

@Component({
  selector: 'app-form-pass',
  templateUrl: './form-pass.component.html',
  styleUrls: ['./form-pass.component.scss'],
})
export class FormPassComponent implements OnInit, OnDestroy {
  passForm: FormGroup;
  selected_tab = 0;
  product_id: number;
  product_type = 2;
  isReady = false;

  @Output() formReady = new EventEmitter<FormGroup>();

  unsubscribe: Subject<any> = new Subject<any>();

  constructor(
    private fb: FormBuilder,
    private productService: ProductService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.activatedRoute.params.pipe().subscribe((params) => {
      this.selected_tab = Number(params['selected-tab']) || 0;
      this.product_id = params['product-id'];
    });

    this.productService.setProductType(2);
    this.initForm();
  }

  initForm() {
    this.passForm = this.fb.group({
      title: ['', Validators.required],
      cost: ['', Validators.required],
      price: ['', Validators.required],
      limit: [null, Validators.required],
      limit_status: [false],
      time: [null, Validators.required],
      activate: [1],
      lifetime_type: [1],
      start_type: [1],
      relations: [[]],
      description: [''],
    });

    // При редактировании передается product_id абонемента
    // запрашиваем данные по редактироваемому абонементу и подставляем в форму
    if (this.product_id) {
      this.productService
        .getProducts({
          product_type: this.product_type,
          product_id: this.product_id,
        })
        .pipe(takeUntil(this.unsubscribe))
        .subscribe((data) => {
          let classItem;

          if (data && data.products && data.products.length) {
            classItem = { ...data.products[0], employees: data.products[0].employees || [] };

            this.passForm.patchValue({
              ...classItem,
              lifetime_type: classItem.time.indexOf('D') !== -1 ? 1 : classItem.time.indexOf('M') !== -1 ? 2 : 3,
              start_type: typeof classItem.activate === 'string' ? 3 : classItem.activate,
              relations: classItem.relations ? classItem.relations.map((relation) => relation.product_id) : [],
              time:
                classItem.time.indexOf('D') !== -1
                  ? parseInt(classItem.time, 10)
                  : classItem.time.indexOf('M') !== -1
                  ? parseInt(classItem.time, 10)
                  : moment(classItem.time).format('DD / MM / YYYY'),
            });
          }
        });
    }

    this.formReady.emit(this.passForm);
  }

  switchTab(selected_tab) {
    this.selected_tab = selected_tab;
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
