import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormServicePackageComponent } from './form-service-package.component';

describe('FormServicePackageComponent', () => {
  let component: FormServicePackageComponent;
  let fixture: ComponentFixture<FormServicePackageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormServicePackageComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormServicePackageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
