import { Component, EventEmitter, OnInit, Output, OnDestroy } from '@angular/core';
import { CommonDataService } from '@core/services/common-data.service';
import { ProductService } from '@core/services/product.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-form-service-package',
  templateUrl: './form-service-package.component.html',
  styleUrls: ['./form-service-package.component.scss'],
})
export class FormServicePackageComponent implements OnInit, OnDestroy {
  servicePackageForm: FormGroup;
  product_type = 4;
  product_id: number;
  currency_symbol: string;
  services = [];
  categories = [];
  selectedCategory;
  lifetime_types = [
    { label: 'day', value: 1 },
    { label: 'month', value: 2 },
    { label: 'date', value: 3 },
  ];

  @Output() formReady = new EventEmitter<FormGroup>();

  unsubscribe: Subject<any> = new Subject<any>();

  constructor(
    private fb: FormBuilder,
    private commonDataService: CommonDataService,
    private productService: ProductService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      this.product_id = params['product-id'];
    });

    this.productService.setProductType(4);
    this.initForm();
    this.getProductCategories();
    this.getServices();
    this.currency_symbol = this.commonDataService.currentCompany.currency_symbol;
  }

  getServices() {
    this.productService
      .getProducts({ product_type: 1, count: 999 })
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((data) => {
        this.services = data.products.map((product) => ({ label: product.title, value: product.id }));
      });
  }

  initForm() {
    this.servicePackageForm = this.fb.group({
      title: ['', Validators.required],
      product_id: [null],
      price: ['', [Validators.required]],
      time: ['', [Validators.required]],
      activate: [1, Validators.required],
      limit: ['', [Validators.required]],
      lifetime_type: [1],
      relations: [[]],
    });

    // При редактировании передается product_id товара
    // запрашиваем данные по редактироваемому товару и подставляем в форму
    if (this.product_id) {
      this.productService
        .getProducts({
          product_type: this.product_type,
          product_id: this.product_id,
        })
        .pipe(takeUntil(this.unsubscribe))
        .subscribe((data) => {
          const product = data.products[0];

          const time = product.time;

          this.servicePackageForm.patchValue({
            ...product,
            lifetime_type: time.indexOf('D') !== -1 ? 1 : time.indexOf('M') !== -1 ? 2 : 3,
            relations: product.relations ? product.relations.map((relation) => relation.product_id) : [],
            time:
              time.indexOf('D') !== -1
                ? parseInt(time, 10)
                : time.indexOf('M') !== -1
                ? parseInt(time, 10)
                : moment(time).format('DD / MM / YYYY'),
          });
        });
    }

    this.formReady.emit(this.servicePackageForm);
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  priceChange(value) {
    this.servicePackageForm.patchValue({
      price: value * 100,
    });
  }

  lifetimeTypeChange() {
    this.servicePackageForm.patchValue({
      time: null,
    });
  }

  relationChange(value) {
    this.servicePackageForm.patchValue({
      relations: [value],
    });
  }

  get price() {
    return this.servicePackageForm.get('price').value;
  }

  get lifetime_type() {
    return this.servicePackageForm.get('lifetime_type').value;
  }

  getProductCategories() {
    this.productService
      .getProductCategories({ product_type: 4 })
      .pipe(take(1))
      .subscribe((categories) => {
        this.categories = categories;
      });
  }

  pushCategory(newCategory) {
    this.categories = [...this.categories, newCategory];

    this.servicePackageForm.patchValue({
      category_id: newCategory.id,
    });
  }

  replaceCategory(editedCategory) {
    const _editedCategoryIndex = this.categories.findIndex((category) => category.id === editedCategory.id);

    this.categories = [
      ...this.categories.slice(0, _editedCategoryIndex),
      ...this.categories.slice(_editedCategoryIndex + 1),
      editedCategory,
    ].sort((a, b) => a.id - b.id);

    this.servicePackageForm.patchValue({
      category_id: editedCategory.id,
    });
  }
}
