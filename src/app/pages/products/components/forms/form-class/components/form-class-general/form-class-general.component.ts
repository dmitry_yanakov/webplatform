import { Component, Input, OnInit } from '@angular/core';
import { ProductService } from '@core/services/product.service';
import { take } from 'rxjs/operators';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-class-general',
  templateUrl: './form-class-general.component.html',
  styleUrls: ['./form-class-general.component.scss'],
})
export class FormClassGeneralComponent implements OnInit {
  _generalForm: FormGroup;
  categories = [];
  selectedCategory;

  @Input() generalForm;

  constructor(private productService: ProductService) {}

  ngOnInit() {
    this.initForm();
    this.getProductCategories();
  }

  initForm() {
    this._generalForm = this.generalForm;
  }

  getProductCategories() {
    this.productService
      .getProductCategories({ product_type: 3 })
      .pipe(take(1))
      .subscribe((categories) => {
        this.categories = categories;
      });
  }

  pushCategory(newCategory) {
    this.categories = [...this.categories, newCategory];

    this._generalForm.patchValue({
      category_id: newCategory.id,
    });
  }

  replaceCategory(editedCategory) {
    const _editedCategoryIndex = this.categories.findIndex((category) => category.id === editedCategory.id);

    this.categories = [
      ...this.categories.slice(0, _editedCategoryIndex),
      ...this.categories.slice(_editedCategoryIndex + 1),
      editedCategory,
    ].sort((a, b) => a.id - b.id);

    this._generalForm.patchValue({
      category_id: editedCategory.id,
    });
  }

  onVisitsStatusChange(status) {
    this._generalForm.patchValue({
      limit: status ? 1 : null,
      limit_status: status,
    });
  }

  get limit() {
    return this._generalForm.get('limit').value;
  }

  get limit_status() {
    return this._generalForm.get('limit_status').value;
  }

  get description() {
    return this._generalForm.get('description').value;
  }
}
