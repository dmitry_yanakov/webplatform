import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {StaffService} from '@core/services/staff.service';

@Component({
  selector: 'app-form-class-employees',
  templateUrl: './form-class-employees.component.html',
  styleUrls: ['./form-class-employees.component.scss'],
})
export class FormClassEmployeesComponent implements OnInit, OnDestroy {
  _employeesForm;
  isLoading = true;
  employees = [];
  filteredEmployees = [];
  searchValue$: Subject<string> = new Subject<string>();

  @Input() display;
  @Input() branch_id;
  @Input() businessType;
  @Input() employeesForm;

  private unsubscribe: Subject<any> = new Subject<any>();

  constructor(private staffService: StaffService) {}

  ngOnInit() {
    this.initForm();
    this.getAllEmployees();

    this.searchValue$.pipe(takeUntil(this.unsubscribe)).subscribe((searchValue) => {
      this.getEmployeesFilteredBySearchValue(searchValue);
    });
  }

  initForm() {
    this._employeesForm = this.employeesForm;
  }

  getAllEmployees() {
    this.staffService.getEmployees({count: 999}).pipe(takeUntil(this.unsubscribe)).subscribe(employees => {
      this.employees = employees;

      this.modifyEmployeesObjects();
      this.isLoading = false;
    });
  }

  modifyEmployeesObjects() {
    this.filteredEmployees = this.employees.map((employee) => {
      const isSelected = this._employeesForm.value.some(employee_id => employee_id === employee.id);

      return {...employee, selected: isSelected, visible: true};
    });
  }

  // Перебирает сотрудников при поиске
  // Назначает статус visible = true только тем, которые соответствуют поисковому запросу
  getEmployeesFilteredBySearchValue(searchValue: string = '') {
    const fullNameArr = searchValue.split(' ');

    this.filteredEmployees.forEach((employee) => {
        employee.visible = fullNameArr.every((name) => {
        const is_first_name_exist = employee.user_first_name.toLowerCase().indexOf(name) !== -1;
        const is_last_name_exist = employee.user_last_name.toLowerCase().indexOf(name) !== -1;

        return is_first_name_exist || is_last_name_exist;
      });
    });
  }

  searchEmployees(text) {
    this.searchValue$.next(text.toLowerCase());
  }

  toggleEmployee(employee) {
    const arrayId = this._employeesForm.value.findIndex((i) => i === employee.id);

    if (arrayId === -1) {
      this._employeesForm.setValue([...this._employeesForm.value, employee.id]);
    } else {
      this._employeesForm.setValue([
        ...this._employeesForm.value.slice(0, arrayId),
        ...this._employeesForm.value.slice(arrayId + 1),
      ]);
    }
    employee.selected = arrayId === -1;
  }

  toggleAllEmployees() {
    if (this.employees.length === this._employeesForm.value.length) {
      this._employeesForm.setValue([]);
      this.setAllEmployeesSelectStatus(false);
    } else {
      const allEmployees = this.employees.map((product) => product.id);

      this._employeesForm.setValue(allEmployees);
      this.setAllEmployeesSelectStatus(true);
    }

  }

  setAllEmployeesSelectStatus(status) {
    this.filteredEmployees.forEach((employee) => {
        employee.selected = status;
    });
  }

  clearSearchField() {
    return this.searchValue$.next('');
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  get isAllSelected() {
    return this.employees.length === this._employeesForm.value.length;
  }
}
