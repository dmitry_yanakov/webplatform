import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormClassRelatedPassesComponent } from './form-class-related-passes.component';

describe('FormClassRelatedPassesComponent', () => {
  let component: FormClassRelatedPassesComponent;
  let fixture: ComponentFixture<FormClassRelatedPassesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormClassRelatedPassesComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormClassRelatedPassesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
