import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subject, of } from 'rxjs';
import { catchError, takeUntil } from 'rxjs/operators';
import { ProductService } from '@core/services/product.service';

@Component({
  selector: 'app-form-class-related-passes',
  templateUrl: './form-class-related-passes.component.html',
  styleUrls: ['./form-class-related-passes.component.scss'],
})
export class FormClassRelatedPassesComponent implements OnInit, OnDestroy {
  relatedPassesForm: FormGroup;
  isLoading = true;
  passes = [];
  selectedPasses = [];
  filteredPasses = [];
  searchValue$: Subject<string> = new Subject<string>();

  @Input() display;
  @Input() branch_id;
  @Input() businessType;
  @Input() inputPasses;

  private unsubscribe: Subject<any> = new Subject<any>();

  constructor(private fb: FormBuilder, private productService: ProductService) {}

  ngOnInit() {
    this.initForm();
    this.productService
      .getProducts({ product_type: 2, count: 999 })
      .pipe(
        catchError(() => {
          return of([]);
        }),
        takeUntil(this.unsubscribe)
      )
      .subscribe((data) => {
        this.passes = data.products;
        this.isLoading = false;
        this.getProductsWithSearch();
      });

    this.searchValue$.pipe(takeUntil(this.unsubscribe)).subscribe((searchValue) => {
      this.getProductsWithSearch(searchValue);
    });
  }

  initForm() {
    this.relatedPassesForm = this.inputPasses;
    this.selectedPasses = this.inputPasses.value;
    console.log(this.inputPasses.value);
  }

  searchProducts(text) {
    this.searchValue$.next(text.toLowerCase());
  }

  getProductsWithSearch(searchValue: string = '') {
    this.filteredPasses = this.passes.filter((product) => {
      return product.title.toLowerCase().indexOf(searchValue) !== -1;
    });
  }

  toggleProduct(product_id) {
    const arrayId = this.selectedPasses.findIndex((i) => i === product_id);

    if (arrayId === -1) {
      this.selectedPasses.push(product_id);
    } else {
      this.selectedPasses = [...this.selectedPasses.slice(0, arrayId), ...this.selectedPasses.slice(arrayId + 1)];
    }

    this.relatedPassesForm.setValue(this.selectedPasses);
  }

  toggleAllProducts() {
    if (this.passes.length === this.selectedPasses.length) {
      this.selectedPasses = [];
    } else {
      this.selectedPasses = this.passes.map((product) => product.id);
    }

    this.relatedPassesForm.setValue(this.selectedPasses);
  }

  clearSearchField() {
    return this.searchValue$.next('');
  }

  checkProductSelected(product_id) {
    return this.selectedPasses.findIndex((i) => i === product_id) !== -1;
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  get isAllSelected() {
    return this.passes.length === this.selectedPasses.length;
  }
}
