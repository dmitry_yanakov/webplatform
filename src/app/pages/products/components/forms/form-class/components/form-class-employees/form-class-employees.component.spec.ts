import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormClassEmployeesComponent } from './form-class-employees.component';

describe('FormClassEmployeesComponent', () => {
  let component: FormClassEmployeesComponent;
  let fixture: ComponentFixture<FormClassEmployeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormClassEmployeesComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormClassEmployeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
