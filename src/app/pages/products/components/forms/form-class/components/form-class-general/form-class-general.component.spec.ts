import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormClassGeneralComponent } from './form-class-general.component';

describe('FormClassGeneralComponent', () => {
  let component: FormClassGeneralComponent;
  let fixture: ComponentFixture<FormClassGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormClassGeneralComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormClassGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
