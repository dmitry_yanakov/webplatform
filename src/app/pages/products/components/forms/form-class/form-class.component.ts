import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  ViewChild,
  ViewContainerRef,
  AfterViewInit,
  ViewChildren,
  OnDestroy,
} from '@angular/core';
import { ProductService } from '@core/services/product.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Tooltip } from 'primeng/primeng';
import { ActivatedRoute } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { forkJoin, Subject } from 'rxjs';
import { StaffService } from '@core/services/staff.service';

@Component({
  selector: 'app-form-class',
  templateUrl: './form-class.component.html',
  styleUrls: ['./form-class.component.scss'],
})
export class FormClassComponent implements OnInit, OnDestroy {
  classForm: FormGroup;
  selected_tab = 0;
  product_id: number;
  product_type = 3;
  isReady = false;

  @Output() formReady = new EventEmitter<FormGroup>();

  unsubscribe: Subject<any> = new Subject<any>();

  constructor(
    private fb: FormBuilder,
    private productService: ProductService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      this.selected_tab = Number(params['selected-tab']) || 0;
      this.product_id = params['product-id'];
    });

    this.productService.setProductType(3);
    this.initForm();
  }

  initForm() {
    this.classForm = this.fb.group({
      title: ['', Validators.required],
      duration: [null, Validators.required],
      category_id: [null, Validators.required],
      limit: [null, Validators.required],
      limit_status: [false],
      description: [''],
      relations: [[]],
      employees: [[]],
    });

    this.productService.getProducts({
      product_type: this.product_type,
      product_id: this.product_id,
    }).pipe(takeUntil(this.unsubscribe))
      .subscribe((products) => {
        let product, hours, minutes;

        if (this.product_id && products && products.products && products.products.length) {
          product = { ...products['products'][0], employees: products['products'][0].employees || [] };
          hours = Math.floor(product.duration / 60);
          minutes = Math.floor(product.duration % 60);

          this.classForm.patchValue({
            ...product,
            limit_status: product.limit === 1,
            duration: `${hours < 10 ? '0' + hours : hours}:${minutes}`,
            employees: product.employees ? product.employees.map((employee) => employee.employee_id) : [],
            relations: product.relations ? product.relations.map((relation) => relation.target_id) : [],
            flexible_price: product.price_max > 0,
          });
        }

        this.isReady = true;
      });

    this.formReady.emit(this.classForm);
  }

  switchTab(selected_tab) {
    this.selected_tab = selected_tab;
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
