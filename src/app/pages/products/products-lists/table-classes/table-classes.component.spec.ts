import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableClassesComponent } from './table-classes.component';

describe('TableClassesComponent', () => {
  let component: TableClassesComponent;
  let fixture: ComponentFixture<TableClassesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TableClassesComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableClassesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
