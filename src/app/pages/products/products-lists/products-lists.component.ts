import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { ProductService } from '@core/services/product.service';
import { CommonDataService } from '@core/services/common-data.service';
import { Subject } from 'rxjs';
import { takeUntil, tap, take } from 'rxjs/operators';

@Component({
  selector: 'app-products-lists',
  templateUrl: './products-lists.component.html',
  styleUrls: ['./products-lists.component.scss'],
})
export class ProductsListsComponent implements OnInit, OnDestroy {
  product_type = 0; // 0 - Товар, 1 - Услуга , 2 - Абонемент, 3 - Класс, 4 - Пакет услуг, -1 Все продукты
  businessType: number;
  userRole: number;
  searchText = '';
  product_link_name = ['good', 'service', 'pass', 'class', 'service-package'];
  product_translate_token = ['add-good', 'add-service', 'add-pass', 'add-class', 'add-service-package'];
  selected_link = '';

  unsubscribe: Subject<any> = new Subject<any>();

  constructor(
    private productService: ProductService,
    private commonDataService: CommonDataService,
    public cdr: ChangeDetectorRef
  ) {}

  ngOnInit() {
    // Подписываемся на изменения поля поиска и запрашиваем продукты, соответствующие поисковому запросу
    this.productService.search
      .pipe(
        tap((res) =>
          this.productService
            .searchProducts({ text: res, product_type: this.product_type })
            .pipe(take(1))
            .subscribe(() => this.productService.loading.next(false))
        ),
        takeUntil(this.unsubscribe)
      )
      .subscribe((value) => {
        this.searchText = value;
      });

    // Подписываемся на изменения типа продукта и очищаем поле поиска при его изменении
    // Тип продукта устанавливается на соответствующих страницах продуктов
    this.productService.product_type.pipe(takeUntil(this.unsubscribe)).subscribe((product_type) => {
      this.product_type = product_type;
      this.selected_link = '/products/create/' + this.product_link_name[product_type];
      this.searchText = '';
    });

    this.businessType = this.commonDataService.currentCompany.session;
    this.userRole = this.commonDataService.currentCompany.role_id;
  }

  search(text) {
    if (this.searchText !== text) {
      this.productService.search.next(text);
    }
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
