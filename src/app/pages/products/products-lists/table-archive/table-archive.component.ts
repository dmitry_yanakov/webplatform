import { Component, OnInit, ViewChild, HostListener, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { ProductService } from '@core/services/product.service';
import { takeUntil, switchMap } from 'rxjs/operators';
import { LazyLoadEvent } from 'primeng/api';
import { CommonDataService } from '@core/services/common-data.service';
import { NotificationMessageService } from '@core/services/notification-message.service';

@Component({
  selector: 'app-table-archive',
  templateUrl: './table-archive.component.html',
  styleUrls: ['./table-archive.component.scss'],
})
export class TableArchiveComponent implements OnInit, OnDestroy {
  offset = 0;
  count = 7;
  total: number;
  archive = [];
  loading = false;
  search = '';
  currency_symbol: string;
  product_type = ['goods', 'services', 'passes', 'classes', 'service-packages'];
  product_restore_notifications = [
    'notification-restore-good',
    'notification-restore-service',
    'notification-restore-pass',
    'notification-restore-class',
    'notification-restore-service-package',
  ];

  @ViewChild('dv', { static: false }) dv;

  private unsubscribe: Subject<any> = new Subject();

  constructor(
    private productService: ProductService,
    private commonDataService: CommonDataService,
    private notificationMessageService: NotificationMessageService
  ) {}

  @HostListener('window:keyup', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if (this.dv.paginate) {
      if (event.key === 'ArrowLeft' && this.offset - this.count >= 0) {
        this.dv.paginate({ first: this.offset - this.count, rows: this.count });
      } else if (event.key === 'ArrowRight' && this.offset + this.count < this.total) {
        this.dv.paginate({ first: this.offset + this.count, rows: this.count });
      }
    }
    return;
  }

  ngOnInit() {
    this.productService.setProductType(-1);
    this.currency_symbol = this.commonDataService.currentCompany.currency_symbol;
    this.productService.loading.pipe(takeUntil(this.unsubscribe)).subscribe((loading) => (this.loading = loading));
    this.productService.search.pipe(takeUntil(this.unsubscribe)).subscribe((search) => (this.search = search));
    this.productService.products.pipe(takeUntil(this.unsubscribe)).subscribe(({ data, offset, count, total }) => {
      this.archive = data;
      this.offset = offset;
      this.count = count;
      this.total = total;
    });
  }

  // event.first = First row offset
  // event.rows = Number of rows per page
  // event.sortField = Field name to sort with
  // event.sortOrder = Sort order as number, 1 for asc and -1 for dec
  // filters: FilterMetadata object having field as key and filter value, filter matchMode as value
  loadGoodsLazy(event: LazyLoadEvent) {
    this.productService.loading.next(true);

    this.productService
      .getProducts({ product_type: -1, offset: event.first, count: event.rows, is_archive: 1 })
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(() => this.productService.loading.next(false));
  }

  clearSearchField() {
    this.productService.search.next('');
  }

  fromArchive(product) {
    this.productService
      .fromArchive(product.id)
      .pipe(
        switchMap((res) => {
          if (res['status'] === 'success') {
            this.notificationMessageService.pushAlert({ detail: this.product_restore_notifications[product.type] });
            return this.productService.getProducts({
              product_type: -1,
              offset: this.offset,
              count: this.count,
              is_archive: 1,
            });
          }
        }),
        takeUntil(this.unsubscribe)
      )
      .subscribe((res) => {});
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
