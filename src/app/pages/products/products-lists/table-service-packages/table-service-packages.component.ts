import { Component, OnInit, ViewChild, HostListener, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { ProductService } from '@core/services/product.service';
import { takeUntil, switchMap } from 'rxjs/operators';
import { LazyLoadEvent } from 'primeng/api';
import { CommonDataService } from '@core/services/common-data.service';
import { NotificationMessageService } from '@core/services/notification-message.service';

export interface TableServicePackagesRelation {
  product_id?: number;
  product_title?: string;
  target_id?: number;
}

export interface TableServicePackages {
  id?: number;
  branch_id?: number;
  category_id?: number;
  category_title?: string | null;
  type?: number;
  vendor?: string;
  title?: string;
  description?: string;
  cost?: number;
  price?: number;
  price_max?: number;
  duration?: number;
  count?: number;
  is_archive?: number;
  date?: string;
  limit?: number;
  activate?: number;
  time?: string;
  relations?: TableServicePackagesRelation[];
}

@Component({
  selector: 'app-table-service-packages',
  templateUrl: './table-service-packages.component.html',
  styleUrls: ['./table-service-packages.component.scss'],
})
export class TableServicePackagesComponent implements OnInit, OnDestroy {
  service_packages: TableServicePackages[] = [];
  offset = 0;
  count = 7;
  total: number;
  loading = false;
  search = '';
  currency_symbol: string;
  user_role: number;

  @ViewChild('dv', { static: false }) dv;

  private unsubscribe: Subject<any> = new Subject();

  constructor(
    private productService: ProductService,
    private commonDataService: CommonDataService,
    private notificationMessageService: NotificationMessageService
  ) {}

  @HostListener('window:keyup', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if (this.dv.paginate) {
      if (event.key === 'ArrowLeft' && this.offset - this.count >= 0) {
        this.dv.paginate({ first: this.offset - this.count, rows: this.count });
      } else if (event.key === 'ArrowRight' && this.offset + this.count < this.total) {
        this.dv.paginate({ first: this.offset + this.count, rows: this.count });
      }
    }
    return;
  }

  ngOnInit() {
    this.productService.setProductType(4);
    this.currency_symbol = this.commonDataService.currentCompany.currency_symbol;
    this.user_role = this.commonDataService.currentCompany.role_id;
    this.productService.loading.pipe(takeUntil(this.unsubscribe)).subscribe((loading) => (this.loading = loading));
    this.productService.search.pipe(takeUntil(this.unsubscribe)).subscribe((search) => (this.search = search));
    this.productService.products.pipe(takeUntil(this.unsubscribe)).subscribe(({ data, offset, count, total }) => {
      this.service_packages = data;
      this.offset = offset;
      this.count = count;
      this.total = total;
    });
  }

  // event.first = First row offset
  // event.rows = Number of rows per page
  // event.sortField = Field name to sort with
  // event.sortOrder = Sort order as number, 1 for asc and -1 for dec
  // filters: FilterMetadata object having field as key and filter value, filter matchMode as value
  loadGoodsLazy(event: LazyLoadEvent) {
    this.productService.loading.next(true);

    this.productService
      .getProducts({ product_type: 4, offset: event.first, count: event.rows })
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(() => this.productService.loading.next(false));
  }

  clearSearchField() {
    this.productService.search.next('');
  }

  toArchive(product_id: number) {
    this.productService
      .toArchive(product_id)
      .pipe(
        switchMap((res) => {
          if (res['status'] === 'success') {
            this.notificationMessageService.pushAlert({ detail: 'notification-remove-service-package' });
            return this.productService.getProducts({ product_type: 4, offset: this.offset, count: this.count });
          }
        }),
        takeUntil(this.unsubscribe)
      )
      .subscribe((res) => {});
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
