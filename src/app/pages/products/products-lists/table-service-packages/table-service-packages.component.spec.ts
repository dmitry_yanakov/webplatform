import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableServicePackagesComponent } from './table-service-packages.component';

describe('TableServicePackagesComponent', () => {
  let component: TableServicePackagesComponent;
  let fixture: ComponentFixture<TableServicePackagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TableServicePackagesComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableServicePackagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
