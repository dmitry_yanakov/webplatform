import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableGoodsComponent } from './table-goods.component';

describe('TableGoodsComponent', () => {
  let component: TableGoodsComponent;
  let fixture: ComponentFixture<TableGoodsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TableGoodsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableGoodsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
