import { Component, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { switchMap, takeUntil, tap } from 'rxjs/operators';
import { ProductService } from '@core/services/product.service';
import { Subject } from 'rxjs';
import { LazyLoadEvent } from 'primeng/api';
import { CommonDataService } from '@core/services/common-data.service';
import { NotificationMessageService } from '@core/services/notification-message.service';

@Component({
  selector: 'app-table-goods',
  templateUrl: './table-goods.component.html',
  styleUrls: ['./table-goods.component.scss'],
})
export class TableGoodsComponent implements OnInit, OnDestroy {
  offset = 0;
  count = 7;
  total: number;
  goods = [];
  loading = true;
  search = '';
  currency_symbol: string;
  user_role: number;

  @ViewChild('dv', { static: false }) dv;

  private unsubscribe: Subject<any> = new Subject();

  constructor(
    private productService: ProductService,
    private commonDataService: CommonDataService,
    private notificationMessageService: NotificationMessageService
  ) {}

  @HostListener('window:keyup', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if (this.dv.paginate) {
      if (event.key === 'ArrowLeft' && this.offset - this.count >= 0) {
        this.dv.paginate({ first: this.offset - this.count, rows: this.count });
      } else if (event.key === 'ArrowRight' && this.offset + this.count < this.total) {
        this.dv.paginate({ first: this.offset + this.count, rows: this.count });
      }
    }
    return;
  }

  ngOnInit() {
    this.productService.setProductType(0);
    this.currency_symbol = this.commonDataService.currentCompany.currency_symbol;
    this.user_role = this.commonDataService.currentCompany.role_id;
    this.productService.loading.pipe(takeUntil(this.unsubscribe)).subscribe((loading) => (this.loading = loading));
    this.productService.search.pipe(takeUntil(this.unsubscribe)).subscribe((search) => (this.search = search));
    this.productService.products.pipe(takeUntil(this.unsubscribe)).subscribe(({ data, offset, count, total }) => {
      this.goods = data;
      this.offset = offset;
      this.count = count;
      this.total = total;
    });
  }

  // event.first = First row offset
  // event.rows = Number of rows per page
  // event.sortField = Field name to sort with
  // event.sortOrder = Sort order as number, 1 for asc and -1 for dec
  // filters: FilterMetadata object having field as key and filter value, filter matchMode as value
  loadGoodsLazy(event: LazyLoadEvent) {
    this.productService.loading.next(true);

    this.productService
      .getProducts({ product_type: 0, offset: event.first, count: event.rows })
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(() => this.productService.loading.next(false));
  }

  clearSearchField() {
    this.productService.search.next('');
    this.productService
      .searchProducts({ text: '', product_type: 0 })
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(() => this.productService.loading.next(false));
  }

  toArchive(product_id: number) {
    this.productService
      .toArchive(product_id)
      .pipe(
        switchMap((res) => {
          if (res['status'] === 'success') {
            this.notificationMessageService.pushAlert({ detail: 'notification-remove-good' });
            return this.productService.getProducts({ product_type: 0, offset: this.offset, count: this.count });
          }
        }),
        takeUntil(this.unsubscribe)
      )
      .subscribe((res) => {});
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
