import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablePassesComponent } from './table-passes.component';

describe('TablePassesComponent', () => {
  let component: TablePassesComponent;
  let fixture: ComponentFixture<TablePassesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TablePassesComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablePassesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
