import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessBlockComponent } from './components/business-block/business-block.component';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule, ButtonModule } from 'src/app/shared';
import { MoreComponent } from './components/more/more.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { ChangeForgotComponent } from './components/change-forgot/change-forgot.component';

@NgModule({
  declarations: [
    BusinessBlockComponent,
    LoginComponent,
    RegistrationComponent,
    MoreComponent,
    ForgotPasswordComponent,
    ChangeForgotComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    ButtonModule,
    RouterModule,
    TranslateModule
  ]
})
export class AuthModule { }
