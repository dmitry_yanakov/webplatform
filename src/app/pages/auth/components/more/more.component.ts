import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '@core/services/api.service';
import { Router } from '@angular/router';
import { IPhoneCodes } from 'src/app/shared/components/phone-field/phone-field.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-more',
  templateUrl: './more.component.html',
  styleUrls: ['./more.component.scss'],
})
export class MoreComponent implements OnInit {
  phoneGroup: FormGroup;
  isCheked = false;
  phoneCodes: Array<IPhoneCodes> = [];
  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private router: Router,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.initPhoneForm();
    this.getPhoneCodes();
  }

  initPhoneForm() {
    this.phoneGroup = this.fb.group({
      phone: [
        {
          code: ['af'],
          phone_code: ['93'],
        },
      ],
      phone_number: ['', [Validators.minLength(6), Validators.required]],
    });
  }

  get f() {
    return this.phoneGroup.controls;
  }

  getPhoneCodes() {
    this.apiService.getPhoneCodes().subscribe((codesResp) => {
      const codes = codesResp['data'].map(
        (phone): IPhoneCodes => {
          return {
            label: phone.name,
            value: {
              name: this.translate.instant(phone.name),
              code: phone.code.toLowerCase(),
              phone_code: phone.phone_code,
            },
          };
        }
      );
      this.phoneCodes = codes;
    });
  }

  checkAgreeDocuments(checked) {
    this.isCheked = checked;
  }

  setPhoneNumber() {
    if (!this.phoneGroup.valid) {
      return;
    }
    if (this.isCheked === false) {
      return;
    }
    const sendData = {
      phone_country: this.phoneGroup.value.phone.code,
      phone_code: this.phoneGroup.value.phone.phone_code,
      phone_number: this.f.phone_number.value,
    };
    this.apiService.setUser(sendData).subscribe((response) => {
      if (response['status'] === 'success') {
        this.router.navigate(['/desk']);
      } else if (response['status'] === 'error') {
        alert(response['errors'][0]['code']);
      }
    });
  }
}
