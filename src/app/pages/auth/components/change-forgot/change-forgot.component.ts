import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '@core/services/api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-change-forgot',
  templateUrl: './change-forgot.component.html',
  styleUrls: ['./change-forgot.component.scss']
})
export class ChangeForgotComponent implements OnInit {
  passwordForm: FormGroup;
  showPass = false;
  constructor(private fb: FormBuilder,
              private apiService: ApiService,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.initPasswordForm();
  }

  initPasswordForm() {
    this.passwordForm = this.fb.group({
      password: ['', [Validators.required]],
    });
  }

  get f() {
    return this.passwordForm.controls;
  }

  changePassword() {
    const forgot_code = this.activatedRoute.snapshot.queryParamMap.get('forgot_code');
    if (this.passwordForm.invalid) {
      return;
    }
    this.apiService.forgotFinish(forgot_code, this.f.password.value).subscribe(response => {
      if (response['status'] === 'success') {
      } else if (response['status'] === 'error') {
        alert(response['errors'][0]['description']);
      }
    });
  }
}
