import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '@core/services/api.service';
import { AppAuthService } from '@core/services/auth.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  showAproveMessage = false;
  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private authService: AppAuthService,
    private activeRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.onInitLoginForm();
    this.checkQueryParams();
  }

  onInitLoginForm() {
    this.loginForm = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]],
    });
  }

  get f() {
    return this.loginForm.controls;
  }

  loginGoogle() {
    this.authService.loginWithGoogle();
  }

  loginFacebook() {
    this.authService.loginWithFB();
  }

  login() {
    if (this.loginForm.invalid) {
      return;
    }

    this.apiService.login(this.f.email.value, this.f.password.value).subscribe((response) => {
      if (response['status'] === 'success') {
        this.authService.afterLogin(response['data']['token']);
      } else if (response['status'] === 'error') {
        for (let i = 0; i < response['errors'].length; i++) {
          if (response['errors'][i]['code'] === 5) {
            alert('Login or password is incorrect!');
          }
        }
      }
    });
  }

  checkQueryParams() {
    const email = this.activeRoute.snapshot.queryParamMap.get('email');
    const email_code = this.activeRoute.snapshot.queryParamMap.get('approve_code');
    if (email && email_code) {
      this.apiService.approveRegister(email, email_code).subscribe((response: any) => {
        if (response.status === 'success') {
          this.showAproveMessage = true;
        } else {
          this.showAproveMessage = false;
        }
      });
    }
  }
}
