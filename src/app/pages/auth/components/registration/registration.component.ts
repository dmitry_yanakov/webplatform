import { ApiService } from './../../../../core/services/api.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { AppAuthService } from '@core/services/auth.service';
import { IPhoneCodes } from 'src/app/shared/components/phone-field/phone-field.component';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent implements OnInit {
  registrationUser: FormGroup;
  showPass = [false, false];
  phoneCodes: Array<IPhoneCodes> = [];
  selectedCounries;
  isCheked = false;
  token;
  constructor(
    private fb: FormBuilder,
    private translate: TranslateService,
    private apiService: ApiService,
    private authService: AppAuthService,
    private router: Router
  ) {}

  ngOnInit() {
    this.initRegForm();
    this.getPhoneCodes();
  }

  initRegForm() {
    this.registrationUser = this.fb.group({
      name: ['', [Validators.required]],
      phone: [
        {
          code: ['af'],
          phone_code: ['93'],
        },
      ],
      phone_number: ['', [Validators.minLength(6), Validators.required]],
      email: ['', [Validators.email, Validators.required]],
      password: this.fb.group({
        pass: ['', Validators.required],
        confirmPass: ['', Validators.required],
      }),
    });
  }

  get f() {
    return this.registrationUser.controls;
  }

  getPhoneCodes() {
    this.apiService.getPhoneCodes().subscribe((codesResp) => {
      const codes = codesResp['data'].map(
        (phone): IPhoneCodes => {
          return {
            label: phone.name,
            value: {
              name: this.translate.instant(phone.name),
              code: phone.code.toLowerCase(),
              phone_code: phone.phone_code,
            },
          };
        }
      );
      this.phoneCodes = codes;
    });
  }

  checkAgreeDocuments(checked) {
    this.isCheked = checked;
  }

  loginGoogle() {
    this.authService.loginWithGoogle();
  }

  loginFacebook() {
    this.authService.loginWithFB();
  }

  register() {
    if (!this.registrationUser.valid) {
      return;
    }
    if (this.isCheked === false) {
      return;
    }
    const sendData = {
      email: this.f.email.value,
      password: this.registrationUser.get('password.pass').value,
      language: this.translate.currentLang,
      first_name: this.f.name.value,
      phone_country: this.registrationUser.value.phone.code,
      phone_code: this.registrationUser.value.phone.phone_code,
      phone_number: this.f.phone_number.value,
      code: '',
    };
    this.apiService.register(sendData).subscribe((response) => {
      if (response['status'] === 'success') {
        alert('Successfully registered!');
        this.router.navigate(['/login']);
      } else if (response['status'] === 'error') {
        for (let i = 0; i < response['errors'].length; i++) {
          if (response['errors'][i]['code'] === 4) {
            // here will be error message
          } else if (response['errors'][i]['code'] === 6) {
            // here will be error message
          }
        }
      }
    });
  }

  afterLogin(token) {
    this.apiService.getSelfUser(token).subscribe((selfUserResponse) => {
      if (selfUserResponse['status'] === 'success') {
        this.apiService.user = selfUserResponse['data'];
        if (!this.apiService.user['phone_number'] && this.apiService.user['phone_number'] === '') {
          this.router.navigate(['/more']);
          this.token = token;
        } else {
          localStorage.setItem('token', token);
          this.apiService.token = token;
          this.router.navigate(['/desk']);
        }
      }
    });
  }
}
