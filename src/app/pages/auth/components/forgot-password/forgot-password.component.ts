import { AppAuthService } from './../../../../core/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '@core/services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  forgotGroup: FormGroup;
  successfullySend = false;
  constructor(private fb: FormBuilder,
              private apiService: ApiService,
              private authService: AppAuthService) { }

  ngOnInit() {
    this.initEmailForm();
  }

  initEmailForm() {
    this.forgotGroup = this.fb.group({
      email: [null, [Validators.required, Validators.email]],
    });
  }

  get f() {
    return this.forgotGroup.controls;
  }

  loginGoogle() {
    this.authService.loginWithGoogle();
  }

  loginFacebook() {
    this.authService.loginWithFB();
  }

  back() {
    this.successfullySend = false;
  }

  restore() {
    if (this.forgotGroup.invalid) {
      return;
    }

    this.apiService.forgot(this.f.email.value).subscribe(response => {
      if (response['status'] === 'success') {
        this.successfullySend = true;
      } else if (response['status'] === 'error') {
        alert(response['errors'][0]['description']);
      }
    });
  }

}
