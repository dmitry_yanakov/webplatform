import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-new-sale',
  templateUrl: './new-sale.component.html',
  styleUrls: ['./new-sale.component.scss'],
})
export class NewSaleComponent implements OnInit {
  constructor(private location: Location) {}
  checked = false;
  ngOnInit() {}

  goBack() {
    this.location.back();
  }
}
