import { Component, OnInit, ViewChild, HostListener, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { CommonDataService } from '@core/services/common-data.service';
import { takeUntil, switchMap } from 'rxjs/operators';
import { LazyLoadEvent } from 'primeng/api';
import { FinanceService } from '@core/services/finance.service';
import { ApiService } from '@core/services/api.service';

@Component({
  selector: 'app-costs',
  templateUrl: './costs.component.html',
  styleUrls: ['./costs.component.scss'],
})
export class CostsComponent implements OnInit, OnDestroy {
  offset = 0;
  count = 7;
  total: number;
  finance = [];
  loading = false;
  search = '';
  tableOptions: any = { sortField: '' };
  currency_symbol: string;
  selectedTransactionId: number;
  @ViewChild('dv', { static: false }) dv;

  private unsubscribe: Subject<any> = new Subject();

  constructor(private financeService: FinanceService, private commonDataService: CommonDataService, private apiService: ApiService) {}

  @HostListener('window:keyup', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if (this.dv.paginate) {
      if (event.key === 'ArrowLeft' && this.offset - this.count >= 0) {
        this.dv.paginate({ first: this.offset - this.count, rows: this.count });
      } else if (event.key === 'ArrowRight' && this.offset + this.count < this.total) {
        this.dv.paginate({ first: this.offset + this.count, rows: this.count });
      }
    }
    return;
  }

  ngOnInit() {
    this.financeService.setFinanceType(1);
    this.currency_symbol = this.commonDataService.currentCompany.currency_symbol;
    this.financeService.loading.pipe(takeUntil(this.unsubscribe)).subscribe((loading) => (this.loading = loading));
    this.financeService.search.pipe(takeUntil(this.unsubscribe)).subscribe((search) => (this.search = search));
    this.financeService.data.pipe(takeUntil(this.unsubscribe)).subscribe(({ data, offset, count, total }) => {
      this.finance = data;
      this.offset = offset;
      this.count = count;
      this.total = total;
    });
  }

  loadGoodsLazy(event: LazyLoadEvent) {
    this.financeService.loading.next(true);

    this.financeService
      .getFinance({ type: 1, offset: event.first, count: event.rows })
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(() => this.financeService.loading.next(false));
  }

  clearSearchField() {
    this.financeService.search.next('');
    this.financeService
      .searchFinance({ text: '', type: 1 })
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(() => this.financeService.loading.next(false));
  }

  removeTransaction(id) {
    this.apiService
      .removeTransaction(id)
      .pipe(
        switchMap((res) => {
          if (res['status'] === 'success') {
            return this.financeService.getFinance({ type: 0, offset: this.offset, count: this.count });
          }
        }),
        takeUntil(this.unsubscribe)
      )
      .subscribe((res) => {});
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
