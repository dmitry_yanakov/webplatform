import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FinanceComponent } from './finance.component';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { TableModule } from 'primeng/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { CostsComponent } from './costs/costs.component';
import { IncomeComponent } from './income/income.component';
import { NewSaleComponent } from './new-sale/new-sale.component';
import { InputSwitchModule } from 'primeng/inputswitch';
import { DataViewModule } from 'primeng/dataview';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { AddProductsComponent } from './add-products/add-products.component';
import { NewGoodsComponent } from './new-goods/new-goods.component';
import { NewServicesComponent } from './new-services/new-services.component';
import { ConfirmModalComponent } from './confirm-modal/confirm-modal.component';
import { HeaderModule, ScrollPanelModule, SharedModule } from '../../shared';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    SharedModule,
    HeaderModule,
    ScrollPanelModule,
    InputSwitchModule,
    TableModule,
    FormsModule,
    ReactiveFormsModule,
    DropdownModule,
    DataViewModule,
    OverlayPanelModule,
    ConfirmDialogModule,
  ],
  declarations: [
    FinanceComponent,
    CostsComponent,
    IncomeComponent,
    NewSaleComponent,
    AddProductsComponent,
    NewGoodsComponent,
    NewServicesComponent,
    ConfirmModalComponent,
  ],
})
export class FinanceModule {}
