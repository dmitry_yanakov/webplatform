import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss'],
})
export class ConfirmModalComponent implements OnInit {
  isOpen = false;
  @Output() remove = new EventEmitter();
  constructor() {}

  ngOnInit() {}

  toggleOpen() {
    this.isOpen = !this.isOpen;
  }

  removeTransaction() {
    this.remove.emit();
    this.toggleOpen();
  }
}
