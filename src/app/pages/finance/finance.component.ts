import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { FinanceService } from '@core/services/finance.service';
import { CommonDataService } from '@core/services/common-data.service';
import { tap, takeUntil, take } from 'rxjs/operators';

@Component({
  selector: 'app-finance',
  templateUrl: './finance.component.html',
  styleUrls: ['./finance.component.scss'],
})
export class FinanceComponent implements OnInit, OnDestroy {
  type: 0 | 1 = 0;
  businessType: number;
  userRole: number;
  searchText = '';

  unsubscribe: Subject<any> = new Subject<any>();

  constructor(private financeService: FinanceService, private commonDataService: CommonDataService) {}

  ngOnInit() {
    // Подписываемся на изменения поля поиска и запрашиваем продукты, соответствующие поисковому запросу
    this.financeService.search
      .pipe(
        tap((res) =>
          this.financeService
            .searchFinance({ text: res, type: this.type })
            .pipe(take(1))
            .subscribe(() => this.financeService.loading.next(false))
        ),
        takeUntil(this.unsubscribe)
      )
      .subscribe((value) => {
        this.searchText = value;
      });

    // Подписываемся на изменения типа продукта и очищаем поле поиска при его изменении
    // Тип продукта устанавливается на соответствующих страницах продуктов
    this.financeService.type.pipe(takeUntil(this.unsubscribe)).subscribe((type) => {
      this.type = type;
      this.searchText = '';
    });

    this.businessType = this.commonDataService.currentCompany.session;
    this.userRole = this.commonDataService.currentCompany.role_id;
  }

  search(text) {
    if (this.searchText !== text) {
      this.financeService.search.next(text);
    }
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
