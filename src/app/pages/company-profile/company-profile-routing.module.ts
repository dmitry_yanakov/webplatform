import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/guards/auth-guard';

// Components
import { GeneralComponent } from './components/general/general.component';
import { CompanyProfileComponent } from './company-profile.component';
import { PortfolioComponent } from './components/portfolio/portfolio.component';
import { TariffComponent } from './components/tariff/tariff.component';
import { BookingSettingsComponent } from './components/booking-settings/booking-settings.component';
import { LocationsComponent } from './components/locations/locations.component';

const companyProfileRoutes: Routes = [
  {
    path: 'company-profile',
    component: CompanyProfileComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'general', component: GeneralComponent, canActivate: [AuthGuard] },
      { path: 'locations', component: LocationsComponent, canActivate: [AuthGuard] },
      { path: 'portfolio', component: PortfolioComponent, canActivate: [AuthGuard] },
      { path: 'tariff', component: TariffComponent, canActivate: [AuthGuard] },
      { path: 'booking-settings', component: BookingSettingsComponent, canActivate: [AuthGuard] },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(companyProfileRoutes)],
  exports: [RouterModule],
})
export class CompanyProfileRoutingModule {}
