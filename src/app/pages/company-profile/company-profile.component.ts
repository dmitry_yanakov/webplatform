import { Component, OnInit } from '@angular/core';
import { CommonDataService } from '@core/services/common-data.service';

@Component({
  selector: 'app-company-profile',
  templateUrl: './company-profile.component.html',
  styleUrls: ['./company-profile.component.scss'],
})
export class CompanyProfileComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
