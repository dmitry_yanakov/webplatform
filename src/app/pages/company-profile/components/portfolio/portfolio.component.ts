import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '@core/services/api.service';
import { NotificationMessageService } from '@core/services/notification-message.service';

export interface Photo {
  company_id: number;
  date: string;
  id: number;
  is_main: number;
  path: string;
}

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss'],
})
export class PortfolioComponent implements OnInit {
  photos: Photo[] = [];
  selectedPhoto: Photo = null;
  @ViewChild('dialog', { static: false })
  dialog;

  constructor(private api: ApiService, private notificationMessageService: NotificationMessageService) {}

  ngOnInit() {
    this.getPhotos();
  }

  addPhoto(photo) {
    this.api.addCompanyPhoto(photo).subscribe((res) => {
      if (res['status'] === 'success') {
        this.getPhotos();
        this.notificationMessageService.pushAlert({ detail: 'notification-loaded-successful' });
      }
    });
  }

  getPhotos() {
    this.api.getCompanyPhotos().subscribe((res) => {
      if (res['status'] === 'success') {
        const photos = res['data'].reduce((acc, photo) => {
          const is_exist = this.photos.find((i) => i.id === photo.id);

          if (!is_exist) {
            acc.push(photo);

            return acc;
          }

          return acc;
        }, []);

        this.photos.push(...photos);
      }
    });
  }

  removePhoto(photo_id) {
    this.api.removeCompanyPhoto(photo_id).subscribe((res) => {
      if (res['status'] === 'success') {
        this.photos = this.photos.filter((photo) => photo.id !== photo_id);
        this.notificationMessageService.pushAlert({ detail: 'notification-remove-photo' });
      }
    });
  }

  handlePhotoModalToggle(photo) {
    if (!this.selectedPhoto) {
      this.selectedPhoto = photo;
    } else {
      this.selectedPhoto = null;
    }
  }
}
