import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonDataService } from '@core/services/common-data.service';
import { ApiService } from '@core/services/api.service';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import * as moment from 'moment';

@Component({
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss'],
})
export class LocationsComponent implements OnInit {
  moment = moment;
  branches = [];
  branch_id: any;
  private selected_branch_id?: number;

  @ViewChild('modal', { static: false }) removeModal: ModalComponent;

  constructor(private api: ApiService, private commonData: CommonDataService) {}

  ngOnInit() {
    this.branch_id = this.commonData.currentBranch.id;
    this.getBranches();
  }

  getBranches() {
    this.api.getCompanyBranch().subscribe((res) => {
      if (res['status'] === 'success') {
        this.branches = res['data']['branches'].map((branch) => {
          const active = branch.id === this.branch_id;
          console.log(branch.id, this.commonData);
          const times = branch.times.map((time) => {
            return { ...time, from: time.time.split('-')[0], to: time.time.split('-')[1] };
          });

          return { ...branch, times, phones: JSON.parse(branch.phones), active };
        });
      }
    });
  }

  openRemoveModal(branch_id) {
    this.selected_branch_id = branch_id;
    this.removeModal.showModal();
  }

  removeLocation() {
    this.api.deactivateBranch({ branch_id: this.selected_branch_id }).subscribe((res) => {
      if (res['status'] === 'success') {
        const branch_index = this.branches.findIndex((branch) => branch.id === this.selected_branch_id);

        if (branch_index !== this.branches.length - 1) {
          this.branches = [...this.branches.slice(0, branch_index), ...this.branches.slice(branch_index + 1)];
        } else {
          this.branches = [...this.branches.slice(0, branch_index)];
        }

        this.removeModal.hideModal();
      }
    });
  }
}
