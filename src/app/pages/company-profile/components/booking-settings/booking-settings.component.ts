import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from '@core/services/api.service';
import { CommonDataService } from '@core/services/common-data.service';
import { HelperService } from '@core/services/helper.service';
import { reduce, isEmpty } from 'lodash';
import { NotificationMessageService } from '@core/services/notification-message.service';

@Component({
  selector: 'app-booking-settings',
  templateUrl: './booking-settings.component.html',
  styleUrls: ['./booking-settings.component.scss'],
})
export class BookingSettingsComponent implements OnInit {
  bookingLink = `https://bodywell.app/card/${this.commonData.currentCompany.id}/${this.commonData.currentBranch.id}`;
  bookingSettingsForm: FormGroup;
  initFormValues = {};

  constructor(
    private fb: FormBuilder,
    private api: ApiService,
    private commonData: CommonDataService,
    private helper: HelperService,
    private notificationMessageService: NotificationMessageService
  ) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.initFormValues = {
      online_booking: this.commonData.currentCompany.online_booking,
      online_accept: this.commonData.currentCompany.online_accept,
      online_ban: this.commonData.currentCompany.online_ban,
      online_reject: this.commonData.currentCompany.online_reject,
    };

    this.bookingSettingsForm = this.fb.group(this.initFormValues);
  }

  copyToClipboard() {
    this.helper.copyToClipboard(this.bookingLink);
  }

  onSubmit() {
    const integerFormValues = reduce(
      this.bookingSettingsForm.value,
      (acc, value, key) => {
        acc[key] = Number(value);
        return acc;
      },
      {}
    );
    const unicFormValues = this.helper.getUnicObjectsValue(integerFormValues, this.initFormValues);

    if (!isEmpty(unicFormValues)) {
      this.api.updateCompany(unicFormValues).subscribe((res) => {
        if (res['status'] === 'success') {
          Object.keys(unicFormValues).forEach((value) => {
            this.commonData.currentCompany[value] = unicFormValues[value];
            this.initFormValues[value] = unicFormValues[value];
          });
          this.notificationMessageService.pushAlert({ detail: 'notification-update-company' });
        }
      });
    }
  }
}
