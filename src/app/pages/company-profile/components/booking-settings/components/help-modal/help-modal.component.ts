import { Component, OnInit } from '@angular/core';
import { CommonDataService } from '@core/services/common-data.service';

@Component({
  selector: 'app-help-modal',
  templateUrl: './help-modal.component.html',
  styleUrls: ['./help-modal.component.scss'],
})
export class HelpModalComponent implements OnInit {
  private _listItems = [
    {
      link: '/company-settings/location',
      link_name: 'booking-settings-go-to-location',
      title: 'work-time',
      description: 'booking-settings-work-time-online-booking-description',
      visible: true,
    },
    {
      link: '/staff/active',
      link_name: 'booking-settings-go-to-employees',
      title: 'employees',
      description: 'booking-settings-employees-online-booking-description',
      visible: this.commonData.currentCompany.type_id !== 1,
    },
    {
      link: `/products/${this.commonData.currentCompany.session === 0 ? 'services' : 'classes'}`,
      link_name: 'booking-settings-go-to-products',
      title: 'services-and-classes',
      description: 'booking-settings-services-and-classes-online-booking-description',
      visible: true,
    },
    {
      link: '/company-settings/portfolio',
      link_name: 'booking-settings-go-to-portfolio',
      title: 'portfolio',
      description: 'booking-settings-portfolio-online-booking-description',
      visible: true,
    },
    {
      link_name: 'booking-settings-go-to-online-booking',
      title: 'booking-settings-turn-online-booking-on',
      description: 'booking-settings-turn-online-booking-on-description',
      visible: true,
      click: true,
    },
  ];

  constructor(private commonData: CommonDataService) {}

  ngOnInit() {}

  get listItems() {
    return this._listItems.filter((item) => item.visible);
  }
}
