import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import jsPDF from 'jspdf';
import { QrCodeGeneratorComponent } from '../../../../../../shared/components/qr-code-generator/qr-code-generator.component';
import { proximaBold, proximaRegular } from './pdfFonts';

@Component({
  selector: 'app-qr-code',
  templateUrl: './qr-code.component.html',
  styleUrls: ['./qr-code.component.scss'],
})
export class QrCodeComponent implements OnInit {
  @ViewChild('qrcode', { static: false }) qrcode: QrCodeGeneratorComponent;
  @Input() value: string;

  constructor(private translate: TranslateService) {}

  ngOnInit() {}

  generatePDF() {
    const qrcode = this.qrcode.getQRCode();

    const bodywellLogo = new Image();
    const bodywellLogoBlack = new Image();
    const bodywellLike = new Image();
    const qrcodeImg = new Image();
    const bodywellLogoIcon = new Image();
    bodywellLogo.src = '../../../../../../../assets/img/Bodywell-Full.jpg';
    bodywellLogoBlack.src = '../../../../../../../assets/img/Bodywell-Full-black.jpg';
    bodywellLike.src = '../../../../../../../assets/img/rounded-heart.jpg';
    qrcodeImg.src = qrcode;
    bodywellLogoIcon.src = '../../../../../../../assets/img/bodywell-logo.png';

    const doc = new jsPDF({
      orientation: 'p',
      unit: 'pt',
      format: 'a4',
    });
    doc.addFileToVFS('ProximaRegular.ttf', proximaRegular);
    doc.addFileToVFS('ProximaSemibold.ttf', proximaBold);
    doc.addFont('ProximaRegular.ttf', 'proximaRegular ', 'normal', 'Identity-H');
    doc.addFont('ProximaSemibold.ttf', 'proximaSemibold ', 'normal', 'Identity-H');

    doc.setFillColor(24, 111, 218).roundedRect(32, 32, 531, 48, 5, 5, 'F');
    doc.addImage(bodywellLogo, 'JPG', 248, 49.78, 97, 14);

    doc
      .setFontSize(22)
      .setFont('proximaSemibold ')
      .text(`${this.translate.instant('qr-code-pdf-sign-up-to-us-online')}`, 164, 136);

    doc
      .setFontSize(14)
      .setFont('proximaRegular ')
      .text(`${this.translate.instant('qr-code-pdf-scan-qr-code-and-go-to-catalog')}`, 297.64, 167, {
        align: 'center',
        maxWidth: 348,
      });

    doc.setFillColor(229, 229, 229).roundedRect(171, 285, 252, 252, 15, 15, 'F');
    doc.setFillColor(255, 255, 255).roundedRect(173, 287, 248, 248, 15, 15, 'F');

    doc.addImage(qrcodeImg, 'JPG', 186, 300, 222, 222);

    doc
      .setFontSize(10)
      .setFont('proximaRegular ')
      .text(`${this.translate.instant('qr-code-pdf-add-us-to-favorite')}`, 297.64, 669, {
        align: 'center',
        maxWidth: 276,
      });

    doc.setFillColor(245, 245, 245).roundedRect(0, 748, 595, 94, 5, 5, 'F');

    doc
      .setFontSize(10)
      .setFont('proximaSemibold ')
      .text('bodywell.app', 297.64, 775);

    doc
      .setFontSize(10)
      .setFont('proximaSemibold ')
      .text('bodywell.io', 424, 775);

    doc
      .setTextColor(62, 62, 62)
      .setFontSize(10)
      .setFont('proximaRegular ')
      .text(`${this.translate.instant('qr-code-pdf-online-catalog')}`, 297.64, 790);

    doc
      .setTextColor(62, 62, 62)
      .setFontSize(10)
      .setFont('proximaRegular ')
      .text(`${this.translate.instant('qr-code-pdf-online-platform')}`, 424, 790, {
        maxWidth: 123,
      });

    doc.addImage(bodywellLike, 'JPG', 278, 611, 40, 40);
    doc.addImage(bodywellLogoBlack, 'JPG', 54, 767, 98, 14);

    doc.save();
  }
}
