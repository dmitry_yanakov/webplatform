import { Component, OnDestroy, OnInit, HostListener, ViewChild } from '@angular/core';
import { ApiService } from '@core/services/api.service';
import { LazyLoadEvent } from 'primeng/api';
import { take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { StaffService } from '@core/services/staff.service';
import { NotificationMessageService } from '@core/services/notification-message.service';

@Component({
  selector: 'app-table-fired-employees',
  templateUrl: './table-fired-employees.component.html',
  styleUrls: ['./table-fired-employees.component.scss'],
})
export class TableFiredEmployeesComponent implements OnInit, OnDestroy {
  offset = 0;
  count = 7;
  employees = [];
  search = '';

  total: number;

  loading: boolean;

  @ViewChild('dv', { static: false }) dv;

  private unsubscribe: Subject<any> = new Subject();

  constructor(
    private staffService: StaffService,
    private apiService: ApiService,
    private notificationMessageService: NotificationMessageService
  ) {}

  @HostListener('window:keyup', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if (this.dv.paginate) {
      if (event.key === 'ArrowLeft' && this.offset - this.count >= 0) {
        this.dv.paginate({ first: this.offset - this.count, rows: this.count });
      } else if (event.key === 'ArrowRight' && this.offset + this.count < this.total) {
        this.dv.paginate({ first: this.offset + this.count, rows: this.count });
      }
    }
    return;
  }

  ngOnInit() {
    this.staffService.setIsFiredStatus(true);
    this.staffService.loading.pipe(takeUntil(this.unsubscribe)).subscribe((loading) => (this.loading = loading));
    this.staffService.search.pipe(takeUntil(this.unsubscribe)).subscribe((search) => (this.search = search));
    this.staffService.employees.pipe(takeUntil(this.unsubscribe)).subscribe(({ data, offset, count, total }) => {
      this.employees = data;
      this.offset = offset;
      this.count = count;
      this.total = total;
    });
  }

  // event.first = First row offset
  // event.rows = Number of rows per page
  // event.sortField = Field name to sort with
  // event.sortOrder = Sort order as number, 1 for asc and -1 for dec
  // filters: FilterMetadata object having field as key and filter value, filter matchMode as value
  loadEmployeesLazy(event: LazyLoadEvent) {
    this.staffService.loading.next(true);

    this.staffService
      .getEmployees({ offset: event.first, count: event.rows })
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(() => this.staffService.loading.next(false));
  }

  restoreEmployee(employee_id) {
    this.apiService
      .restoreEmployee(employee_id)
      .pipe(take(1))
      .subscribe((res) => {
        if (res['status'] === 'success') {
          const index = this.employees.findIndex((employee) => employee.id === employee_id);

          this.employees = [...this.employees.slice(0, index), ...this.employees.slice(index + 1)];
          this.notificationMessageService.pushAlert({ detail: 'notification-restore-employee' });
        }
      });
  }

  clearSearchField() {
    this.staffService.loading.next(true);
    this.staffService.search.next('');
    this.staffService
      .getEmployees({})
      .pipe(take(1))
      .subscribe(() => this.staffService.loading.next(false));
  }

  ngOnDestroy() {
    this.staffService.search.next('');
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
