import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableFiredEmployeesComponent } from './table-fired-employees.component';

describe('TableFiredEmployeesComponent', () => {
  let component: TableFiredEmployeesComponent;
  let fixture: ComponentFixture<TableFiredEmployeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TableFiredEmployeesComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableFiredEmployeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
