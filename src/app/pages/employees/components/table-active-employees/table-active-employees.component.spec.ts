import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableActiveEmployeesComponent } from './table-active-employees.component';

describe('TableActiveEmployeesComponent', () => {
  let component: TableActiveEmployeesComponent;
  let fixture: ComponentFixture<TableActiveEmployeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TableActiveEmployeesComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableActiveEmployeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
