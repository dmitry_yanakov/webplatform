import { Component, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { LazyLoadEvent } from 'primeng/api';
import { takeUntil, take } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { StaffService } from '@core/services/staff.service';

@Component({
  selector: 'app-table-active-employees',
  templateUrl: './table-active-employees.component.html',
  styleUrls: ['./table-active-employees.component.scss'],
})
export class TableActiveEmployeesComponent implements OnInit, OnDestroy {
  offset = 0;
  count = 7;
  total: number;
  employees = [];
  loading = false;
  search = '';

  @ViewChild('dv', { static: false }) dv;

  private unsubscribe: Subject<any> = new Subject();

  constructor(private staffService: StaffService) {}

  @HostListener('window:keyup', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if (this.dv.paginate) {
      if (event.key === 'ArrowLeft' && this.offset - this.count >= 0) {
        this.dv.paginate({ first: this.offset - this.count, rows: this.count });
      } else if (event.key === 'ArrowRight' && this.offset + this.count < this.total) {
        this.dv.paginate({ first: this.offset + this.count, rows: this.count });
      }
    }
    return;
  }

  ngOnInit() {
    this.staffService.setIsFiredStatus(false);
    this.staffService.loading.pipe(takeUntil(this.unsubscribe)).subscribe((loading) => (this.loading = loading));
    this.staffService.search.pipe(takeUntil(this.unsubscribe)).subscribe((search) => (this.search = search));
    this.staffService.employees.pipe(takeUntil(this.unsubscribe)).subscribe(({ data, offset, count, total }) => {
      this.employees = data;
      this.offset = offset;
      this.count = count;
      this.total = total;
    });
  }

  // event.first = First row offset
  // event.rows = Number of rows per page
  // event.sortField = Field name to sort with
  // event.sortOrder = Sort order as number, 1 for asc and -1 for dec
  // filters: FilterMetadata object having field as key and filter value, filter matchMode as value
  loadEmployeesLazy(event: LazyLoadEvent) {
    this.staffService.loading.next(true);

    this.staffService
      .getEmployees({ offset: event.first, count: event.rows })
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(() => this.staffService.loading.next(false));
  }

  clearSearchField() {
    this.staffService.loading.next(true);
    this.staffService.search.next('');
    this.staffService.getEmployees({}).pipe(take(1))
      .subscribe(() => this.staffService.loading.next(false));
  }

  ngOnDestroy() {
    this.staffService.search.next('');
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
