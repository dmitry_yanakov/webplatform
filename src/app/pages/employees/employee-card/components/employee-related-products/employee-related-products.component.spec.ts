import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeRelatedProductsComponent } from './employee-related-products.component';

describe('EmployeeRelatedProductsComponent', () => {
  let component: EmployeeRelatedProductsComponent;
  let fixture: ComponentFixture<EmployeeRelatedProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmployeeRelatedProductsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeRelatedProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
