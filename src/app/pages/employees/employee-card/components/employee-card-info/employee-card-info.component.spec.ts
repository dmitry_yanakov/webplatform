import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeCardInfoComponent } from './employee-card-info.component';

describe('EmployeeCardInfoComponent', () => {
  let component: EmployeeCardInfoComponent;
  let fixture: ComponentFixture<EmployeeCardInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmployeeCardInfoComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeCardInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
