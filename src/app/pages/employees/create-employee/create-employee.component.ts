import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from '@core/services/api.service';
import { IPhoneCodes } from 'src/app/shared/components/phone-field/phone-field.component';
import { mergeMap, takeUntil, tap } from 'rxjs/operators';
import { Observable, of, Subject } from 'rxjs';
import { Location } from '@angular/common';
import { CommonDataService } from '@core/services/common-data.service';
import { MessageService } from 'primeng/api';
import { NotificationMessageService } from '@core/services/notification-message.service';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.scss'],
})
export class CreateEmployeeComponent implements OnInit, OnDestroy {
  createEmployeeForm: FormGroup;
  selectedTab = 0;
  phoneCodes: IPhoneCodes;
  branches = [];
  roles;
  businessType;

  subscriptions: Subject<any> = new Subject<any>();

  constructor(
    private fb: FormBuilder,
    public translate: TranslateService,
    private apiService: ApiService,
    private location: Location,
    private commonDataService: CommonDataService,
    private notificationMessageService: NotificationMessageService
  ) {}

  ngOnInit() {
    this.businessType = this.commonDataService.currentCompany.session;
    this.initForm();
    this.getPhoneCodes();
    this.getRoles();
    this.getBranches();
  }

  initForm() {
    this.createEmployeeForm = this.fb.group({
      employeeInfo: this.fb.group({
        branch_id: this.commonDataService.currentBranch.id,
        avatar: '',
        first_name: ['', [Validators.required]],
        last_name: ['', [Validators.required]],
        role_id: [null, [Validators.required]],
        is_service: '',
        email: ['', [Validators.required, Validators.email]],
        phone: [
          {
            name: this.commonDataService.userLocation.country,
            code: this.commonDataService.userLocation.code.toLowerCase(),
            phone_code: this.commonDataService.userLocation.phone_code,
          },
        ],
        phone_number: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(23)]],
      }),
      employeeProducts: this.fb.group({
        products: [[]],
      }),
    });
  }

  getPhoneCodes() {
    this.apiService
      .getPhoneCodes()
      .pipe(takeUntil(this.subscriptions))
      .subscribe((codesResp) => {
        this.phoneCodes = codesResp['data'].map(
          (phone): IPhoneCodes => {
            return {
              label: phone.name,
              value: {
                name: this.translate.instant(phone.name),
                code: phone.code.toLowerCase(),
                phone_code: phone.phone_code,
              },
            };
          }
        );
      });
  }

  getRoles() {
    this.apiService
      .getRoles()
      .pipe(takeUntil(this.subscriptions))
      .subscribe((res) => {
        if (res['status'] === 'success') {
          this.roles = res['data']
            .map((role) => ({ label: role.title, value: role.id }))
            .filter((i) => {
              if (this.commonDataService.currentCompany.role_id === 1) {
                return i.value !== 1;
              } else if (this.commonDataService.currentCompany.role_id === 3) {
                return i.value === 5 || i.value === 4;
              }
              return false;
            });
        }
      });
  }

  getBranches() {
    this.commonDataService.branches.map((branch) => {
      this.branches.push({ label: branch.title, value: branch.id });
    });
  }

  switchTab(tabIndex) {
    this.selectedTab = tabIndex;
  }

  goBack() {
    this.location.back();
  }

  onSubmit() {
    const employee = this.createEmployeeForm.value.employeeInfo;
    const employeeData = {
      branch_id: employee.branch_id,
      role: employee.role_id,
      first_name: employee.first_name,
      last_name: employee.last_name,
      is_service: Number(employee.is_service),
      email: employee.email,
      phone_code: employee.phone.phone_code,
      phone_country: employee.phone.name,
      phone_number: employee.phone_number,
      avatar: employee.avatar,
    };

    this.apiService
      .createEmployee(employeeData)
      .pipe(
        takeUntil(this.subscriptions),
        mergeMap((res) => {
          if (res['status'] === 'success') {
            if (
              employeeData.is_service &&
              (this.commonDataService.currentCompany.role_id === 1 || this.commonDataService.currentCompany.role_id === 3)
            ) {
              const products = this.createEmployeeForm.value.employeeProducts.products.join(',');

              return this.apiService.addEmployeeProducts(res['data']['employee_id'], products || -1);
            }
            return of(res);
          }
        })
      )
      .subscribe((res) => {
        if (res['status'] === 'success') {
          this.notificationMessageService.pushAlert({ detail: 'notification-create-employee' });
          this.goBack();
        }
      });
  }

  ngOnDestroy() {
    this.subscriptions.next();
    this.subscriptions.complete();
  }
}
