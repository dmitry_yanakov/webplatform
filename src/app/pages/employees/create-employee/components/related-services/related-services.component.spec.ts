import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelatedServicesComponent } from './related-services.component';

describe('RelatedServicesComponent', () => {
  let component: RelatedServicesComponent;
  let fixture: ComponentFixture<RelatedServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RelatedServicesComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatedServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
