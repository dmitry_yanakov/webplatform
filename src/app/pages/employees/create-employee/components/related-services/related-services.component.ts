import { Component, OnInit, Output, EventEmitter, Input, OnDestroy, Pipe, PipeTransform } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from '@core/services/api.service';
import { CommonDataService } from '@core/services/common-data.service';
import { StaffService } from '@core/services/staff.service';
import { forkJoin, Subject, of } from 'rxjs';
import { catchError, mergeMap, takeUntil } from 'rxjs/operators';
import { error } from 'util';

@Component({
  selector: 'app-related-services',
  templateUrl: './related-services.component.html',
  styleUrls: ['./related-services.component.scss'],
})
export class RelatedServicesComponent implements OnInit, OnDestroy {
  employeeProductsForm: FormGroup;

  isLoading = true;
  products = [];
  categories = [];
  selectedProducts = [];
  filteredProductsByCategory = [];
  searchValue$: Subject<string> = new Subject<string>();

  @Input() display;
  @Input() branch_id;
  @Input() businessType;
  @Input() employeeProductsFormGroup;

  private unsubscribe: Subject<any> = new Subject<any>();

  constructor(private fb: FormBuilder, private staffService: StaffService) {}

  ngOnInit() {
    this.initForm();

    forkJoin([this.staffService.getProducts(this.branch_id), this.staffService.getCategory(this.branch_id)])
      .pipe(
        catchError(() => {
          return of([[], []]);
        }),
        takeUntil(this.unsubscribe)
      )
      .subscribe(([products, categories]) => {
        this.categories = categories;
        this.products = products;
        this.isLoading = false;
        this.getFilteredProductsByCategory();
      });

    this.searchValue$.pipe(takeUntil(this.unsubscribe)).subscribe((searchValue) => {
      this.getFilteredProductsByCategory(searchValue);
    });
  }

  searchProducts(text) {
    this.searchValue$.next(text.toLowerCase());
  }

  initForm() {
    this.employeeProductsForm = this.employeeProductsFormGroup;
    this.selectedProducts = this.employeeProductsFormGroup.value.products;
  }

  getFilteredProductsByCategory(searchValue: string = '') {
    this.filteredProductsByCategory = this.categories.reduce((acc, category) => {
      const filteredProducts = this.products.filter((product) => {
        if (searchValue) {
          const search = product.title.toLowerCase().indexOf(searchValue) !== -1;

          return product.category_id === category.id && search;
        }

        return product.category_id === category.id;
      });

      if (filteredProducts.length) {
        acc.push({ category: category.title, products: filteredProducts });
      }

      return acc;
    }, []);
  }

  toggleProduct(product_id) {
    const arrayId = this.selectedProducts.findIndex((i) => i === product_id);

    if (arrayId === -1) {
      this.selectedProducts.push(product_id);
    } else {
      this.selectedProducts = [...this.selectedProducts.slice(0, arrayId), ...this.selectedProducts.slice(arrayId + 1)];
    }

    this.employeeProductsForm.patchValue({
      products: this.selectedProducts,
    });
  }

  toggleAllProducts() {
    if (this.products.length === this.selectedProducts.length) {
      this.selectedProducts = [];
    } else {
      this.selectedProducts = this.products.map((product) => product.id);
    }

    this.employeeProductsForm.patchValue({
      products: this.selectedProducts,
    });
  }

  public clearSearchField() {
    return this.searchValue$.next('');
  }

  public checkProductSelected(product_id) {
    return this.selectedProducts.findIndex((i) => i === product_id) !== -1;
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
