import { Component, OnDestroy, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonDataService } from '@core/services/common-data.service';
import { ApiService } from '@core/services/api.service';
import { takeUntil, mergeMap } from 'rxjs/operators';
import { Subject, of } from 'rxjs';
import { StaffService } from '@core/services/staff.service';
import { NotificationMessageService } from '@core/services/notification-message.service';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.scss'],
})
export class EditEmployeeComponent implements OnInit, OnDestroy {
  editEmployeeForm: FormGroup;
  businessType: number;
  selectedTab = 0;
  roles = [];

  @Input() modal;
  @Input() employee;

  private unsubscribe: Subject<any> = new Subject<any>();

  constructor(
    private fb: FormBuilder,
    private commonDataService: CommonDataService,
    private apiService: ApiService,
    private staffService: StaffService,
    private notificationMessageService: NotificationMessageService
  ) {}

  ngOnInit() {
    this.businessType = this.commonDataService.currentCompany.session;
    this.initForm();
    this.getRoles();
  }

  initForm() {
    this.editEmployeeForm = this.fb.group({
      employeeInfo: this.fb.group({
        employee_id: this.employee.id,
        branch_id: this.employee.branch_id,
        avatar: this.employee.avatar,
        role_id: [this.employee.role, [Validators.required]],
        is_service: this.employee.is_service,
      }),
      employeeProducts: this.fb.group({
        products: [this.getEmployeeProducts()],
      }),
    });
  }

  private getEmployeeProducts() {
    const productsType = this.businessType === 0 ? 'products' : this.businessType === 1 ? 'groups' : '';
    return this.employee[productsType].map((product) => product.product_id);
  }

  getRoles() {
    this.apiService
      .getRoles()
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((res) => {
        if (res['status'] === 'success') {
          this.roles = res['data']
            .map((role) => ({ label: role.title, value: role.id }))
            .filter((i) => {
              if (this.commonDataService.currentCompany.role_id === 1) {
                return i.value !== 1;
              } else if (this.commonDataService.currentCompany.role_id === 3) {
                return i.value === 5 || i.value === 4;
              }
              return false;
            });
        }
      });
  }

  switchTab(tabIndex) {
    this.selectedTab = tabIndex;
  }

  goBack() {
    this.modal.toggle();
  }

  onSubmit() {
    const employee = this.editEmployeeForm.value.employeeInfo;
    const employeeData = {
      employee_id: employee.employee_id,
      branch_id: employee.branch_id,
      role: employee.role_id,
      is_service: Number(employee.is_service),
      avatar: employee.avatar || '',
    };

    this.apiService
      .editEmployee(employeeData)
      .pipe(
        takeUntil(this.unsubscribe),
        mergeMap((res) => {
          if (res['status'] === 'success') {
            if (
              employeeData.is_service &&
              (this.commonDataService.currentCompany.role_id === 1 || this.commonDataService.currentCompany.role_id === 3)
            ) {
              const products = this.editEmployeeForm.value.employeeProducts.products.join(',');

              return this.apiService.addEmployeeProducts(employee.employee_id, products || -1);
            }
            return of(res);
          }
        })
      )
      .subscribe((res) => {
        if (res['status'] === 'success') {
          this.staffService.employeeUpdated.emit(true);
          this.modal.toggle();
          this.notificationMessageService.pushAlert({ detail: 'notification-update-employee' });
        }
      });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
