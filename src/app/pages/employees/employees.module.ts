import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeesComponent } from './employees.component';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { CropperModule, HeaderModule, ScrollPanelModule, SharedModule } from '../../shared';
import { TableActiveEmployeesComponent } from './components/table-active-employees/table-active-employees.component';
import { TableFiredEmployeesComponent } from './components/table-fired-employees/table-fired-employees.component';
import { TableModule } from 'primeng/table';
import { DataViewModule } from 'primeng/dataview';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateEmployeeComponent } from './create-employee/create-employee.component';
import { GeneralComponent } from './create-employee/components/general/general.component';
import { RelatedServicesComponent } from './create-employee/components/related-services/related-services.component';
import { DropdownModule } from 'primeng/dropdown';
import { EmployeeCardComponent } from './employee-card/employee-card.component';
import { EditEmployeeComponent } from './edit-employee/edit-employee.component';
import { EmployeeCardInfoComponent } from './employee-card/components/employee-card-info/employee-card-info.component';
import { EmployeeRelatedProductsComponent } from './employee-card/components/employee-related-products/employee-related-products.component';

@NgModule({
  declarations: [
    EmployeesComponent,
    TableActiveEmployeesComponent,
    TableFiredEmployeesComponent,
    CreateEmployeeComponent,
    GeneralComponent,
    RelatedServicesComponent,
    EmployeeCardComponent,
    EditEmployeeComponent,
    EmployeeCardInfoComponent,
    EmployeeRelatedProductsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    SharedModule,
    HeaderModule,
    ScrollPanelModule,
    TableModule,
    DataViewModule,
    FormsModule,
    CropperModule,
    ReactiveFormsModule,
    DropdownModule,
  ],
})
export class EmployeesModule {}
