import { Component, OnInit } from '@angular/core';
import { ApiService } from '@core/services/api.service';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
})
export class NotFoundComponent implements OnInit {
  isLoggedIn = false;
  constructor(private api: ApiService) {}

  ngOnInit() {
    this.isLoggedIn = !!this.api.token;
  }
}
