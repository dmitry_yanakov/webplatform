import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateLocationComponent } from './create-location.component';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ScrollPanelModule, SharedModule } from 'src/app/shared';
import { InputSwitchModule } from 'primeng/inputswitch';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    ScrollPanelModule,
    SharedModule,
    InputSwitchModule,
    AgmCoreModule,
  ],
  declarations: [CreateLocationComponent],
})
export class CreateLocationModule {}
