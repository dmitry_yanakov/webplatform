import { Component, OnInit, ElementRef, NgZone, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from '@core/services/api.service';
import { IPhoneCodes } from 'src/app/shared/components/phone-field/phone-field.component';
import { MapsAPILoader, MouseEvent } from '@agm/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-create-location',
  templateUrl: './create-location.component.html',
  styleUrls: ['./create-location.component.scss'],
})
export class CreateLocationComponent implements OnInit {
  createLocation: FormGroup;
  phoneCodes: Array<IPhoneCodes> = [];
  items: FormArray;
  checked = false;
  workHoursStep = false;
  daysOfTheWeek = [
    { day: 1, label: 'Monday', timeFrom: '09:00', timeTo: '18:00', is_active: true },
    { day: 2, label: 'Tuesday', timeFrom: '09:00', timeTo: '18:00', is_active: true },
    { day: 3, label: 'Wednesday', timeFrom: '09:00', timeTo: '18:00', is_active: true },
    { day: 4, label: 'Thursday', timeFrom: '09:00', timeTo: '18:00', is_active: true },
    { day: 5, label: 'Friday', timeFrom: '09:00', timeTo: '18:00', is_active: true },
    { day: 6, label: 'Saturday', timeFrom: '09:00', timeTo: '18:00', is_active: false },
    { day: 7, label: 'Sunday', timeFrom: '09:00', timeTo: '18:00', is_active: false },
  ];
  private geoCoder;

  public latitude: number;
  public longitude: number;
  public street_number: string;
  public route: string;
  public locality: string;
  public country: string;
  public city: string;
  public postalCode: string;
  public placeId: string;
  public address: string;
  public phoneFormat: string;
  public searchControl: FormControl;
  public zoom: number;

  @ViewChild('search', { static: false }) public searchElementRef: ElementRef;
  constructor(
    private fb: FormBuilder,
    private translate: TranslateService,
    private apiService: ApiService,
    private mapsAPILoader: MapsAPILoader,
    public location: Location,
    private ngZone: NgZone
  ) {}

  ngOnInit() {
    this.loadPlacesAutocomplete();
    this.getPhoneCodes();
    this.initCreateLocationForm();
  }

  initCreateLocationForm() {
    this.createLocation = this.fb.group({
      title: ['', [Validators.required]],
      zip: ['', [Validators.required]],
      phones: this.fb.array([this.createPhone()]),
      country: [''],
      state: [''],
      city: [''],
      street: [''],
      latitude: [''],
      longitude: [''],
    });
  }

  get formPhones() {
    return <FormArray>this.createLocation.get('phones');
  }

  loadPlacesAutocomplete() {
    // load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder();

      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['address'],
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          // get the place result
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();

          // verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          // set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });
  }

  private getAddressComponentByPlace(place) {
    const components = place.address_components;
    let country = null;
    let city = null;
    let postalCode = null;
    let street_number = null;
    let route = null;
    let locality = null;

    for (let i = 0, component; (component = components[i]); i++) {
      if (component.types[0] === 'country') {
        country = component['long_name'];
      }
      if (component.types[0] === 'administrative_area_level_1') {
        city = component['long_name'];
      }
      if (component.types[0] === 'postal_code') {
        postalCode = component['short_name'];
      }
      if (component.types[0] === 'street_number') {
        street_number = component['short_name'];
      }
      if (component.types[0] === 'route') {
        route = component['long_name'];
      }
      if (component.types[0] === 'locality') {
        locality = component['short_name'];
      }
    }

    this.address = place['formatted_address'];
    this.createLocation.patchValue({
      title: this.address,
      zip: postalCode,
      country: country,
      state: city,
      city: locality,
      street: `${route}, ${street_number}`,
      latitude: this.latitude,
      longitude: this.longitude,
    });
  }

  // Get Current Location Coordinates
  public setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 8;
        this.getAddress(this.latitude, this.longitude);
      });
    }
  }

  markerDragEnd($event: MouseEvent) {
    console.log($event);
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ location: { lat: latitude, lng: longitude } }, (results, status) => {
      console.log(results);
      console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.getAddressComponentByPlace(results[0]);
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
    });
  }

  getPhoneCodes() {
    this.apiService.getPhoneCodes().subscribe((codesResp) => {
      const codes = codesResp['data'].map(
        (phone): IPhoneCodes => {
          return {
            label: phone.name,
            value: {
              name: this.translate.instant(phone.name),
              code: phone.code.toLowerCase(),
              phone_code: phone.phone_code,
            },
          };
        }
      );
      this.phoneCodes = codes;
    });
  }

  createPhone(): FormGroup {
    return this.fb.group({
      phone_code: [
        {
          name: 'Албания',
          code: 'al',
          phone_code: 355,
        },
      ],
      phone_number: ['', [Validators.minLength(6), Validators.required]],
    });
  }

  addPhone(): void {
    this.items = this.createLocation.get('phones') as FormArray;
    this.items.push(this.createPhone());
  }

  removePhone(i: number): void {
    (<FormArray>this.createLocation.get('phones')).removeAt(i);
  }

  goToWorkHours() {
    this.workHoursStep = true;
    console.log(this.createLocation.value, this.address, this.street_number, this.city, this.country);
  }

  goToLocationAdress() {
    this.workHoursStep = false;
  }

  log() {
    console.log(this.daysOfTheWeek);
  }
}
