import { DeskComponent } from './desk.component';
import { SharedModule } from './../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { TabViewModule } from 'primeng/tabview';
import { HeaderModule } from '../../shared/modules';
import { TranslateModule } from '@ngx-translate/core';
import { ScrollPanelModule } from 'src/app/shared';

@NgModule({
  declarations: [DeskComponent],
  imports: [CommonModule, ReactiveFormsModule, SharedModule, RouterModule, TabViewModule, HeaderModule, TranslateModule, ScrollPanelModule],
})
export class DeskModule {}
