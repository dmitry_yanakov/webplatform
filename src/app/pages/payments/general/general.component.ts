import { Component, OnInit, Inject } from '@angular/core';
import { ApiService } from '@core/services/api.service';
import { CommonDataService } from '@core/services/common-data.service';
import * as moment from 'moment';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss'],
})
export class GeneralComponent implements OnInit {
  isOpen = false;
  companyTypes;
  typesPrice: any;
  prices = [];
  selectedType: any;
  subscribeFinishDate;
  leftDays = 0;
  balance = '';
  testPeriod;
  subscribeDate;
  selectedPrice;
  constructor(private apiService: ApiService, private commonData: CommonDataService, @Inject(DOCUMENT) private document: Document) {}

  ngOnInit() {
    this.getCompanyTypes();
    this.getTypesPrice();
    this.transformAllowDate();
    this.calculateLeftDays();
    this.showCurrentBalance();
    this.testPeriod = this.commonData.currentCompany.is_credit;
    this.subscribeDate = this.commonData.currentCompany.date;
  }

  transformAllowDate() {
    this.subscribeFinishDate = moment(this.commonData.currentCompany.allow_date).format('LL');
  }

  calculateLeftDays() {
    const oneDay = 24 * 60 * 60 * 1000;
    this.leftDays = Math.round(Math.abs((Date.parse(this.commonData.currentCompany.allow_date) - Date.now()) / oneDay));
  }

  showCurrentBalance() {
    const balance = this.commonData.currentCompany.balance;
    const currency_symbol = this.commonData.currentCompany.currency_symbol;
    this.balance = balance + ' ' + currency_symbol;
  }

  getCompanyTypes() {
    this.apiService.getCompanyTypes().subscribe((response: any) => {
      if (response.status === 'success') {
        this.companyTypes = response.data;
        this.selectedType = this.companyTypes.find((type) => type.id === this.commonData.currentCompany.type_id);
      }
    });
  }

  getTypesPrice() {
    this.apiService.getTypesPrice().subscribe((response: any) => {
      if (response.status === 'success') {
        this.typesPrice = response.data;
        this.prices = this.typesPrice.filter((type) => type.company_type_id === this.commonData.currentCompany.type_id).reverse();
      }
    });
  }

  getColor(period) {
    switch (period) {
      case 12:
        return '#63C57E';
      case 3:
        return '#5593DF';
      case 1:
        return '#99B1CE';
    }
  }

  toggleOpen(type_id?) {
    this.selectedPrice = type_id;
    this.isOpen = !this.isOpen;
  }

  doPayment(payment_system: string) {
    switch (payment_system) {
      case 'interkassa': {
        this.apiService.payInterKassa(this.selectedPrice).subscribe((response: any) => {
          if (response.status === 'success') {
            this.parsePayDataToUrl(response);
          }
        });
        break;
      }
      case 'liqpay': {
        this.apiService.payLiqPay(this.selectedPrice).subscribe((response: any) => {
          if (response.status === 'success') {
            this.parsePayDataToUrl(response);
          }
        });
        break;
      }
      default: {
        break;
      }
    }
  }

  parsePayDataToUrl(response) {
    const params = response.data.parameters;
    let paramsStr = '';
    Object.keys(params).forEach((param) => (paramsStr += `${param}=${params[param]}&`));
    this.document.location.href = `${response.data.url}?${paramsStr}`;
  }
}
