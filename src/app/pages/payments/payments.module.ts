import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentsComponent } from './payments.component';
import { PaymentHistoryComponent } from './payment-history/payment-history.component';
import { GeneralComponent } from './general/general.component';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { TableModule } from 'primeng/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { HeaderModule, ScrollPanelModule, SharedModule } from '../../shared';
import { DataViewModule } from 'primeng/dataview';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    SharedModule,
    HeaderModule,
    ScrollPanelModule,
    TableModule,
    FormsModule,
    ReactiveFormsModule,
    DropdownModule,
    DataViewModule,
  ],
  declarations: [PaymentsComponent, PaymentHistoryComponent, GeneralComponent],
})
export class PaymentsModule {}
