import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/components/common/menuitem';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from '@core/services/api.service';

@Component({
  selector: 'app-create-company',
  templateUrl: './create-company.component.html',
  styleUrls: ['./create-company.component.scss'],
})
export class CreateCompanyComponent implements OnInit {
  items: MenuItem[];
  activeIndex = 0;
  createCompanyForm: FormGroup;
  createdCompanyId: number;
  constructor(private fb: FormBuilder, public translate: TranslateService, private api: ApiService) {}

  ngOnInit() {
    this.items = [
      { label: 'create-company-session-type-item-rate-selection' },
      { label: 'create-company-session-type-item-general-info' },
      { label: 'create-company-session-type-item-locations' },
      { label: 'create-company-session-type-item-session-type' },
      { label: 'create-company-session-type-item-staff' },
    ];
    this.getItemsTranslate();
    this.initCreateCompanyForm();
  }

  getItemsTranslate() {
    this.items.forEach((item, index) => {
      this.translate.get(item.label).subscribe((value) => {
        this.items[index].label = value;
      });
    });
  }

  initCreateCompanyForm() {
    this.createCompanyForm = this.fb.group({});
  }

  // After a form is initialized, we link it to our main form
  formInitialized(name: string, form: FormGroup) {
    this.createCompanyForm.setControl(name, form);
  }

  goToStep(pageNumber, companyId?) {
    this.activeIndex = pageNumber;
  }

  createCompany() {
    const sendingForm = {
      ...this.createCompanyForm.get('generalInfoForm').value,
      type_id: this.createCompanyForm.get('rateSelection').value.type_id,
    };

    this.api.createCompany({ ...sendingForm }).subscribe((response: any) => {
      if (response.status === 'success') {
        this.createdCompanyId = response.data.company_id;
        this.goToStep(2, this.createdCompanyId);
      }
    });
  }
}
