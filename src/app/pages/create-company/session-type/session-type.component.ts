import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-session-type',
  templateUrl: './session-type.component.html',
  styleUrls: ['./session-type.component.scss'],
})
export class SessionTypeComponent implements OnInit, OnDestroy {
  sessionTypes = [];
  selectedType: any;
  sessionTypeForm: FormGroup;
  @Output() formReady = new EventEmitter<FormGroup>();
  @Output() changed = new EventEmitter();
  constructor(private fb: FormBuilder, public translate: TranslateService) {}

  ngOnInit() {
    this.sessionTypes = [
      { label: 'create-company-session-type-services', value: '0' },
      { label: 'create-company-session-type-group-classes', value: '1' },
    ];
    this.selectedType = this.sessionTypes[1];
    this.initSessionTypeFormForm();
    this.populateForm();
  }

  initSessionTypeFormForm() {
    this.sessionTypeForm = this.fb.group({
      typeId: '',
    });

    // Emit the form group to the father to do whatever it wishes
    this.formReady.emit(this.sessionTypeForm);
  }

  populateForm() {
    this.sessionTypeForm.patchValue({
      typeId: `${this.selectedType.value}`,
    });
  }

  onSelectType(type) {
    this.selectedType = type;
    this.populateForm();
  }

  continueCreation() {
    console.log(this.selectedType);
  }

  ngOnDestroy(): void {}
}
