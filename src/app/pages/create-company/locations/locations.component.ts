import { Component, OnInit, Input, Type, NgZone } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from '@core/services/api.service';
import { CommonDataService } from '@core/services/common-data.service';
import { Router } from '@angular/router';
import { MapsAPILoader, MouseEvent } from '@agm/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss'],
})
export class LocationsComponent implements OnInit {
  @Input() type;
  @Input() companyId;
  branches: any;
  company_id = this.companyId || this.commonData.currentCompany.id;
  phone_code;
  isOpen = false;
  selectedLocationId;
  constructor(
    public translate: TranslateService,
    private api: ApiService,
    private commonData: CommonDataService,
    private router: Router,
    public location: Location
  ) {}

  ngOnInit() {
    const company_id = this.companyId || this.commonData.currentCompany.id;
    this.getBranches(company_id);
  }

  getBranches(company_id) {
    this.api.getCompanyBranch(company_id).subscribe((res) => {
      if (res['status'] === 'success') {
        this.commonData.branches = res['data']['branches'];
        this.branches = res['data']['branches'];
      }
    });
  }

  getPhonesArray(i) {
    return JSON.parse(this.branches[i].phones);
  }

  toggleOpen(location?) {
    this.selectedLocationId = location;
    this.isOpen = !this.isOpen;
  }

  removeLocation() {
    console.log(this.selectedLocationId);
    this.api.deactivateBranch({ branch_id: this.selectedLocationId }).subscribe((res) => {
      if (res['status'] === 'success') {
        this.toggleOpen();
        this.getBranches(this.company_id);
      }
    });
  }
}
