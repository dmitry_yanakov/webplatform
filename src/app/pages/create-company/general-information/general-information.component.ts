import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { ApiService } from '@core/services/api.service';
import { TranslateService } from '@ngx-translate/core';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-general-information',
  templateUrl: './general-information.component.html',
  styleUrls: ['./general-information.component.scss'],
})
export class GeneralInformationComponent implements OnInit {
  @Output() formReady = new EventEmitter<FormGroup>();
  @Output() createCompany = new EventEmitter();
  generalInfoForm: FormGroup;
  currencies = [];
  industries = [];
  languages = [
    { label: 'Русский', value: 'ru', img: 'assets/flags/248-russia.svg' },
    { label: 'English', value: 'en', img: 'assets/flags/226-united-states.svg' },
    { label: 'French', value: 'fr', img: 'assets/flags/195-france.svg' },
  ];
  constructor(private fb: FormBuilder, private api: ApiService) {}

  ngOnInit() {
    this.getIndustry();
    this.getCurrencies();
    this.initGeneralInfoForm();
  }

  initGeneralInfoForm() {
    const selectedLanguage = localStorage.getItem('locale') ? localStorage.getItem('locale') : 'en';
    this.generalInfoForm = this.fb.group({
      logo: '',
      title: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(64)]],
      industry_id: [1, Validators.required],
      currency_id: [1, Validators.required],
      language: [selectedLanguage, Validators.required],
      description: [''],
    });

    // Emit the form group to the father to do whatever it wishes
    this.formReady.emit(this.generalInfoForm);
  }

  getIndustry() {
    this.api
      .getIndustries()
      .pipe(take(1))
      .subscribe((res) => {
        this.industries = res['data'].reduce((acc: any[], value: any): any[] => {
          const categoryIndex = acc.findIndex((i) => i.label === value.category);
          if (categoryIndex === -1) {
            const category = { label: value.category, items: [] };
            category.items.push({ label: value.title, value: value.id });
            acc.push(category);
          } else {
            acc[categoryIndex].items.push({ label: value.title, value: value.id });
          }
          return acc;
        }, []);
      });
  }

  getCurrencies() {
    this.api
      .getCurrencies()
      .pipe(take(1))
      .subscribe((res) => {
        this.currencies = res['data'].map((currency: any) => {
          return { label: currency.title, value: currency.id };
        });
      });
  }

  companyCreation(pageNumber) {
    if (!this.generalInfoForm.valid) {
      return;
    }
    this.createCompany.emit(pageNumber);
  }
}
