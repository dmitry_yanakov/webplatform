import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule, CropperModule } from 'src/app/shared';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RateSelectionComponent } from './rate-selection/rate-selection.component';
import { GeneralInformationComponent } from './general-information/general-information.component';
import { LocationsComponent } from './locations/locations.component';
import { SessionTypeComponent } from './session-type/session-type.component';
import { StaffComponent } from './staff/staff.component';
import { CreateCompanyComponent } from './create-company.component';
import { StepsModule } from 'primeng/steps';
import { DropdownModule } from 'primeng/dropdown';
import { ScrollPanelModule } from 'src/app/shared/components/scroll-panel/scroll-panel.module';
import { CreateLocationModule } from '../create-location/create-location.module';
import { AgmCoreModule } from '@agm/core';
import { DataViewModule } from 'primeng/dataview';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    StepsModule,
    DropdownModule,
    CropperModule,
    ScrollPanelModule,
    CreateLocationModule,
    AgmCoreModule,
    DataViewModule,
  ],
  declarations: [
    CreateCompanyComponent,
    RateSelectionComponent,
    GeneralInformationComponent,
    LocationsComponent,
    SessionTypeComponent,
    StaffComponent,
  ],
})
export class CreateCompanyModule {}
