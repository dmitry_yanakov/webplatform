import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { ApiService } from '@core/services/api.service';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-rate-selection',
  templateUrl: './rate-selection.component.html',
  styleUrls: ['./rate-selection.component.scss'],
})
export class RateSelectionComponent implements OnInit, OnDestroy {
  companyTypes;
  typesPrice: any;
  prices = [];
  selectedType: any;
  rateSelectionForm: FormGroup;
  @Output() formReady = new EventEmitter<FormGroup>();
  @Output() goToSecondStep = new EventEmitter();
  constructor(private apiService: ApiService, private fb: FormBuilder) {}

  ngOnInit() {
    this.getCompanyTypes();
    this.getTypesPrice();
    this.initRateSelectionForm();
  }

  initRateSelectionForm() {
    this.rateSelectionForm = this.fb.group({
      type_id: '',
    });

    // Emit the form group to the father to do whatever it wishes
    this.formReady.emit(this.rateSelectionForm);
  }

  getCompanyTypes() {
    this.apiService.getCompanyTypes().subscribe((response: any) => {
      if (response.status === 'success') {
        this.companyTypes = response.data;
        this.selectedType = this.companyTypes[1];
        this.populateForm();
      }
    });
  }

  getTypesPrice() {
    this.apiService.getTypesPrice().subscribe((response: any) => {
      if (response.status === 'success') {
        this.typesPrice = response.data;
        this.prices = [this.typesPrice[0], this.typesPrice[3], this.typesPrice[6]];
      }
    });
  }

  populateForm() {
    this.rateSelectionForm.patchValue({
      type_id: `${this.selectedType.id}`,
    });
  }

  onSelectType(type) {
    this.selectedType = type;
    this.populateForm();
  }

  continueCreation(pageNumber) {
    if (!this.rateSelectionForm.valid) {
      return;
    }
    this.goToSecondStep.emit(pageNumber);
  }

  ngOnDestroy(): void {}
}
