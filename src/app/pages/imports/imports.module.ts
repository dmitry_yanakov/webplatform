import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImportsComponent } from './imports.component';
import { SharedModule, HeaderModule } from 'src/app/shared';
import { NgxFileDropModule } from 'ngx-file-drop';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import {ProgressBarModule} from 'primeng/progressbar';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    HeaderModule,
    NgxFileDropModule,
    TranslateModule,
    RouterModule,
    ProgressBarModule
  ],
  declarations: [ImportsComponent]
})
export class ImportsModule { }
