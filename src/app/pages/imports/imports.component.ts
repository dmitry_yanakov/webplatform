import { Component, OnInit, Output, ViewChild, EventEmitter, ElementRef, AfterViewInit } from '@angular/core';
import { Subject } from 'rxjs';
import { NgxFileDropEntry, FileSystemFileEntry } from 'ngx-file-drop';
import Cropper from 'cropperjs';
import { ApiService } from '@core/services/api.service';

@Component({
  selector: 'app-imports',
  templateUrl: './imports.component.html',
  styleUrls: ['./imports.component.scss'],
})
export class ImportsComponent implements OnInit, AfterViewInit {
  @Output() submit: EventEmitter<any> = new EventEmitter();
  @ViewChild('image', { static: false })
  imageElement: ElementRef;
  imageSource = new Subject<any>();

  isOpen = false;
  isReady = false;

  public cropper: Cropper;

  public cropperConfig = {
    width: 1040,
    height: 584,
    minWidth: 256,
    minHeight: 256,
    maxWidth: 4096,
    maxHeight: 4096,
    imageSmoothingEnabled: false,
    imageSmoothingQuality: 'high',
    viewMode: 1,
    dragMode: 'move',
    zoomable: true,
    scalable: true,
    guides: false,
    center: false,
    aspectRatio: 16 / 9,
  };

  constructor(public apiService: ApiService) {}

  ngAfterViewInit() {
    this.cropper = new Cropper(
      this.imageElement.nativeElement,
      // @ts-ignore
      this.cropperConfig
    );
  }

  ngOnInit() {
    this.imageSource.subscribe((img) => {
      if (img) {
        this.cropper.replace(img);
        this.cropper.setCropBoxData({ left: 0, top: 0, width: 250, height: 250 });
        this.cropper.crop();
        this.isReady = true;
      }
    });
  }

  dropped(files: NgxFileDropEntry[]) {
    for (const droppedFile of files) {
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          const reader = new FileReader();

          reader.readAsDataURL(file);

          reader.onload = () => {
            this.imageSource.next(reader.result);
          };
          return;
        });
      }
    }
  }

  onFileChange(event) {
    if (event.target.files && event.target.files.length) {
      const reader = new FileReader();
      const [file] = event.target.files;

      reader.readAsDataURL(file);

      reader.onload = () => {
        this.imageSource.next(reader.result);
      };
    }
  }

  toggleOpen() {
    this.isOpen = !this.isOpen;
  }

  onSubmit() {
    const croppedCanvas = this.cropper.getCroppedCanvas().toDataURL();

    this.submit.emit(croppedCanvas);
  }
}
