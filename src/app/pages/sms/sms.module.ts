import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SmsComponent } from './sms.component';
import { SharedModule, HeaderModule, ScrollPanelModule } from 'src/app/shared';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { InputSwitchModule } from 'primeng/inputswitch';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    HeaderModule,
    TranslateModule,
    RouterModule,
    ScrollPanelModule,
    InputSwitchModule,
    FormsModule,
    DropdownModule,
  ],
  declarations: [SmsComponent],
})
export class SmsModule {}
