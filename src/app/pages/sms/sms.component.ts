import { Component, OnInit } from '@angular/core';
import { IPhoneCodes } from 'src/app/shared/components/phone-field/phone-field.component';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from '@core/services/api.service';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-sms',
  templateUrl: './sms.component.html',
  styleUrls: ['./sms.component.scss'],
})
export class SmsComponent implements OnInit {
  checked = false;
  isOpen = false;
  phoneCodes: Array<IPhoneCodes> = [];
  selectedCountry: SelectItem[];
  constructor(private translate: TranslateService, private apiService: ApiService) {}

  ngOnInit() {
    this.getPhoneCodes();
  }

  toggleOpen() {
    this.isOpen = !this.isOpen;
  }

  getPhoneCodes() {
    this.apiService.getPhoneCodes().subscribe((codesResp) => {
      const codes = codesResp['data'].map(
        (phone): IPhoneCodes => {
          return {
            label: phone.name,
            value: {
              name: this.translate.instant(phone.name),
              code: phone.code.toLowerCase(),
              phone_code: phone.phone_code,
            },
          };
        }
      );
      this.phoneCodes = codes;
    });
  }
}
