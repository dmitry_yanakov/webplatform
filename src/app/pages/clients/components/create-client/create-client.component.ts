import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IPhoneCodes } from 'src/app/shared/components/phone-field/phone-field.component';
import { Subject, of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from '@core/services/api.service';
import { CommonDataService } from '@core/services/common-data.service';
import { takeUntil, mergeMap } from 'rxjs/operators';
import { OverlayPanel } from 'primeng/primeng';
import * as moment from 'moment';
import { ClientService } from '@core/services/client.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-create-client',
  templateUrl: './create-client.component.html',
  styleUrls: ['./create-client.component.scss'],
})
export class CreateClientComponent implements OnInit, OnDestroy {
  createClientForm: FormGroup;
  phoneCodes: IPhoneCodes;
  businessType;
  clientId;
  sources;
  isDropdownOpen = false;
  subscriptions: Subject<any> = new Subject<any>();
  userSex = [
    { id: 1, title: 'male-sex' },
    { id: 2, title: 'female-sex' },
  ];
  moment = moment;
  selectedSex = 3;
  is_ban = 0;
  @ViewChild('lifetimeCalendar', { static: false }) lifetimeCalendar: OverlayPanel;
  constructor(
    private fb: FormBuilder,
    public translate: TranslateService,
    private apiService: ApiService,
    private clientService: ClientService,
    private location: Location,
    private commonDataService: CommonDataService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      this.clientId = params['id'];
    });
    this.businessType = this.commonDataService.currentCompany.session;
    this.initForm();
    this.getPhoneCodes();
    this.getClientSource();

    if (this.clientId) {
      this.getClientData();
    }
  }

  initForm() {
    this.createClientForm = this.fb.group({
      branch_id: this.commonDataService.currentBranch.id,
      avatar: '',
      first_name: ['', [Validators.required]],
      last_name: [''],
      middle_name: [''],
      email: ['', [Validators.email]],
      phone: [
        {
          name: this.commonDataService.userLocation.country,
          code: this.commonDataService.userLocation.code.toLowerCase(),
          phone_code: this.commonDataService.userLocation.phone_code,
        },
      ],
      phone_number: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(23)]],
      birth: [''],
      sex: [3],
      source_id: [28],
      comment: '',
    });
  }

  getPhoneCodes() {
    this.apiService
      .getPhoneCodes()
      .pipe(takeUntil(this.subscriptions))
      .subscribe((codesResp) => {
        this.phoneCodes = codesResp['data'].map(
          (phone): IPhoneCodes => {
            return {
              label: phone.name,
              value: {
                name: this.translate.instant(phone.name),
                code: phone.code.toLowerCase(),
                phone_code: phone.phone_code,
              },
            };
          }
        );
      });
  }

  getClientSource() {
    this.apiService
      .clientSourceGet()
      .pipe(takeUntil(this.subscriptions))
      .subscribe((res: any) => {
        this.sources = res['data'].reverse().map((source) => {
          return {
            label: source.title,
            value: source.id,
          };
        });
      });
  }

  getClientData() {
    this.clientService
      .getClient(this.clientId)
      .pipe(takeUntil(this.subscriptions))
      .subscribe((res: any) => {
        this.createClientForm.patchValue({
          branch_id: res.branch_id,
          avatar: res.avatar,
          first_name: res.first_name,
          last_name: res.last_name,
          middle_name: res.middle_name,
          email: res.email,
          phone: [
            {
              code: res.phone_country,
              phone_code: res.phone_code,
            },
          ],
          phone_number: res.phone_number,
          birth: res.birth,
          sex: res.sex,
          source_id: res.source_id,
          comment: res.comment,
        });
        this.selectedSex = res.sex;
        this.is_ban = res.is_ban;
      });
  }

  goBack() {
    this.location.back();
  }

  createClient() {
    const client = this.createClientForm.value;
    const clientData = {
      branch_id: client.branch_id,
      avatar: client.avatar,
      first_name: client.first_name,
      last_name: client.last_name,
      middle_name: client.middle_name,
      email: client.email,
      phone_country: client.phone.code,
      phone_code: client.phone.phone_code,
      phone_number: client.phone_number,
      birth: client.birth ? this.moment(client.birth).format('DD.MM.YYYY') : null,
      sex: client.sex,
      source_id: client.source_id,
      comment: client.comment,
    };
    this.clientService.createClient(clientData).subscribe((res: any) => {
      if (res !== 0) {
        this.goBack();
      }
    });
  }

  updateClient() {
    const client = this.createClientForm.value;
    const clientData = {
      branch_id: client.branch_id,
      avatar: client.avatar,
      first_name: client.first_name,
      last_name: client.last_name,
      middle_name: client.middle_name,
      email: client.email,
      phone_country: client.phone.code,
      phone_code: client.phone.phone_code,
      phone_number: client.phone_number,
      birth: client.birth ? this.moment(client.birth).format('DD.MM.YYYY') : null,
      sex: client.sex,
      source_id: client.source_id,
      comment: client.comment,
      is_ban: this.is_ban,
      client_id: this.clientId,
    };
    this.clientService.updateClient(clientData).subscribe((res: any) => {
      if (res.status === 'success') {
        this.goBack();
      }
    });
  }

  get comment() {
    return this.createClientForm.get('comment').value;
  }

  rotateIcon() {
    this.isDropdownOpen = !this.isDropdownOpen;
  }

  toggleSex(id) {
    this.selectedSex = id;
    this.createClientForm.patchValue({
      sex: this.selectedSex,
    });
  }

  ngOnDestroy() {
    this.subscriptions.next();
    this.subscriptions.complete();
  }
}
