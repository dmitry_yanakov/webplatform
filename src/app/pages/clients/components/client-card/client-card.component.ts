import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { ClientService } from '@core/services/client.service';
import { Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-client-card',
  templateUrl: './client-card.component.html',
  styleUrls: ['./client-card.component.scss'],
})
export class ClientCardComponent implements OnInit, OnDestroy {
  checked;
  userId;
  userData;
  unsubscribe: Subject<any> = new Subject<any>();
  moment = moment;
  userEmail = null;
  constructor(private location: Location, private clientService: ClientService, private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      this.userId = params['id'];
    });
    this.getUserData();
  }

  getUserData() {
    this.clientService
      .getClient(this.userId)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((response: any) => {
        this.userData = response;
        this.checked = this.userData.is_ban;
        this.userEmail = this.userData.email;
      });
  }

  goBack() {
    this.location.back();
  }

  deleteClient(user_id) {
    console.log(user_id);
  }

  banClient() {
    const is_ban = this.checked ? 1 : 0;
    this.clientService
      .banClient(this.userId, is_ban)
      .pipe(take(1))
      .subscribe();
  }

  inviteClient() {
    console.log(this.userData);
    const phone = `${this.userData.phone_code}${this.userData.phone_number}`;
    if (!this.userEmail) {
      return;
    }
    this.clientService
      .inviteClient(this.userId, this.userEmail, phone)
      .pipe(take(1))
      .subscribe();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
