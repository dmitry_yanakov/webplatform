import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ClientService } from '@core/services/client.service';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { takeUntil, take } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-client-notes',
  templateUrl: './client-notes.component.html',
  styleUrls: ['./client-notes.component.scss'],
})
export class ClientNotesComponent implements OnInit, OnDestroy {
  moment = moment;
  notes = [];
  note = '';
  clientId;
  isRemoveOpen = false;
  isOpen = false;
  selectedNote;
  unsubscribe: Subject<any> = new Subject<any>();
  constructor(private clientService: ClientService) {}

  ngOnInit() {
    this.clientService.clientId$.pipe(takeUntil(this.unsubscribe)).subscribe((data: any) => {
      this.clientId = data;
      this.getUserNotes(this.clientId);
    });
  }

  getUserNotes(id) {
    this.clientService
      .getClientNote(id)
      .pipe(take(1))
      .subscribe((response) => {
        this.notes = response;
      });
  }

  createClientNote() {
    this.clientService
      .createClientNote(this.clientId, this.note)
      .pipe()
      .subscribe((res: any) => {
        if (res.status === 'success') {
          this.getUserNotes(this.clientId);
        }
      });
  }

  toggleOpen(note?) {
    this.selectedNote = note;
    this.isRemoveOpen = !this.isRemoveOpen;
  }

  toggleOpenNote(note?) {
    this.note = note ? note.note : '';
    this.selectedNote = note;
    this.isOpen = !this.isOpen;
  }

  updateNote() {
    this.clientService
      .updateClientNote(this.selectedNote.id, this.note)
      .pipe()
      .subscribe((res: any) => {
        if (res.status === 'success') {
          this.getUserNotes(this.clientId);
        }
      });
  }

  removeNote() {
    this.clientService
      .removeNote(this.selectedNote.id)
      .pipe()
      .subscribe((res: any) => {
        if (res.status === 'success') {
          this.getUserNotes(this.clientId);
        }
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
