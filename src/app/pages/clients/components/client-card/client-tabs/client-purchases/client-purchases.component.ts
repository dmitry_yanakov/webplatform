import { Component, OnInit, OnDestroy } from '@angular/core';
import { takeUntil, delay } from 'rxjs/operators';
import { ClientService } from '@core/services/client.service';
import { CommonDataService } from '@core/services/common-data.service';
import { Subject } from 'rxjs';
import * as moment from 'moment';

@Component({
  selector: 'app-client-purchases',
  templateUrl: './client-purchases.component.html',
  styleUrls: ['./client-purchases.component.scss'],
})
export class ClientPurchasesComponent implements OnInit, OnDestroy {
  transactions;
  currency_symbol: string;
  moment = moment;
  unsubscribe: Subject<any> = new Subject<any>();
  constructor(private clientService: ClientService, private commonDataService: CommonDataService) {}

  ngOnInit() {
    this.currency_symbol = this.commonDataService.currentCompany.currency_symbol;
    this.clientService.transactions$.pipe(takeUntil(this.unsubscribe)).subscribe((data: any) => {
      this.transactions = data;
    });
  }

  search(value: string) {
    delay(1000);
    console.log(value);
  }

  public ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
