import { Component, OnInit, OnDestroy } from '@angular/core';
import * as moment from 'moment';
import { CommonDataService } from '@core/services/common-data.service';
import { ClientService } from '@core/services/client.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ApiService } from '@core/services/api.service';

@Component({
  selector: 'app-client-sessions',
  templateUrl: './client-sessions.component.html',
  styleUrls: ['./client-sessions.component.scss'],
})
export class ClientSessionsComponent implements OnInit, OnDestroy {
  booking;
  upcoming_appointments;
  past_appointments;
  canceled_appointments;
  currency_symbol: string;
  moment = moment;
  unsubscribe: Subject<any> = new Subject<any>();
  constructor(private clientService: ClientService, private commonDataService: CommonDataService, public apiService: ApiService) {}

  ngOnInit() {
    this.currency_symbol = this.commonDataService.currentCompany.currency_symbol;
    this.clientService.booking$.pipe(takeUntil(this.unsubscribe)).subscribe((data: any) => {
      this.booking = data;
      this.sortData();
    });
  }

  sortData() {
    this.upcoming_appointments = this.booking
      .filter((i) => i.status !== 4 && i.status !== 5)
      .sort((a, b) => moment(b.from_date).diff(a.from_date));

    this.past_appointments = this.booking.filter((i) => i.status === 4).sort((a, b) => moment(b.from_date).diff(a.from_date));

    this.canceled_appointments = this.booking.filter((i) => i.status === 5).sort((a, b) => moment(b.from_date).diff(a.from_date));
  }

  public ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
