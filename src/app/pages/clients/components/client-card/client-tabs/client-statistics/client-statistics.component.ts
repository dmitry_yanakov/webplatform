import { Component, OnInit, OnDestroy } from '@angular/core';
import { ClientService } from '@core/services/client.service';
import { take, takeUntil } from 'rxjs/operators';
import { CommonDataService } from '@core/services/common-data.service';
import * as moment from 'moment';
import { BehaviorSubject, Subject } from 'rxjs';

@Component({
  selector: 'app-client-statistics',
  templateUrl: './client-statistics.component.html',
  styleUrls: ['./client-statistics.component.scss'],
})
export class ClientStatisticsComponent implements OnInit, OnDestroy {
  statistics;
  currency_symbol: string;
  moment = moment;
  unsubscribe: Subject<any> = new Subject<any>();
  constructor(private clientService: ClientService, private commonDataService: CommonDataService) {}

  ngOnInit() {
    this.currency_symbol = this.commonDataService.currentCompany.currency_symbol;
    this.clientService.statistics$.pipe(takeUntil(this.unsubscribe)).subscribe((data: any) => {
      this.statistics = data;
    });
  }

  public ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
