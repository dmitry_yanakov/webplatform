import { Component, OnInit, OnDestroy } from '@angular/core';
import * as moment from 'moment';
import { CommonDataService } from '@core/services/common-data.service';
import { ClientService } from '@core/services/client.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-client-tickets',
  templateUrl: './client-tickets.component.html',
  styleUrls: ['./client-tickets.component.scss'],
})
export class ClientTicketsComponent implements OnInit, OnDestroy {
  courses;
  currency_symbol: string;
  moment = moment;
  text;
  unsubscribe: Subject<any> = new Subject<any>();
  constructor(private clientService: ClientService, private commonDataService: CommonDataService) {}

  ngOnInit() {
    this.currency_symbol = this.commonDataService.currentCompany.currency_symbol;
    this.clientService.courses$.pipe(takeUntil(this.unsubscribe)).subscribe((data: any) => {
      this.courses = data;
      console.log(data);
    });
  }

  filteredUnfreezeCourses() {
    return this.courses.filter(
      (i) => moment().diff(moment(i.end_date)) < 0 && i.freeze_to === 0 && i.product_title.toLowerCase().indexOf(this.text)
    );
  }

  filteredFreezeCourses() {
    return this.courses.filter((i) => i.freeze_to !== 0 && i.product_title.toLowerCase().indexOf(this.text));
  }

  filteredDisabledCourses() {
    return this.courses.filter(
      (i) => (moment().diff(moment(i.end_date)) >= 0 || i.count === i.limit) && i.product_title.toLowerCase().indexOf(this.text)
    );
  }

  search(value) {
    console.log(value);
    this.text = value;
  }

  public ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
