import { Component, OnInit, ViewChild, HostListener, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { ClientService } from '@core/services/client.service';
import { CommonDataService, Title } from '@core/services/common-data.service';
import { ApiService } from '@core/services/api.service';
import { takeUntil, take } from 'rxjs/operators';
import { LazyLoadEvent } from 'primeng/api';
import * as moment from 'moment';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.scss'],
})
export class ClientsListComponent implements OnInit, OnDestroy {
  offset = 0;
  count = 7;
  total: number;
  clients = [];
  search = '';
  loading = false;
  tableOptions: any = { sortField: '' };
  currency_symbol: string;
  selectedTransactionId: number;
  moment = moment;
  @ViewChild('dv', { static: false }) dv;
  private unsubscribe: Subject<any> = new Subject();

  constructor(private clientService: ClientService, private commonDataService: CommonDataService, private apiService: ApiService) {}

  @HostListener('window:keyup', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if (this.dv.paginate) {
      if (event.key === 'ArrowLeft' && this.offset - this.count >= 0) {
        this.dv.paginate({ first: this.offset - this.count, rows: this.count });
      } else if (event.key === 'ArrowRight' && this.offset + this.count < this.total) {
        this.dv.paginate({ first: this.offset + this.count, rows: this.count });
      }
    }
    return;
  }

  ngOnInit() {
    this.currency_symbol = this.commonDataService.currentCompany.currency_symbol;
    this.clientService.loading.pipe(takeUntil(this.unsubscribe)).subscribe((loading) => (this.loading = loading));
    this.clientService.search.pipe(takeUntil(this.unsubscribe)).subscribe((search) => (this.search = search));
    this.clientService.clients.pipe(takeUntil(this.unsubscribe)).subscribe(({ data, offset, count, total }) => {
      this.clients = data;
      this.offset = offset;
      this.count = count;
      this.total = total;
    });
    this.commonDataService.changeTitle(Title.clients);
  }

  loadGoodsLazy(event: LazyLoadEvent) {
    this.clientService.loading.next(true);

    this.clientService
      .getClients({ offset: event.first, count: event.rows })
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(() => this.clientService.loading.next(false));
  }

  clearSearchField() {
    this.clientService.loading.next(true);
    this.clientService.search.next('');
    this.clientService
      .getClients({})
      .pipe(take(1))
      .subscribe(() => this.clientService.loading.next(false));
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
