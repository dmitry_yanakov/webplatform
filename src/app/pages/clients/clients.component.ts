import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { ClientService } from '@core/services/client.service';
import { CommonDataService } from '@core/services/common-data.service';
import { tap, take, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss'],
})
export class ClientsComponent implements OnInit, OnDestroy {
  searchText = '';

  unsubscribe: Subject<any> = new Subject<any>();

  constructor(private clientService: ClientService, public cdr: ChangeDetectorRef) {}

  ngOnInit() {
    // Подписываемся на изменения поля поиска и запрашиваем продукты, соответствующие поисковому запросу
    this.clientService.search
      .pipe(
        tap((res) =>
          this.clientService
            .searchClients({ text: res })
            .pipe(take(1))
            .subscribe(() => this.clientService.loading.next(false))
        ),
        takeUntil(this.unsubscribe)
      )
      .subscribe((value) => {
        this.searchText = value;
      });

    this.clientService.search.pipe(takeUntil(this.unsubscribe)).subscribe((product_type) => {
      this.searchText = '';
    });
  }

  search(text) {
    if (this.searchText !== text) {
      this.clientService.search.next(text);
    }
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
