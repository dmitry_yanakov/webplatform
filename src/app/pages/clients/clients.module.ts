import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientsComponent } from './clients.component';
import { CreateClientComponent } from './components/create-client/create-client.component';
import { ClientsListComponent } from './components/clients-list/clients-list.component';
import { ClientTicketsComponent } from './components/client-card/client-tabs/client-tickets/client-tickets.component';
import { ClientPurchasesComponent } from './components/client-card/client-tabs/client-purchases/client-purchases.component';
import { ClientSessionsComponent } from './components/client-card/client-tabs/client-sessions/client-sessions.component';
import { ClientNotesComponent } from './components/client-card/client-tabs/client-notes/client-notes.component';
import { ClientStatisticsComponent } from './components/client-card/client-tabs/client-statistics/client-statistics.component';
import { ClientCardComponent } from './components/client-card/client-card.component';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderModule, CropperModule } from '@modules';
import { RouterModule } from '@angular/router';
import { ScrollPanelModule, SharedModule } from 'src/app/shared';
import { DataViewModule } from 'primeng/dataview';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { DropdownModule } from 'primeng/dropdown';
import { NgSelectModule } from '@ng-select/ng-select';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { InputSwitchModule } from 'primeng/inputswitch';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    HeaderModule,
    RouterModule,
    ScrollPanelModule,
    DataViewModule,
    OverlayPanelModule,
    DropdownModule,
    NgSelectModule,
    InputSwitchModule,
    ReactiveFormsModule,
    SharedModule,
    FormsModule,
    CropperModule,
  ],
  declarations: [
    ClientsComponent,
    CreateClientComponent,
    ClientsListComponent,
    ClientTicketsComponent,
    ClientPurchasesComponent,
    ClientSessionsComponent,
    ClientNotesComponent,
    ClientStatisticsComponent,
    ClientCardComponent,
  ],
})
export class ClientsModule {}
