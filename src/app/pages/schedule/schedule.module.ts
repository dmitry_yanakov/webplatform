import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { ScheduleComponent } from './schedule.component';
import { SessionComponent } from './session/session.component';
import { CreateSessionComponent } from './session/create-session/create-session.component';
import { SessionFormServiceComponent } from './session/components/session-form-service/session-form-service.component';
import { ScrollPanelModule, SharedModule } from '../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { TooltipModule } from 'primeng/tooltip';
import { SessionFormClassComponent } from './session/components/session-form-class/session-form-class.component';

@NgModule({
  declarations: [ScheduleComponent, SessionComponent, CreateSessionComponent, SessionFormServiceComponent, SessionFormClassComponent],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    ScrollPanelModule,
    ReactiveFormsModule,
    NgSelectModule,
    OverlayPanelModule,
    SharedModule,
    TooltipModule,
    FormsModule,
  ],
})
export class ScheduleModule {}
