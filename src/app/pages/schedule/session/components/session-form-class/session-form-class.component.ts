import { Component, OnInit, Input, EventEmitter, OnDestroy, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '@core/services/product.service';
import { Subject } from 'rxjs';
import { distinctUntilChanged, takeUntil, debounceTime, switchMap } from 'rxjs/operators';
import { isEmpty } from 'lodash';
import * as moment from 'moment';
import { ScheduleService } from '@core/services/schedule.service';
import { AvailableEmployeesAndTime } from '@core/interfaces/schedule';
import { CommonDataService } from '@core/services/common-data.service';
import { ClientService } from '@core/services/client.service';
import { ApiService } from '@core/services/api.service';
import { IPhoneCodes } from '../../../../../shared/components/phone-field/phone-field.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-session-form-class',
  templateUrl: './session-form-class.component.html',
  styleUrls: ['./session-form-class.component.scss'],
})
export class SessionFormClassComponent implements OnInit, OnDestroy {
  sessionForm: FormGroup;

  moment = moment;
  session_type: number;
  clients_found_arr: [];
  services = [];
  available_times = [];
  times_meta = {};
  available_employees_meta = {};
  phoneCodes: Array<IPhoneCodes> = [];
  selectedServiceEmployees = [
    {
      employee_id: 0,
      full_name: this.translate.instant('not-assigned'),
    },
  ];
  repeat_start = [
    {
      value: -1,
      label: 'never',
    },
    {
      value: 1,
      label: 'repeat-every-day',
    },
    {
      value: 7,
      label: 'repeat-every-week',
    },
  ];
  repeat_to_arr = [
    {
      value: -1,
      label: 'never',
    },
    {
      value: 2,
      label: 'repeat-end-2',
    },
    {
      value: 10,
      label: 'repeat-end-10',
    },
    {
      value: null,
      label: 'date',
    },
  ];
  is_employees_select_open = false;

  clientTypeahead = new EventEmitter<string>();
  unsubscribe: Subject<any> = new Subject<any>();

  @Input() date: string;
  @Input() employee;

  @Output() formReady: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private clientService: ClientService,
    private productService: ProductService,
    private scheduleService: ScheduleService,
    private commonDataService: CommonDataService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.session_type = this.commonDataService.currentCompany.session;

    this.initForm();

    this.getPhoneCodes();

    this.productService
      .getProducts({ count: 999, product_type: 3 })
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((products_data) => {
        this.services = products_data['products'];
      });

    this.serverSideSearch();

    this.sessionForm.get('from_date').valueChanges.subscribe((date) => {
      const employee_id = this.employee_id ? this.employee_id : '';

      this.getAvailableEmployeesAndTimes({ employee_id, from_date: date });
    });

    this.clientService.clients.subscribe((clients) => {
      console.log(clients);
      this.clients_found_arr = clients['data'];
    });
  }

  initForm() {
    this.sessionForm = this.fb.group({
      product_id: ['', Validators.required],
      employee_id: [0, Validators.required],
      random_employee_id: [0, Validators.required],
      from_date: [moment().format('YYYY-MM-DD'), Validators.required],
      from_time: ['Выберите время', Validators.required],
      is_new_client: false,
      repeat_from: [-1],
      repeat_to: [-1],
      is_formed: [false],
      clients: [[]],
      client_full_name: [''],
      client_first_name: ['', Validators.required],
      phone_code: [
        {
          name: this.commonDataService.userLocation.country,
          code: this.commonDataService.userLocation.code.toLowerCase(),
          phone_code: this.commonDataService.userLocation.phone_code,
        },
      ],
      phone_number: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(23)]],
    });

    this.formReady.emit(this.sessionForm);

    this.repeat_start.forEach((item, idx) => {
      this.translate.get(item.label).subscribe((value) => {
        this.repeat_start[idx].label = value;
      });
    });

    this.repeat_to_arr.forEach((item, idx) => {
      this.translate.get(item.label).subscribe((value) => {
        this.repeat_to_arr[idx].label = value;
      });
    });
  }

  getPhoneCodes() {
    this.apiService
      .getPhoneCodes()
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((codesResp) => {
        this.phoneCodes = codesResp['data'].map(
          (phone): IPhoneCodes => {
            return {
              label: phone.name,
              value: {
                name: this.translate.instant(phone.name),
                code: phone.code.toLowerCase(),
                phone_code: phone.phone_code,
              },
            };
          }
        );
      });
  }

  onServiceSelect(product) {
    const filteredEmployees = product.employees
      ? product.employees
          .filter((employee) => employee.employee_is_service)
          .map((employee) => {
            return { ...employee, full_name: `${employee.last_name} ${employee.first_name}` };
          })
      : [];

    this.selectedServiceEmployees = [...this.selectedServiceEmployees, ...filteredEmployees];

    this.getAvailableEmployeesAndTimes({ product_id: product.id, from_date: this.from_date });

    this.sessionForm.patchValue({
      product_id: product.id,
    });
  }

  onEmployeeSelect() {
    this.getAvailableEmployeesAndTimes({ product_id: this.product_id, employee_id: this.employee_id, from_date: this.from_date });
  }

  addClient(value) {
    this.sessionForm.patchValue({ clients: [...this.clients, value], client_full_name: '' });
  }

  setToday() {
    this.sessionForm.get('from_date').setValue(moment().format('YYYY-MM-DD'));
  }

  setTomorrow() {
    this.sessionForm.get('from_date').setValue(
      moment()
        .add(1, 'days')
        .format('YYYY-MM-DD')
    );
  }

  addNewClient() {
    const client = {
      first_name: this.sessionForm.get('client_first_name').value,
      phone_number: this.sessionForm.get('phone_number').value,
      phone_country: this.sessionForm.get('phone_code').value.code,
      phone_code: this.sessionForm.get('phone_code').value.phone_code,
    };

    this.clientService.createClient(client).subscribe((client_id) => {
      if (client_id) {
        this.sessionForm.patchValue({ clients: [...this.clients, { id: client_id, ...client }] });
      }
    });
  }

  getAvailableEmployeesAndTimes({ product_id = this.product_id, employee_id = '', from_date }) {
    this.scheduleService
      .getEmployeesAvailableTime({ product_id, employee_id, date: from_date })
      .subscribe((res: AvailableEmployeesAndTime) => {
        if (!isEmpty(res.entities.employees)) {
          this.available_times = res.result.filter((time) => {
            return res.entities.times[time].employees.length > 0;
          });
          this.times_meta = res.entities.times;
          this.available_employees_meta = res.entities.employees;
        }
      });
  }

  private serverSideSearch() {
    this.clientTypeahead
      .pipe(
        distinctUntilChanged(),
        debounceTime(100),
        switchMap((term) => {
          if (term !== '') {
            return this.clientService.getClients({ count: 999, search: term });
          } else {
            this.clients_found_arr = [];
          }
        }),
        takeUntil(this.unsubscribe)
      )
      .subscribe((value) => {});
  }

  get product_id() {
    return this.sessionForm.get('product_id').value;
  }
  get employee_id() {
    return this.sessionForm.get('employee_id').value;
  }
  get from_date() {
    return this.sessionForm.get('from_date').value;
  }
  get is_new_client() {
    return this.sessionForm.get('is_new_client').value;
  }

  get clients() {
    return this.sessionForm.get('clients').value;
  }

  get repeat_to() {
    return this.sessionForm.get('repeat_to').value;
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
