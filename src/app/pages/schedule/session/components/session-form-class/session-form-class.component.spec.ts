import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionFormClassComponent } from './session-form-class.component';

describe('SessionFormClassComponent', () => {
  let component: SessionFormClassComponent;
  let fixture: ComponentFixture<SessionFormClassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SessionFormClassComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionFormClassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
