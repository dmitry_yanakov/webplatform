import { Component, OnInit, Input, EventEmitter, OnDestroy, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '@core/services/product.service';
import { Subject } from 'rxjs';
import { distinctUntilChanged, takeUntil, debounceTime, switchMap } from 'rxjs/operators';
import { isEmpty } from 'lodash';
import * as moment from 'moment';
import { ScheduleService } from '@core/services/schedule.service';
import { AvailableEmployeesAndTime } from '@core/interfaces/schedule';
import { CommonDataService } from '@core/services/common-data.service';
import { ClientService } from '@core/services/client.service';
import { ApiService } from '@core/services/api.service';
import { IPhoneCodes } from '../../../../../shared/components/phone-field/phone-field.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-session-form-service',
  templateUrl: './session-form-service.component.html',
  styleUrls: ['./session-form-service.component.scss'],
})
export class SessionFormServiceComponent implements OnInit, OnDestroy {
  sessionForm: FormGroup;

  moment = moment;
  session_type: number;
  clients_found_arr: [];
  services = [];
  available_times = [];
  times_meta = {};
  available_employees_meta = {};
  phoneCodes: Array<IPhoneCodes> = [];
  selectedServiceEmployees = [
    {
      employee_id: 0,
      full_name: 'Любой доступный',
    },
  ];
  is_employees_select_open = false;

  clientTypeahead = new EventEmitter<string>();
  unsubscribe: Subject<any> = new Subject<any>();

  @Input() date: string;
  @Input() employee;

  @Output() formReady: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private clientService: ClientService,
    private productService: ProductService,
    private scheduleService: ScheduleService,
    private commonDataService: CommonDataService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.session_type = this.commonDataService.currentCompany.session;

    this.sessionForm = this.fb.group({
      product_id: ['', Validators.required],
      employee_id: [0, Validators.required],
      random_employee_id: [0, Validators.required],
      from_date: [moment().format('YYYY-MM-DD'), Validators.required],
      from_time: ['Выберите время', Validators.required],
      is_new_client: false,
      clients: [[], Validators.required],
      client_first_name: ['', Validators.required],
      phone_code: [
        {
          name: this.translate.instant(this.commonDataService.userLocation.country),
          code: this.commonDataService.userLocation.code.toLowerCase(),
          phone_code: this.commonDataService.userLocation.phone_code,
        },
      ],
      phone_number: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(23)]],
    });

    this.formReady.emit(this.sessionForm);

    this.getPhoneCodes();

    this.productService
      .getProducts({ count: 999, product_type: 1 })
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((products_data) => {
        this.services = products_data['products'];
      });

    this.serverSideSearch();

    this.sessionForm.get('from_date').valueChanges.subscribe((date) => {
      const employee_id = this.employee_id ? this.employee_id : '';

      this.getAvailableEmployeesAndTimes({ employee_id, from_date: date });
    });

    this.clientService.clients.subscribe((clients) => {
      this.clients_found_arr = clients['data'];
    });
  }

  getPhoneCodes() {
    this.apiService
      .getPhoneCodes()
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((codesResp) => {
        this.phoneCodes = codesResp['data'].map(
          (phone): IPhoneCodes => {
            return {
              label: phone.name,
              value: {
                name: this.translate.instant(phone.name),
                code: phone.code.toLowerCase(),
                phone_code: phone.phone_code,
              },
            };
          }
        );
      });
  }

  onServiceSelect(product) {
    const filteredEmployees = product.employees
      ? product.employees
          .filter((employee) => employee.employee_is_service)
          .map((employee) => {
            return { ...employee, full_name: `${employee.last_name} ${employee.first_name}` };
          })
      : [];

    this.selectedServiceEmployees = [...this.selectedServiceEmployees, ...filteredEmployees];

    this.getAvailableEmployeesAndTimes({ product_id: product.id, from_date: this.from_date });

    this.sessionForm.patchValue({
      product_id: product.id,
    });
  }

  onEmployeeSelect() {
    this.getAvailableEmployeesAndTimes({ product_id: this.product_id, employee_id: this.employee_id, from_date: this.from_date });
  }

  onTimeSelect(time) {
    if (this.session_type === 0 && !this.employee_id) {
      const time_meta = this.times_meta[time];
      const random_employee_id = time_meta.employees[Math.round(Math.random() * (time_meta.employees.length - 1))];

      this.sessionForm.patchValue({ random_employee_id });
    }
  }

  addClient(value) {
    this.sessionForm.patchValue({ clients: [value] });
  }

  getAvailableEmployeesAndTimes({ product_id = this.product_id, employee_id = '', from_date }) {
    this.scheduleService
      .getEmployeesAvailableTime({ product_id, employee_id, date: from_date })
      .subscribe((res: AvailableEmployeesAndTime) => {
        if (!isEmpty(res.entities.employees)) {
          this.available_times = res.result.filter((time) => {
            return res.entities.times[time].employees.length > 0;
          });
          this.times_meta = res.entities.times;
          this.available_employees_meta = res.entities.employees;
        }
      });
  }

  private serverSideSearch() {
    this.clientTypeahead
      .pipe(
        distinctUntilChanged(),
        debounceTime(100),
        switchMap((term) => {
          if (term !== '') {
            return this.clientService.getClients({ count: 999, search: term });
          } else {
            this.clients_found_arr = [];
          }
        }),
        takeUntil(this.unsubscribe)
      )
      .subscribe((value) => {});
  }

  get product_id() {
    return this.sessionForm.get('product_id').value;
  }
  get employee_id() {
    return this.sessionForm.get('employee_id').value;
  }
  get from_date() {
    return this.sessionForm.get('from_date').value;
  }
  get is_new_client() {
    return this.sessionForm.get('is_new_client').value;
  }

  get clients() {
    return this.sessionForm.get('clients').value;
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
