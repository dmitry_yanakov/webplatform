import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionFormServiceComponent } from './session-form-service.component';

describe('SessionFormServiceComponent', () => {
  let component: SessionFormServiceComponent;
  let fixture: ComponentFixture<SessionFormServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SessionFormServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionFormServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
