import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ScheduleService } from '@core/services/schedule.service';
import { mergeMap, takeUntil } from 'rxjs/operators';
import { ClientService } from '@core/services/client.service';
import { CommonDataService } from '@core/services/common-data.service';
import { Subject } from 'rxjs/internal/Subject';
import { NotificationMessageService } from '@core/services/notification-message.service';

@Component({
  selector: 'app-create-service',
  templateUrl: './create-session.component.html',
  styleUrls: ['./create-session.component.scss'],
})
export class CreateSessionComponent implements OnInit, OnDestroy {
  sessionForm: FormGroup;
  session_type_name: string;
  client_id: number;
  booking_id: number;

  unsubscribe: Subject<any> = new Subject<any>();

  constructor(
    private fb: FormBuilder,
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private scheduleService: ScheduleService,
    private clientService: ClientService,
    private commonDataService: CommonDataService,
    private notificationMessageService: NotificationMessageService
  ) {}

  ngOnInit() {
    this.sessionForm = this.fb.group({});
    this.activatedRoute.params.subscribe((params) => {
      this.session_type_name = params['session-type'];
    });
  }

  formInitialized(form) {
    this.sessionForm = form;
  }

  onSubmit() {
    console.log(this.sessionForm.value);

    const booking_data = {
      product_id: this.form.product_id,
      employee_id: this.form.employee_id ? this.form.employee_id : this.form.random_employee_id ? this.form.random_employee_id : 0,
      from_date: `${this.form.from_date} ${this.form.from_time}`,
      repeat_from: this.form.repeat_from ? this.form.repeat_from : '',
      repeat_to: this.form.repeat_to ? this.form.repeat_to : '',
      is_formed: Number(this.form.is_formed),
      comment: this.form.comment,
    };

    if (this.commonDataService.currentCompany.session === 0) {
      this.serviceSubmit(booking_data);
    } else if (this.commonDataService.currentCompany.session === 1) {
      this.classSubmit(booking_data);
    }
  }

  private serviceSubmit(data) {
    if (this.form.is_new_client) {
      this.createClient();
    }

    this.scheduleService
      .createBooking(data)
      .pipe(
        mergeMap((res) => {
          if (res['status'] === 'success') {
            const clients = this.form.clients.map((client) => client.id);

            return this.scheduleService.addBookingClient({
              booking_id: res['data']['booking_id'],
              clients: this.client_id ? this.client_id : clients.join(','),
            });
          }
        }),
        takeUntil(this.unsubscribe)
      )
      .subscribe((res) => {
        console.log(res);
        // TODO: Удалять букинг, если клиент не добавился в букинг и выводить сообщение об ошибке

        if (res['status'] === 'success') {
          this.notificationMessageService.pushAlert({ detail: 'notification-create-event' });
        }
      });
  }

  private classSubmit(data) {
    this.scheduleService
      .createBooking(data)
      .pipe(
        mergeMap((res) => {
          if (res['status'] === 'success' && this.form.clients.length) {
            const clients = this.form.clients.map((client) => client.id);

            return this.scheduleService.addBookingClient({ booking_id: res['data']['booking_id'], clients: clients.join(',') });
          }
        }),
        takeUntil(this.unsubscribe)
      )
      .subscribe((res) => {
        if (res['status'] === 'success') {
          this.goBack();
          this.notificationMessageService.pushAlert({ detail: 'notification-create-event' });
        }
      });
  }

  private createClient() {
    return this.clientService
      .createClient({
        first_name: this.form.client_first_name,
        phone_code: this.form.phone_code.phone_code,
        phone_country: this.form.phone_code.name,
        phone_number: this.form.phone_number,
      })
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((client_id) => (this.client_id = client_id));
  }

  goBack() {
    this.location.back();
  }

  get form() {
    return this.sessionForm.value;
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
