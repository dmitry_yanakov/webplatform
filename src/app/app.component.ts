import { ApiService } from '@core/services/api.service';
import { Component, DoCheck, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { locale, updateLocale } from 'moment';
import { Router } from '@angular/router';
import { Branch, CommonDataService, Company } from '@core/services/common-data.service';
import { NotificationsService } from '@core/services/notifications.service';
import { Toast } from 'primeng/toast';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, DoCheck {
  isReady = false;
  isCompanyReady = false;

  @ViewChild('notificationMessages', { static: false }) notificationMessages: Toast;
  @ViewChild('requestsMessages', { static: false }) requestsMessages: Toast;

  constructor(
    public translate: TranslateService,
    private api: ApiService,
    private commonData: CommonDataService,
    private router: Router,
    private notificationsService: NotificationsService
  ) {}

  ngDoCheck(): void {
    if (this.notificationMessages && this.notificationMessages.messages && this.notificationMessages.messages.length > 3) {
      this.notificationMessages.messages.shift();
    }

    if (this.requestsMessages && this.requestsMessages.messages && this.requestsMessages.messages.length > 3) {
      this.requestsMessages.messages.shift();
    }
  }

  ngOnInit() {
    this.initTranslate();
    this.initUser();
    this.initMomentLocale();
    this.commonData.isCompanyReadySubject$.subscribe((isReady) => (this.isCompanyReady = isReady));
  }

  initUser() {
    const token = localStorage.getItem('token');
    this.api.token = token ? token : null;
    if (this.api.token === null) {
      this.isReady = true;
      return;
    }
    this.getUserLocation();
    this.getUserData();
  }

  initTranslate() {
    this.translate.addLangs(['en', 'ru', 'fr']);
    if (localStorage.getItem('locale')) {
      const browserLang = localStorage.getItem('locale');
      this.translate.use(browserLang.match(/en|ru|fr/) ? browserLang : 'en');
    } else {
      localStorage.setItem('locale', 'en');
      this.translate.setDefaultLang('en');
    }
  }

  initMomentLocale() {
    updateLocale('ru', {
      months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
      weekdaysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
      weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
    });

    updateLocale('fr', {
      months: ['Janvier', 'Février', 'Mars', 'Avril', 'May', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
      weekdaysShort: ['Dim', 'Lu', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
      weekdays: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
    });

    locale(localStorage.getItem('locale') || 'en');
  }

  getUserData() {
    this.api.getSelfUser().subscribe((selfUserResponse) => {
      if (selfUserResponse['status'] === 'success') {
        this.api.user = selfUserResponse['data'];
        this.commonData.user = selfUserResponse['data'];

        this.checkLanguage();
        if (!this.api.user['phone_number']) {
          this.router.navigate(['/more']);
        } else {
          this.getCompanies();
        }
      }
    });
  }

  getUserLocation() {
    this.api.getUserLocation().subscribe((res) => {
      if (res['status']) {
        this.commonData.userLocation = res['data'];
      }
    });
  }

  checkLanguage() {
    const currentLanguage = localStorage.getItem('locale');
    const newLanguage = this.commonData.user.language;

    if (newLanguage !== currentLanguage) {
      this.translate.use(newLanguage);
      localStorage.setItem('locale', newLanguage);
    }
  }

  getCompanies() {
    this.api.getSelfCompany().subscribe((selfCompanyResponse) => {
      if (selfCompanyResponse['status'] === 'success') {
        if (selfCompanyResponse['data'].length) {
          this.commonData.companies = selfCompanyResponse['data'];
          this.setCurrentCompany();
        } else {
          this.router.navigate(['/create-company']);
        }
      } else {
        this.router.navigate(['/create-company']);
      }
    });
  }

  getBranches(company_id) {
    this.api.getCompanyBranch(company_id).subscribe((res) => {
      if (res['status'] === 'success') {
        if (res['data']['branches'].length) {
          this.commonData.branches = res['data']['branches'];
          this.setCurrentBranch();
        } else {
          this.router.navigate(['/create-company']);
        }
      }
    });
  }

  // Если у юзера есть id текущей выюранной компании
  // выбераем из массива компанию с таким же id
  // и выносим в отдельный объект, а если id отсутствует,
  // берем первую из списка и сохраняем её id юзеру
  setCurrentCompany() {
    const companies: Company[] = this.commonData.companies;
    const company_id: number = this.commonData.user.company_id;

    if (company_id) {
      this.commonData.currentCompany = companies.find((company) => company_id === company.id);
      this.getBranches(company_id);
    } else {
      const currentCompany: Company = companies[0];

      this.getBranches(currentCompany['id']);
      this.api.setUser({ company_id: currentCompany['id'] }).subscribe((res) => {
        if (res['status'] === 'success') {
          this.commonData.currentCompany = currentCompany;
        }
      });
    }
  }

  // Если у юзера есть id текущей выюранной локации
  // выбераем из массива локацию с таким же id
  // и выносим в отдельный объект, а если id отсутствует,
  // берем первую из списка и сохраняем её id юзеру
  setCurrentBranch() {
    const branches: Branch[] = this.commonData.branches;
    const branch_id: number = this.commonData.user.branch_id;

    if (branch_id) {
      this.commonData.currentBranch = branches.find((branch) => branch_id === branch.id);
      this.isReady = true;
    } else if (branches.length) {
      const currentBranch: Branch = branches[0];

      this.api.setUser({ branch_id: currentBranch['id'] }).subscribe((res) => {
        if (res['status'] === 'success') {
          this.commonData.currentBranch = currentBranch;
          this.isReady = true;
        }
      });
    } else {
      this.isReady = true;
    }
    this.commonData.isCompanyReadySubject$.next(!!this.commonData.currentCompany.is_ready);

    this.getNotifications();
  }

  getNotifications() {
    this.notificationsService.getNotifications().subscribe((res) => {
      if (res['status'] === 'success') {
        this.getNotifications();
      } else {
        this.getNotifications();
      }
    });
  }
}
