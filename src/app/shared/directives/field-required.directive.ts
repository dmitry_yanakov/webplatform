import { Directive, Renderer2, ElementRef } from '@angular/core';

// Директива для тега label обязательного поля формы.
// Добавляет * красного цвета в конец строки посредством назначения класса 'field-required'.
@Directive({
  selector: '[appFieldRequired]',
})
export class FieldRequiredDirective {
  constructor(private elementRef: ElementRef, private renderer: Renderer2) {
    this.renderer.addClass(this.elementRef.nativeElement, 'field-required');
  }
}
