import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NumericDirective, DisableControlDirective } from '@directives';
import { DropdownModule } from 'primeng/dropdown';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { DialogModule } from 'primeng/dialog';
import { SidebarModule } from 'primeng/sidebar';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { MegaMenuModule } from 'primeng/megamenu';

// Custom component
import { LanguageSelectorComponent } from './components/language-selector/language-selector.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { PhoneFieldComponent } from './components/phone-field/phone-field.component';
import { DatePickerComponent } from './components/date-picker/date-picker.component';
import { ModalComponent } from './components/modal/modal.component';
import { QrCodeGeneratorComponent } from './components/qr-code-generator/qr-code-generator.component';
import { FieldRequiredDirective } from './directives/field-required.directive';
import { TimePickerComponent } from './components/time-picker/time-picker.component';
import { NgFooterDirective } from './components/date-picker/ng-templates.directive';
import { WorkTimeSelectComponent } from './components/work-time-select/work-time-select.component';

@NgModule({
  declarations: [
    NumericDirective,
    DisableControlDirective,
    LanguageSelectorComponent,
    SidebarComponent,
    PhoneFieldComponent,
    DatePickerComponent,
    ModalComponent,
    QrCodeGeneratorComponent,
    FieldRequiredDirective,
    TimePickerComponent,
    NgFooterDirective,
    WorkTimeSelectComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    DropdownModule,
    OverlayPanelModule,
    DialogModule,
    SidebarModule,
    RouterModule,
    TranslateModule,
    MegaMenuModule,
  ],
  exports: [
    NumericDirective,
    DisableControlDirective,
    LanguageSelectorComponent,
    SidebarComponent,
    PhoneFieldComponent,
    DatePickerComponent,
    ModalComponent,
    QrCodeGeneratorComponent,
    FieldRequiredDirective,
    TimePickerComponent,
    NgFooterDirective,
    WorkTimeSelectComponent,
  ],
  providers: [],
})
export class SharedModule {}
