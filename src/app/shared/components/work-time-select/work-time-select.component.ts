import { Component, OnInit, forwardRef, Input, ViewChild, ElementRef, HostListener } from '@angular/core';
import * as moment from 'moment';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

@Component({
  selector: 'app-work-time-select',
  templateUrl: './work-time-select.component.html',
  styleUrls: ['./work-time-select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => WorkTimeSelectComponent),
      multi: true,
    },
  ],
})
export class WorkTimeSelectComponent implements OnInit, ControlValueAccessor {
  hours = [];
  isOpen = false;
  is_collapsed = false;
  _selectedTime;

  onChange: (value: string) => void;
  onTouched: () => void;

  @Input() max = 0;

  @ViewChild('timeSelect', { static: true }) timeSelect: ElementRef;
  @ViewChild('timeSelectInput', { static: false }) input: ElementRef;
  @ViewChild('timeSelectList', { static: false }) list: ElementRef;

  constructor() {}

  ngOnInit() {
    this.generateTimesArray();
  }

  @HostListener('window:click', ['$event'])
  hideModal(event) {
    if (this.isOpen && !this.timeSelect.nativeElement.contains(event.target)) {
      this.isOpen = false;
      this.is_collapsed = false;
    }
  }

  generateTimesArray() {
    for (let hour = 0; hour < 24; hour++) {
      this.hours.push(moment({ hour }).format('HH:mm'));
      this.hours.push(
        moment({
          hour,
          minute: 30,
        }).format('HH:mm')
      );
    }
  }

  setMaxValue() {
    const start = this.hours.findIndex((item) => item === this.max);
    this.hours.splice(start);
  }

  public toggle() {
    this.isOpen = !this.isOpen;
    this.is_collapsed = !this.is_collapsed;
  }

  get selectedTime() {
    if (!this._selectedTime) {
      return '';
    }
    return this._selectedTime;
  }

  setTime(value) {
    if (!value) {
      this._selectedTime = '00:00';
    } else {
      this._selectedTime = value;
    }
    this.onChange(this._selectedTime);
  }

  // вызовет форму если значение изменилось извне
  writeValue(time): void {
    if (!time) {
      return;
    }
    this._selectedTime = time;
  }

  // сохраняем обратный вызов для изменений
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  // сохраняем обратный вызов для "касаний"
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
}
