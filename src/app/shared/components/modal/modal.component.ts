import { Component, HostListener, Input, OnInit, Output, ViewChild, EventEmitter, ElementRef } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  @Input() isOpen = false;
  @Input() overlayClass = null;
  @Input() overlayStyle = null;
  @Input() contentClass = null;
  @Input() contentStyle = null;

  @Output() hide: EventEmitter<any> = new EventEmitter();

  @ViewChild('content', { static: false }) content: ElementRef;

  constructor() {}

  ngOnInit() {}

  @HostListener('click', ['$event'])
  _hideModal(event) {
    if (this.isOpen && !this.content.nativeElement.contains(event.target)) {
      this.hideModal();
      this.hide.emit();
    }
  }

  public toggle() {
    this.isOpen = !this.isOpen;
  }

  public hideModal() {
    this.isOpen = false;
  }

  public showModal() {
    this.isOpen = true;
  }
}
