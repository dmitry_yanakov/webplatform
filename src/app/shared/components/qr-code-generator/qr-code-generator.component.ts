import { Component, OnInit, ViewChild, Input, ElementRef } from '@angular/core';
import QRCode from 'qrcode';

interface Excavation {
  x: number;
  y: number;
  w: number;
  h: number;
}

interface QRSettings {
  value?: string;
  size?: number;
  level?: string;
  bgColor?: string;
  fgColor?: string;
  style?: Object;
  includeMargin?: boolean;
  imageSettings?: {
    src?: string;
    height?: number;
    width?: number;
    excavate?: boolean;
    x?: number;
    y?: number;
  };
}

const SUPPORTS_PATH2D = (function() {
  try {
    new Path2D().addPath(new Path2D());
  } catch (e) {
    return false;
  }
  return true;
})();

@Component({
  selector: 'app-qr-code-generator',
  templateUrl: './qr-code-generator.component.html',
  styleUrls: ['./qr-code-generator.component.scss'],
})
export class QrCodeGeneratorComponent implements OnInit {
  private MARGIN_SIZE = 4;
  private DEFAULT_IMG_SCALE = 0.1;
  private _settings: QRSettings = {
    value: '',
    size: 500,
    level: 'Q',
    includeMargin: false,
    bgColor: '#fff',
    fgColor: '#186fda',
    imageSettings: {
      src: '',
      height: 95,
      width: 95,
      excavate: true,
    },
  };

  @ViewChild('canvas', { static: false }) canvas: ElementRef;
  @ViewChild('img', { static: false }) img: ElementRef;
  @Input() settings: QRSettings;

  constructor() {}

  ngOnInit() {
    this._settings = { ...this._settings, ...this.settings };
  }

  public getQRCode(): string {
    return this.canvas.nativeElement.toDataURL();
  }

  public drawImg() {
    const qrcode = QRCode.create(this._settings.value, {
      errorCorrectionLevel: this._settings.level,
      width: this._settings.size,
      height: this._settings.size,
      margin: this._settings.includeMargin ? 1 : 0,
      color: {
        dark: this._settings.fgColor,
        light: this._settings.bgColor,
      },
    });

    const ctx = this.canvas.nativeElement.getContext('2d');
    const margin = this._settings.includeMargin ? this.MARGIN_SIZE : 0;
    let cells = [];

    for (let i = 0; i < Math.ceil(qrcode.modules.data.length / qrcode.modules.size); i++) {
      cells[i] = qrcode.modules.data.slice(i * qrcode.modules.size, i * qrcode.modules.size + qrcode.modules.size);
    }

    const numCells = cells.length + margin * 2;
    const calculatedImageSettings = this.getImageSettings(this._settings, cells);

    if (this._settings.imageSettings != null && calculatedImageSettings != null) {
      if (calculatedImageSettings.excavation != null) {
        cells = this.excavateModules(cells, calculatedImageSettings.excavation);
      }
    }

    const pixelRatio = window.devicePixelRatio || 1;
    this.canvas.nativeElement.height = this.canvas.nativeElement.width = this._settings.size * pixelRatio;
    const scale = (this._settings.size / numCells) * pixelRatio;
    ctx.scale(scale, scale);

    ctx.fillStyle = this._settings.bgColor;
    ctx.fillRect(0, 0, numCells, numCells);
    ctx.fillStyle = this._settings.fgColor;

    if (SUPPORTS_PATH2D) {
      ctx.fill(new Path2D(this.generatePath(cells, margin)));
    } else {
      cells.forEach(function(row, rdx) {
        row.forEach(function(cell, cdx) {
          if (cell) {
            ctx.fillRect(cdx + margin, rdx + margin, 1, 1);
          }
        });
      });
    }

    if (calculatedImageSettings != null) {
      ctx.drawImage(
        this.img.nativeElement,
        calculatedImageSettings.x + margin,
        calculatedImageSettings.y + margin,
        calculatedImageSettings.w,
        calculatedImageSettings.h
      );
    }
  }

  private excavateModules(modules: Array<Array<number>>, excavation: Excavation): Array<Array<number | boolean>> {
    return modules.slice().map((row, y) => {
      if (y < excavation.y || y >= excavation.y + excavation.h) {
        return row;
      }
      return row.map((cell, x) => {
        if (x < excavation.x || x >= excavation.x + excavation.w) {
          return cell;
        }
        return false;
      });
    });
  }

  private getImageSettings(
    settings: QRSettings,
    cells: Array<Array<number>>
  ): null | { x: number; y: number; h: number; w: number; excavation: Excavation } {
    const { imageSettings, size, includeMargin } = settings;
    if (imageSettings == null) {
      return null;
    }
    const margin = includeMargin ? this.MARGIN_SIZE : 0;
    const numCells = cells.length + margin * 2;
    const defaultSize = Math.floor(size * this.DEFAULT_IMG_SCALE);
    const scale = numCells / size;

    const w = (imageSettings.width || defaultSize) * scale;
    const h = (imageSettings.height || defaultSize) * scale;
    const x = imageSettings.x == null ? cells.length / 2 - w / 2 : imageSettings.x * scale;
    const y = imageSettings.y == null ? cells.length / 2 - h / 2 : imageSettings.y * scale;

    let excavation = null;
    if (imageSettings.excavate) {
      const floorX = Math.floor(x);
      const floorY = Math.floor(y);
      const ceilW = Math.ceil(w + x - floorX);
      const ceilH = Math.ceil(h + y - floorY);
      excavation = { x: floorX, y: floorY, w: ceilW, h: ceilH };
    }

    return { x, y, h, w, excavation };
  }

  private generatePath(modules: Array<Array<any>>, margin: number = 0): string {
    const ops = [];
    modules.forEach(function(row, y) {
      let start = null;
      row.forEach(function(cell, x) {
        if (!cell && start !== null) {
          ops.push(`M${start + margin} ${y + margin}h${x - start}v1H${start + margin}z`);
          start = null;
          return;
        }

        if (x === row.length - 1) {
          if (!cell) {
            return;
          }
          if (start === null) {
            ops.push(`M${x + margin},${y + margin} h1v1H${x + margin}z`);
          } else {
            ops.push(`M${start + margin},${y + margin} h${x + 1 - start}v1H${start + margin}z`);
          }
          return;
        }

        if (cell && start === null) {
          start = x;
        }
      });
    });
    return ops.join('');
  }
}
