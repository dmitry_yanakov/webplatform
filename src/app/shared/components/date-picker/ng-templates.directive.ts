import { Directive, TemplateRef } from '@angular/core';

@Directive({ selector: '[ng-footer-tmp]' })
export class NgFooterDirective {
  constructor(public template: TemplateRef<any>) {}
}
