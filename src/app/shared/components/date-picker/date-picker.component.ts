import {
  Component,
  OnInit,
  Input,
  DoCheck,
  forwardRef,
  ViewEncapsulation,
  EventEmitter,
  Output,
  ContentChild,
  TemplateRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import * as moment from 'moment';
import { NgFooterDirective } from './ng-templates.directive';

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DatePickerComponent),
      multi: true,
    },
  ],
})
export class DatePickerComponent implements ControlValueAccessor, OnInit, DoCheck {
  moment = moment;
  yearViewArr: Array<moment.Moment> = [];
  monthViewArr: Array<moment.Moment> = [];
  daysViewArr: Array<moment.Moment> = [];
  date: moment.Moment = moment();
  private prevView: 'year-range' | 'year' | 'month' | 'day';
  @Input() view: 'year-range' | 'year' | 'month' | 'day' = 'year';
  @Output() toggle: EventEmitter<any> = new EventEmitter();
  onChange: (value: string) => void;
  onTouched: () => void;

  selectedYear: number = moment().year();
  selectedMonth: number = moment().month();
  selectedDay: number = moment().date();

  @ContentChild(NgFooterDirective, { read: TemplateRef, static: false }) footerTemplate: TemplateRef<any>;

  constructor() {}

  get selectedDate() {
    return moment()
      .year(this.selectedYear)
      .month(this.selectedMonth)
      .date(this.selectedDay);
  }

  set selectedDate(date: moment.Moment) {
    this.selectedYear = moment(date).year();
    this.selectedMonth = moment(date).month();
    this.selectedDay = moment(date).date();
  }

  ngDoCheck() {
    if (this.view !== this.prevView) {
      this.prevView = this.view;

      switch (this.view) {
        case 'year-range':
          break;

        case 'year':
          this.yearViewArr = this.initYearView(this.selectedDate);
          break;

        case 'month':
          this.monthViewArr = this.initMonthView(this.selectedDate);
          break;

        case 'day':
          this.daysViewArr = this.initDayView(this.selectedDate);
          break;

        default:
          return;
      }
    }
  }

  ngOnInit() {}

  initYearView(date): Array<moment.Moment> {
    const offset = date.year() % 21;
    const firstMonth = moment(date)
      .subtract(offset, 'y')
      .startOf('year');

    return Array.apply(null, { length: 21 }).map((i, idx) => moment(firstMonth).add(idx, 'year'));
  }

  initMonthView(date): Array<moment.Moment> {
    const firstMonth = moment(date).startOf('year');

    return Array.apply(null, { length: 12 }).map((i, idx) => moment(firstMonth).add(idx, 'month'));
  }

  initDayView(date): Array<moment.Moment> {
    const firstDay = moment(date).startOf('month');

    const days = Array.apply(null, { length: date.daysInMonth() }).map((i, idx) => moment(firstDay).add(idx, 'day'));

    for (let i = 0; i < firstDay.weekday(); i++) {
      days.unshift(null);
    }

    return days;
  }

  changeView() {
    this.prevView = this.view;

    switch (this.view) {
      case 'month':
        this.view = 'year';
        break;

      case 'day':
        this.view = 'month';
        break;

      default:
        return;
    }
  }

  prev() {
    switch (this.view) {
      case 'year':
        this.yearViewArr = this.initYearView(this.date.subtract(21, 'y'));
        break;

      case 'month':
        this.monthViewArr = this.initMonthView(this.date.subtract(1, 'y'));
        break;

      case 'day':
        this.daysViewArr = this.initDayView(this.date.subtract(1, 'M'));
        break;

      default:
        return;
    }
  }

  next() {
    switch (this.view) {
      case 'year':
        this.yearViewArr = this.initYearView(this.date.add(21, 'y'));
        break;

      case 'month':
        this.monthViewArr = this.initMonthView(this.date.add(1, 'y'));
        break;

      case 'day':
        this.daysViewArr = this.initDayView(this.date.add(1, 'M'));
        break;

      default:
        return;
    }
  }

  select(date: moment.Moment) {
    if (!date) {
      return;
    }

    this.prevView = this.view;

    switch (this.view) {
      case 'year':
        this.selectedYear = moment(date).year();
        this.date = date;
        this.view = 'month';
        break;

      case 'month':
        this.selectedYear = moment(date).year();
        this.selectedMonth = moment(date).month();
        this.date = date;
        this.view = 'day';
        break;

      case 'day':
        this.selectedYear = moment(date).year();
        this.selectedMonth = moment(date).month();
        this.selectedDay = moment(date).date();

        this.toggle.emit();
        console.log(2);

        if (this.onChange) {
          const value = this.selectedDate.format('YYYY-MM-DD');
          this.onChange(value);
        }

        break;

      default:
        return;
    }
  }

  dateCheck(date: moment.Moment) {
    if (!date) {
      return false;
    }

    switch (this.view) {
      case 'year':
        return moment(date).format('YYYY') === moment(this.selectedDate).format('YYYY');

      case 'month':
        return moment(date).format('MM-YYYY') === moment(this.selectedDate).format('MM-YYYY');

      case 'day':
        return moment(date).format('L') === moment(this.selectedDate).format('L');

      default:
        return;
    }
  }

  // вызовет форма если значение изменилось извне
  writeValue(date): void {
    if (!date) {
      return;
    }
    this.selectedDate = date;
  }

  // сохраняем обратный вызов для изменений
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  // сохраняем обратный вызов для "касаний"
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
}
