import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, Input } from '@angular/core';
import { countries } from './languages';

@Component({
  selector: 'app-language-selector',
  templateUrl: './language-selector.component.html',
  styleUrls: ['./language-selector.component.scss'],
})
export class LanguageSelectorComponent implements OnInit {
  @Input() mode = 'default';
  @Input() customClass;
  countries = countries;
  selectedLanguage: any;

  constructor(public translate: TranslateService) {}

  ngOnInit() {
    this.selectedLanguage = this.translate.currentLang;
  }
  // TODO: При смене языка добавить смену текущей locale для moment.js
  useLanguage(language: string) {
    localStorage.setItem('locale', language);
    this.translate.use(language);
    this.translate.currentLang = language;
  }
}
