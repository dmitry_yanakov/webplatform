export const countries = [
    { 'label': 'English', 'img': 'assets/flags/226-united-states.svg', 'value': 'en' },
    { 'label': 'France', 'img': 'assets/flags/195-france.svg', 'value': 'fr' },
    { 'label': 'Russia', 'img': 'assets/flags/248-russia.svg', 'value': 'ru' }
];
