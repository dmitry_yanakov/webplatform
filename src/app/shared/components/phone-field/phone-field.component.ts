import { Component, Input, OnInit, Output, EventEmitter, forwardRef, ViewEncapsulation, HostListener } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

export interface ISelectedPhoneCode {
  name: string;
  code: string;
  phone_code: number;
}

export interface IPhoneCodes {
  label: string;
  value: {
    name: string;
    code: string;
    phone_code: number;
  };
}

@Component({
  selector: 'app-phone-field',
  templateUrl: './phone-field.component.html',
  styleUrls: ['./phone-field.component.scss'],
  // tslint:disable-next-line
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PhoneFieldComponent),
      multi: true,
    },
  ],
})
export class PhoneFieldComponent implements ControlValueAccessor, OnInit {
  @Input() defaultValue: ISelectedPhoneCode;
  @Input() options: Array<IPhoneCodes>;
  @Input() appendTo;
  selectedPhoneCode: ISelectedPhoneCode;
  isOpen = false;
  onChange: (value: ISelectedPhoneCode) => void;
  onTouched: () => void;

  constructor() {}

  ngOnInit() {}

  rotateIcon() {
    this.isOpen = !this.isOpen;
  }

  // вызовет форма если значение изменилось извне
  writeValue(obj: any): void {
    this.selectedPhoneCode = obj;
    console.log(obj);
  }

  // сохраняем обратный вызов для изменений
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  // сохраняем обратный вызов для "касаний"
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
}
