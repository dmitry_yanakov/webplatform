import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrollPanelComponent } from './scroll-panel.component';



@NgModule({
  declarations: [ScrollPanelComponent],
  imports: [
    CommonModule,
  ],
  exports: [ScrollPanelComponent]
})
export class ScrollPanelModule { }
