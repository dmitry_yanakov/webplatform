import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GoogleLoginComponent } from './google-login/google-login.component';
import { FacebookLoginComponent } from './facebook-login/facebook-login.component';
import { TranslateModule } from '@ngx-translate/core';
import { LoginBtnComponent } from './login-btn/login-btn.component';

@NgModule({
  declarations: [
      GoogleLoginComponent,
      FacebookLoginComponent,
      LoginBtnComponent
  ],
  imports: [
    CommonModule,
    TranslateModule
  ],
  exports: [
    GoogleLoginComponent,
    FacebookLoginComponent
  ],
  providers: []
})
export class ButtonModule {}
