import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import { NgxFileDropModule } from 'ngx-file-drop';
import { DialogModule } from 'primeng/dialog';
import { SliderModule } from 'primeng/slider';
import { FormsModule } from '@angular/forms';

import { AvatarCropperComponent } from './avatar-cropper/avatar-cropper.component';
import { PhotoCropperComponent } from './photo-cropper/photo-cropper.component';
import {SharedModule} from '../../shared.module';



@NgModule({
  declarations: [
    AvatarCropperComponent,
    PhotoCropperComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    NgxFileDropModule,
    DialogModule,
    SliderModule,
    FormsModule,
    SharedModule,
  ],
  exports: [
    AvatarCropperComponent,
    PhotoCropperComponent,
  ]
})
export class CropperModule { }
