import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  AfterViewInit,
  ViewEncapsulation,
  forwardRef,
  ChangeDetectorRef,
  Input
} from '@angular/core';
import Cropper from 'cropperjs';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Subject } from 'rxjs';
import {FileSystemFileEntry, NgxFileDropEntry} from 'ngx-file-drop';

@Component({
  selector: 'app-avatar-cropper',
  templateUrl: './avatar-cropper.component.html',
  styleUrls: ['./avatar-cropper.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => AvatarCropperComponent),
    multi: true
  }],
})
export class AvatarCropperComponent implements ControlValueAccessor, OnInit, AfterViewInit {
  @ViewChild('image', { static: false })
  imageElement: ElementRef;
  imageSource = new Subject<any>();
  imageDestination = '';
  private cropper: Cropper;
  zoom;

  isReady = false;
  isModalOpen = false;

  public cropperConfig = {
    width: 500,
    height: 500,
    minWidth: 250,
    minHeight: 250,
    minContainerWidth: 250,
    minContainerHeight: 250,
    minCanvasWidth: 250,
    minCanvasHeight: 250,
    maxWidth: 4000,
    maxHeight: 4000,
    fillColor: '#fff',
    imageSmoothingEnabled: false,
    imageSmoothingQuality: 'high',
    viewMode: 3,
    dragMode: 'move',
    zoomable: true,
    scalable: true,
    guides: false,
    center: false,
    cropBoxMovable: false,
    cropBoxResizable: false,
    background: false,
    autoCropArea: 1,
    wheelZoomRatio: 0.1,
    zoom: (value) => {
      this.zoom = value.detail.ratio * 10;
    },
  };

  onChange: (value) => void;
  onTouched: () => void;

  constructor(private cd: ChangeDetectorRef) {
      this.imageDestination = '';
  }

  ngAfterViewInit() {
    this.cropper = new Cropper(
      this.imageElement.nativeElement,
      // @ts-ignore
      this.cropperConfig
    );
  }
  ngOnInit() {
    this.imageSource.subscribe(img => {
      if (img) {
        this.cropper.replace(img);
        this.cropper.setCropBoxData({left: 0, top: 0, width: 250, height: 250});
        this.cropper.crop();

        this.isReady = true;
      }
    });
  }

  onFileChange(event) {
    if (event.target.files && event.target.files.length) {
      const reader = new FileReader();
      const [file] = event.target.files;

      reader.readAsDataURL(file);

      reader.onload = () => {
        this.imageSource.next(reader.result);
      };
    }
  }

  getRoundedCanvas(sourceCanvas) {
    const canvas = document.createElement('canvas');
    const context = canvas.getContext('2d');
    const width = sourceCanvas.width;
    const height = sourceCanvas.height;

    canvas.width = width;
    canvas.height = height;
    context.imageSmoothingEnabled = true;
    context.drawImage(sourceCanvas, 0, 0, width, height);
    context.globalCompositeOperation = 'destination-in';
    context.beginPath();
    context.arc(width / 2, height / 2, Math.min(width, height) / 2, 0, 2 * Math.PI, true);
    context.fill();

    return canvas;
  }

  toggleOpen() {
    this.isModalOpen = !this.isModalOpen;
  }

  onSubmit() {
    const croppedCanvas = this.cropper.getCroppedCanvas();
    const roundedCanvas = this.getRoundedCanvas(croppedCanvas);
    this.imageDestination = roundedCanvas.toDataURL();
    this.onChange(this.imageDestination);
    this.toggleOpen();
    console.log(this.imageDestination);
  }

  public dropped(files: NgxFileDropEntry[]) {
    const _files = files;

    for (const droppedFile of _files) {
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          const reader = new FileReader();

          reader.readAsDataURL(file);

          reader.onload = () => {
            this.imageSource.next(reader.result);
          };
          return;
        });
      }
    }
  }

  resize(value) {
    this.zoom = value / 10;
    this.cropper.zoomTo(this.zoom, {x: 125, y: 125});
  }

  writeValue(avatar): void {
    this.imageDestination = avatar;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
}
