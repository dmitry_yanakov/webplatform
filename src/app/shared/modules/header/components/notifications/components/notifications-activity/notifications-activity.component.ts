import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import * as moment from 'moment';
import { BehaviorSubject, Subject } from 'rxjs';
import { NotificationsService } from '@core/services/notifications.service';

@Component({
  selector: 'app-notifications-activity',
  templateUrl: './notifications-activity.component.html',
  styleUrls: ['./notifications-activity.component.scss'],
})
export class NotificationsActivityComponent implements OnInit, OnDestroy {
  moment = moment;
  all_notifications = [];
  notifications_limit: BehaviorSubject<number> = new BehaviorSubject<number>(6);
  _today_notifications: BehaviorSubject<any> = new BehaviorSubject<any>({ date: 'today', items: [] });
  _yesterday_notifications: BehaviorSubject<any> = new BehaviorSubject<any>({ date: 'yesterday', items: [] });
  _other_notifications: BehaviorSubject<any> = new BehaviorSubject<any>([]);

  constructor(private notificationsService: NotificationsService) {}

  ngOnInit() {
    this.notificationsService.notifications.pipe().subscribe((notifications) => {
      this.all_notifications = [];
      notifications.forEach((notification) => {
        if (notification.type !== 'booking_request') {
          this.all_notifications.push(notification);
        }
      });

      this.all_notifications.sort((a, b) => b.id - a.id);
      this.filterNotifications(this.notifications_limit.getValue());
    });

    this.notifications_limit.subscribe((limit) => {
      this.filterNotifications(limit);
    });
  }

  filterNotifications(limit) {
    const today_notifications = { date: 'today', items: [] };
    const yesterday_notifications = { date: 'yesterday', items: [] };
    const other_notifications = [];

    this.all_notifications.slice(0, limit).forEach((notification) => {
      if (
        moment(notification.date)
          .startOf('days')
          .diff(moment().startOf('days'), 'days') === 0
      ) {
        today_notifications.items.push(notification);
      } else if (
        moment(notification.date)
          .startOf('days')
          .diff(moment().startOf('days'), 'days') === -1
      ) {
        yesterday_notifications.items.push(notification);
      } else if (
        moment(notification.date)
          .startOf('days')
          .diff(moment().startOf('days'), 'days') < -1
      ) {
        const other_notification = other_notifications.find(
          (i) => moment(i.date, 'YYYY-MM-DD').diff(moment(notification.date, 'YYYY-MM-DD'), 'days') === 0
        );

        if (other_notification) {
          other_notification.items.push(notification);
        } else {
          const _other_notifications = { date: moment(notification.date), items: [] };
          _other_notifications.items.push(notification);
          other_notifications.push(_other_notifications);
        }
      }
    });

    this._today_notifications.next(today_notifications);
    this._yesterday_notifications.next(yesterday_notifications);
    this._other_notifications.next(other_notifications);
  }

  getMoreNotifications() {
    const notification_count = this.all_notifications.length;
    const notification_limit = this.notifications_limit.getValue();

    if (notification_count === notification_limit) {
      return;
    } else if (notification_limit <= notification_count) {
      this.notifications_limit.next(notification_limit + 6);
    }
  }

  get today_notifications() {
    return this._today_notifications.getValue();
  }

  get yesterday_notifications() {
    return this._yesterday_notifications.getValue();
  }

  get other_notifications() {
    return this._other_notifications.getValue();
  }

  ngOnDestroy() {
    this.all_notifications = [];
  }
}
