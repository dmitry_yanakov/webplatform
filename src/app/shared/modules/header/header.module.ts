import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderMenuComponent } from './components/header-menu/header-menu.component';
import { HeaderComponent } from './header.component';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../shared.module';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { NotificationsActivityComponent } from './components/notifications/components/notifications-activity/notifications-activity.component';
import { NotificationsRequestsComponent } from './components/notifications/components/notifications-requests/notifications-requests.component';
import { ScrollPanelModule } from '../../components/scroll-panel/scroll-panel.module';

@NgModule({
  declarations: [
    HeaderComponent,
    HeaderMenuComponent,
    NotificationsComponent,
    NotificationsActivityComponent,
    NotificationsRequestsComponent,
  ],
  imports: [CommonModule, RouterModule, TranslateModule, SharedModule, ScrollPanelModule],
  exports: [HeaderComponent, HeaderMenuComponent],
})
export class HeaderModule {}
