import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommonDataService } from '@core/services/common-data.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  pageTitle;
  constructor(private commonDataService: CommonDataService, public translate: TranslateService) {}

  ngOnInit() {
    this.subscriptions.push(
      this.commonDataService.titleChanged$.subscribe((title) => {
        this.pageTitle = title;
      })
    );
  }

  ngOnDestroy(): void {
    //Unsubscribe subscriptions before the instance is destroyed
    if (this.subscriptions) {
      this.subscriptions.forEach((sub) => sub.unsubscribe());
    }
  }
}
